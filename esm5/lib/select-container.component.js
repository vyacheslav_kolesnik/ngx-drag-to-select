/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, ElementRef, Output, EventEmitter, Input, Renderer2, ViewChild, NgZone, ContentChildren, QueryList, HostBinding } from '@angular/core';
import { Subject, combineLatest, merge, from, fromEvent, BehaviorSubject, asyncScheduler } from 'rxjs';
import { switchMap, takeUntil, map, tap, filter, auditTime, mapTo, share, withLatestFrom, distinctUntilChanged, observeOn, startWith, concatMapTo, first } from 'rxjs/operators';
import { SelectItemDirective } from './select-item.directive';
import { ShortcutService } from './shortcut.service';
import { createSelectBox, whenSelectBoxVisible, distinctKeyEvents } from './operators';
import { Action, UpdateActions } from './models';
import { AUDIT_TIME, NO_SELECT_CLASS, NO_SELECT_CLASS_MOBILE } from './constants';
import { inBoundingBox, cursorWithinElement, clearSelection, boxIntersects, calculateBoundingClientRect, getRelativeMousePosition, getMousePosition, hasMinimumSize } from './utils';
var SelectContainerComponent = /** @class */ (function () {
    function SelectContainerComponent(shortcuts, hostElementRef, renderer, ngZone) {
        this.shortcuts = shortcuts;
        this.hostElementRef = hostElementRef;
        this.renderer = renderer;
        this.ngZone = ngZone;
        this.selectOnDrag = true;
        this.disabled = false;
        this.disableDrag = false;
        this.selectMode = false;
        this.selectWithShortcut = false;
        this.longTouch = false;
        this.longTouchSeconds = 1;
        this.isMobileDevice = false;
        this.custom = false;
        this.selectedItemsChange = new EventEmitter();
        this.select = new EventEmitter();
        this.itemSelected = new EventEmitter();
        this.itemDeselected = new EventEmitter();
        this.selectionStarted = new EventEmitter();
        this.selectionEnded = new EventEmitter();
        this._tmpItems = new Map();
        this._selectedItems$ = new BehaviorSubject([]);
        this.updateItems$ = new Subject();
        this.destroy$ = new Subject();
        this._disabledByLongTouch = false;
    }
    /**
     * @return {?}
     */
    SelectContainerComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.host = this.hostElementRef.nativeElement;
        this._initSelectedItemsChange();
        this._calculateBoundingClientRect();
        this._observeBoundingRectChanges();
        this._observeSelectableItems();
        // distinctKeyEvents is used to prevent multiple key events to be fired repeatedly
        // on Windows when a key is being pressed
        /** @type {?} */
        var keydown$ = fromEvent(window, 'keydown').pipe(distinctKeyEvents(), share());
        /** @type {?} */
        var keyup$ = fromEvent(window, 'keyup').pipe(distinctKeyEvents(), share());
        if (!this.isMobileDevice) {
            /** @type {?} */
            var mouseup$_1 = fromEvent(window, 'mouseup').pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.disabled; })), tap((/**
             * @return {?}
             */
            function () { return _this._onMouseUp(); })), share());
            /** @type {?} */
            var mousemove$_1 = fromEvent(window, 'mousemove').pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.disabled; })), share());
            /** @type {?} */
            var mousedown$ = fromEvent(this.host, 'mousedown').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return event.button === 0; })), // only emit left mouse
            filter((/**
             * @return {?}
             */
            function () { return !_this.disabled; })), tap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this._onMouseDown(event); })), share());
            /** @type {?} */
            var dragging$ = mousedown$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return !_this.shortcuts.disableSelection(event); })), filter((/**
             * @return {?}
             */
            function () { return !_this.selectMode; })), filter((/**
             * @return {?}
             */
            function () { return !_this.disableDrag; })), switchMap((/**
             * @return {?}
             */
            function () { return mousemove$_1.pipe(takeUntil(mouseup$_1)); })), share());
            /** @type {?} */
            var currentMousePosition$ = mousedown$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return getRelativeMousePosition(event, _this.host); })));
            /** @type {?} */
            var show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            var hide$ = mouseup$_1.pipe(mapTo(0));
            /** @type {?} */
            var opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            var selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host), share());
            this.selectBoxClasses$ = merge(dragging$, mouseup$_1, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var _b = tslib_1.__read(_a, 2), event = _b[0], selectBox = _b[1];
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !_this.shortcuts.removeFromSelection(event),
                    'dts-removing': _this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return JSON.stringify(a) === JSON.stringify(b); })));
            /** @type {?} */
            var selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.selectOnDrag; })), filter((/**
             * @return {?}
             */
            function () { return !_this.selectMode; })), filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this._cursorWithinHost(event); })), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            function (_) { return mouseup$_1.pipe(first()); })), filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                return (!_this.shortcuts.disableSelection(event) && !_this.shortcuts.toggleSingleItem(event)) ||
                    _this.shortcuts.removeFromSelection(event);
            })));
            /** @type {?} */
            var selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(mousemove$_1, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            function (selectBox, event) { return ({
                selectBox: selectBox,
                event: event
            }); })), filter((/**
             * @return {?}
             */
            function () { return _this.selectOnDrag; })), filter((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var selectBox = _a.selectBox;
                return hasMinimumSize(selectBox);
            })), map((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var event = _a.event;
                return event;
            })));
            /** @type {?} */
            var selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (_this._isExtendedSelection(event)) {
                    _this._tmpItems.clear();
                }
                else {
                    _this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this._selectItems(event); }));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            function (selectBox) { return ({
                top: selectBox.top + "px",
                left: selectBox.left + "px",
                width: selectBox.width + "px",
                height: selectBox.height + "px",
                opacity: selectBox.opacity
            }); })));
            this._initSelectionOutputs(mousedown$, mouseup$_1);
        }
        else {
            this._disabledByLongTouch = this.longTouch;
            /** @type {?} */
            var touchmove$_1 = fromEvent(window, 'touchmove').pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.disabled; })), tap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (_this.longTouch && _this._longTouchTimer) {
                    clearInterval(_this._longTouchTimer);
                    if (_this._disabledByLongTouch) {
                        _this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
                    }
                    _this._longTouchTimer = null;
                }
            })), share());
            /** @type {?} */
            var touchstart$ = fromEvent(this.host, 'touchstart').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return event.detail === 0; })), // only emit left mouse
            filter((/**
             * @return {?}
             */
            function () { return !_this.disabled && (!_this.isMobileDevice || !_this._longTouchTimer); })), tap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (_this.longTouch) {
                    _this._longTouchSeconds = 0;
                    _this.clearSelection();
                    _this._longTouchTimer = setInterval((/**
                     * @return {?}
                     */
                    function () {
                        _this._longTouchSeconds += 0.1;
                        _this._longTouchSeconds = +_this._longTouchSeconds.toFixed(2);
                        if (_this._longTouchSeconds === _this.longTouchSeconds) {
                            _this._activateSelectingAfterLongTouch(event);
                        }
                    }), 100);
                }
                _this._onMouseDown(event);
            })), share());
            /** @type {?} */
            var touchend$_1 = fromEvent(window, 'touchend').pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.disabled; })), tap((/**
             * @return {?}
             */
            function () {
                if (_this.longTouch) {
                    if (_this._longTouchTimer) {
                        clearInterval(_this._longTouchTimer);
                        _this._longTouchTimer = null;
                    }
                    _this._deactivateSelectingAfterLongTouch();
                }
                _this._onMouseUp();
            })), share());
            /** @type {?} */
            var dragging$ = touchstart$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return !_this.shortcuts.disableSelection(event); })), filter((/**
             * @return {?}
             */
            function () { return !_this.selectMode; })), filter((/**
             * @return {?}
             */
            function () { return !_this.disableDrag; })), switchMap((/**
             * @return {?}
             */
            function () { return touchmove$_1.pipe(takeUntil(touchend$_1)); })), share());
            /** @type {?} */
            var currentMousePosition$ = touchstart$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return getRelativeMousePosition(event, _this.host); })));
            /** @type {?} */
            var show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            var hide$ = touchend$_1.pipe(mapTo(0));
            /** @type {?} */
            var opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            var selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host, (/**
             * @return {?}
             */
            function () { return _this._disabledByLongTouch; })), share());
            this.selectBoxClasses$ = merge(dragging$, touchend$_1, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var _b = tslib_1.__read(_a, 2), event = _b[0], selectBox = _b[1];
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !_this.shortcuts.removeFromSelection(event),
                    'dts-removing': _this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) { return JSON.stringify(a) === JSON.stringify(b); })));
            /** @type {?} */
            var selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            function () { return !_this.selectOnDrag; })), filter((/**
             * @return {?}
             */
            function () { return !_this.selectMode; })), filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this._cursorWithinHost(event); })), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            function (_) { return touchend$_1.pipe(first()); })), filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                return (!_this.shortcuts.disableSelection(event) && !_this.shortcuts.toggleSingleItem(event)) ||
                    _this.shortcuts.removeFromSelection(event);
            })));
            /** @type {?} */
            var selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(touchmove$_1, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            function (selectBox, event) { return ({
                selectBox: selectBox,
                event: event
            }); })), filter((/**
             * @return {?}
             */
            function () { return _this.selectOnDrag; })), filter((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var selectBox = _a.selectBox;
                return hasMinimumSize(selectBox);
            })), map((/**
             * @param {?} __0
             * @return {?}
             */
            function (_a) {
                var event = _a.event;
                return event;
            })));
            /** @type {?} */
            var selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (_this._isExtendedSelection(event)) {
                    _this._tmpItems.clear();
                }
                else {
                    _this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this._selectItems(event); }));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            function (selectBox) { return ({
                top: selectBox.top + "px",
                left: selectBox.left + "px",
                width: selectBox.width + "px",
                height: selectBox.height + "px",
                opacity: selectBox.opacity
            }); })));
            this._initSelectionOutputs(touchstart$, touchend$_1);
        }
    };
    /**
     * @return {?}
     */
    SelectContainerComponent.prototype.selectAll = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            _this._selectItem(item);
        }));
    };
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    SelectContainerComponent.prototype.toggleItems = /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    function (predicate) {
        var _this = this;
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return _this._toggleItem(item); }));
    };
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    SelectContainerComponent.prototype.selectItems = /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    function (predicate) {
        var _this = this;
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return _this._selectItem(item); }));
    };
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    SelectContainerComponent.prototype.deselectItems = /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    function (predicate) {
        var _this = this;
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return _this._deselectItem(item); }));
    };
    /**
     * @return {?}
     */
    SelectContainerComponent.prototype.clearSelection = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            _this._deselectItem(item);
        }));
    };
    /**
     * @return {?}
     */
    SelectContainerComponent.prototype.update = /**
     * @return {?}
     */
    function () {
        this._calculateBoundingClientRect();
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.calculateBoundingClientRect(); }));
    };
    /**
     * @return {?}
     */
    SelectContainerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.destroy$.next();
        this.destroy$.complete();
    };
    /**
     * @private
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    SelectContainerComponent.prototype._filterSelectableItems = /**
     * @private
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    function (predicate) {
        // Wrap select items in an observable for better efficiency as
        // no intermediate arrays are created and we only need to process
        // every item once.
        return from(this.$selectableItems.toArray()).pipe(filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return predicate(item.value); })));
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._initSelectedItemsChange = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this._selectedItems$
            .pipe(auditTime(AUDIT_TIME), takeUntil(this.destroy$))
            .subscribe({
            next: (/**
             * @param {?} selectedItems
             * @return {?}
             */
            function (selectedItems) {
                _this.selectedItemsChange.emit(selectedItems);
                _this.select.emit(selectedItems);
            }),
            complete: (/**
             * @return {?}
             */
            function () {
                _this.selectedItemsChange.emit([]);
            })
        });
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._observeSelectableItems = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        // Listen for updates and either select or deselect an item
        this.updateItems$
            .pipe(withLatestFrom(this._selectedItems$), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 2), update = _b[0], selectedItems = _b[1];
            /** @type {?} */
            var item = update.item;
            switch (update.type) {
                case UpdateActions.Add:
                    if (_this._addItem(item, selectedItems)) {
                        item._select();
                    }
                    break;
                case UpdateActions.Remove:
                    if (_this._removeItem(item, selectedItems)) {
                        item._deselect();
                    }
                    break;
            }
        }));
        // Update the container as well as all selectable items if the list has changed
        this.$selectableItems.changes
            .pipe(withLatestFrom(this._selectedItems$), observeOn(asyncScheduler), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 2), items = _b[0], selectedItems = _b[1];
            /** @type {?} */
            var newList = items.toArray();
            /** @type {?} */
            var removedItems = selectedItems.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !newList.includes(item.value); }));
            if (removedItems.length) {
                removedItems.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return _this._removeItem(item, selectedItems); }));
            }
            _this.update();
        }));
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._observeBoundingRectChanges = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var resize$ = fromEvent(window, 'resize');
            /** @type {?} */
            var windowScroll$ = fromEvent(window, 'scroll');
            /** @type {?} */
            var containerScroll$ = fromEvent(_this.host, 'scroll');
            merge(resize$, windowScroll$, containerScroll$)
                .pipe(startWith('INITIAL_UPDATE'), auditTime(AUDIT_TIME), takeUntil(_this.destroy$))
                .subscribe((/**
             * @return {?}
             */
            function () {
                _this.update();
            }));
        }));
    };
    /**
     * @private
     * @param {?} mousedown$
     * @param {?} mouseup$
     * @return {?}
     */
    SelectContainerComponent.prototype._initSelectionOutputs = /**
     * @private
     * @param {?} mousedown$
     * @param {?} mouseup$
     * @return {?}
     */
    function (mousedown$, mouseup$) {
        var _this = this;
        mousedown$
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return _this._cursorWithinHost(event); })), tap((/**
         * @return {?}
         */
        function () { return _this.selectionStarted.emit(); })), concatMapTo(mouseup$.pipe(first())), withLatestFrom(this._selectedItems$), map((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 2), items = _b[1];
            return items;
        })), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} items
         * @return {?}
         */
        function (items) {
            _this.selectionEnded.emit(items);
        }));
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._calculateBoundingClientRect = /**
     * @private
     * @return {?}
     */
    function () {
        this.host.boundingClientRect = calculateBoundingClientRect(this.host);
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._cursorWithinHost = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return cursorWithinElement(event, this.host);
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._onMouseUp = /**
     * @private
     * @return {?}
     */
    function () {
        this._flushItems();
        this.renderer.removeClass(document.body, NO_SELECT_CLASS);
        this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._onMouseDown = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        if (this.shortcuts.disableSelection(event) || this.disabled) {
            return;
        }
        clearSelection(window);
        if (!this.disableDrag && (!this.longTouch || !this._disabledByLongTouch)) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
        }
        if (!this.disableDrag && this.isMobileDevice) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        /** @type {?} */
        var mousePoint = getMousePosition(event);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        function (item, index) {
            /** @type {?} */
            var itemRect = item.getBoundingClientRect();
            /** @type {?} */
            var withinBoundingBox = inBoundingBox(mousePoint, itemRect);
            if (_this.shortcuts.extendedSelectionShortcut(event)) {
                return;
            }
            /** @type {?} */
            var shouldAdd = (withinBoundingBox &&
                !_this.shortcuts.toggleSingleItem(event) &&
                !_this.selectMode &&
                !_this.selectWithShortcut) ||
                (withinBoundingBox && _this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (!withinBoundingBox && _this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (withinBoundingBox && !item.selected && _this.selectMode) ||
                (!withinBoundingBox && item.selected && _this.selectMode);
            /** @type {?} */
            var shouldRemove = (!withinBoundingBox &&
                !_this.shortcuts.toggleSingleItem(event) &&
                !_this.selectMode &&
                !_this.selectWithShortcut) ||
                (!withinBoundingBox && _this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (withinBoundingBox && _this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (!withinBoundingBox && !item.selected && _this.selectMode) ||
                (withinBoundingBox && item.selected && _this.selectMode);
            if (shouldAdd) {
                _this._selectItem(item);
            }
            else if (shouldRemove) {
                _this._deselectItem(item);
            }
        }));
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._selectItems = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        /** @type {?} */
        var selectionBox = calculateBoundingClientRect(this.$selectBox.nativeElement);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (_this._isExtendedSelection(event)) {
                _this._extendedSelectionMode(selectionBox, item, event);
            }
            else {
                _this._normalSelectionMode(selectionBox, item, event);
            }
        }));
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._isExtendedSelection = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.shortcuts.extendedSelectionShortcut(event) && this.selectOnDrag;
    };
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._normalSelectionMode = /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    function (selectBox, item, event) {
        /** @type {?} */
        var inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        var shouldAdd = inSelection && !item.selected && !this.shortcuts.removeFromSelection(event);
        /** @type {?} */
        var shouldRemove = (!inSelection && item.selected && !this.shortcuts.addToSelection(event)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event));
        if (shouldAdd) {
            this._selectItem(item);
        }
        else if (shouldRemove) {
            this._deselectItem(item);
        }
    };
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._extendedSelectionMode = /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    function (selectBox, item, event) {
        /** @type {?} */
        var inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        var shoudlAdd = (inSelection && !item.selected && !this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item));
        /** @type {?} */
        var shouldRemove = (!inSelection && item.selected && this.shortcuts.addToSelection(event) && this._tmpItems.has(item)) ||
            (!inSelection && !item.selected && this.shortcuts.removeFromSelection(event) && this._tmpItems.has(item));
        if (shoudlAdd) {
            item.selected ? item._deselect() : item._select();
            /** @type {?} */
            var action = this.shortcuts.removeFromSelection(event)
                ? Action.Delete
                : this.shortcuts.addToSelection(event)
                    ? Action.Add
                    : Action.None;
            this._tmpItems.set(item, action);
        }
        else if (shouldRemove) {
            this.shortcuts.removeFromSelection(event) ? item._select() : item._deselect();
            this._tmpItems.delete(item);
        }
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._flushItems = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this._tmpItems.forEach((/**
         * @param {?} action
         * @param {?} item
         * @return {?}
         */
        function (action, item) {
            if (action === Action.Add) {
                _this._selectItem(item);
            }
            if (action === Action.Delete) {
                _this._deselectItem(item);
            }
        }));
        this._tmpItems.clear();
    };
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    SelectContainerComponent.prototype._addItem = /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    function (item, selectedItems) {
        /** @type {?} */
        var success = false;
        if (!this._disabledByLongTouch && !this._hasItem(item, selectedItems)) {
            success = true;
            selectedItems.push(item.value);
            this._selectedItems$.next(selectedItems);
            this.itemSelected.emit(item.value);
        }
        return success;
    };
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    SelectContainerComponent.prototype._removeItem = /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    function (item, selectedItems) {
        /** @type {?} */
        var success = false;
        /** @type {?} */
        var value = item instanceof SelectItemDirective ? item.value : item;
        /** @type {?} */
        var index = selectedItems.indexOf(value);
        if (index > -1) {
            success = true;
            selectedItems.splice(index, 1);
            this._selectedItems$.next(selectedItems);
            this.itemDeselected.emit(item.value);
        }
        return success;
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    SelectContainerComponent.prototype._toggleItem = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (item.selected) {
            this._deselectItem(item);
        }
        else {
            this._selectItem(item);
        }
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    SelectContainerComponent.prototype._selectItem = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.updateItems$.next({ type: UpdateActions.Add, item: item });
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    SelectContainerComponent.prototype._deselectItem = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.updateItems$.next({ type: UpdateActions.Remove, item: item });
    };
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    SelectContainerComponent.prototype._hasItem = /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    function (item, selectedItems) {
        return selectedItems.includes(item.value);
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._drawRipple = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var position = getMousePosition(event);
        /** @type {?} */
        var node = document.querySelector('.ripple');
        /** @type {?} */
        var newNode = (/** @type {?} */ (node.cloneNode(true)));
        newNode.classList.add('animate');
        newNode.style.left = position.x - 5 + 'px';
        newNode.style.top = position.y - 5 + 'px';
        node.parentNode.replaceChild(newNode, node);
    };
    ;
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    SelectContainerComponent.prototype._activateSelectingAfterLongTouch = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this._disabledByLongTouch = false;
        clearInterval(this._longTouchTimer);
        this._longTouchTimer = null;
        this.$selectBox.nativeElement.style.opacity = 1;
        if (!this.disableDrag) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        this._drawRipple(event);
    };
    /**
     * @private
     * @return {?}
     */
    SelectContainerComponent.prototype._deactivateSelectingAfterLongTouch = /**
     * @private
     * @return {?}
     */
    function () {
        this._disabledByLongTouch = true;
        this.$selectBox.nativeElement.style.opacity = 0;
        document.body.style.overflow = 'auto';
    };
    /**
     * @private
     * @param {?} msg
     * @return {?}
     */
    SelectContainerComponent.prototype.log = /**
     * @private
     * @param {?} msg
     * @return {?}
     */
    function (msg) {
        document.getElementById('messages').style.whiteSpace = 'normal';
        document.getElementById('messages').innerHTML += ' ' + msg;
    };
    SelectContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'dts-select-container',
                    exportAs: 'dts-select-container',
                    host: {
                        class: 'dts-select-container'
                    },
                    template: "\n    <ng-content></ng-content>\n    <div class=\"ripple\" #ripple></div>\n    <div\n      class=\"dts-select-box\"\n      #selectBox\n      [ngClass]=\"selectBoxClasses$ | async\"\n      [ngStyle]=\"selectBoxStyles$ | async\"\n    ></div>\n  ",
                    styles: [":host{display:block;position:relative}::ng-deep .dts-no-select dts-select-container{touch-action:none}::ng-deep body.dts-no-select-mobile{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}::ng-deep body.dts-no-select.dts-no-select-mobile{overflow:hidden!important}.ripple{width:20px;height:20px;opacity:0;-webkit-transform:scale(0);transform:scale(0);background:rgba(20,178,252,.5);border-radius:50%;position:fixed}.animate{-webkit-animation:1s cubic-bezier(0,0,.2,1) ripple-mo;animation:1s cubic-bezier(0,0,.2,1) ripple-mo}@-webkit-keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}@keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}"]
                }] }
    ];
    /** @nocollapse */
    SelectContainerComponent.ctorParameters = function () { return [
        { type: ShortcutService },
        { type: ElementRef },
        { type: Renderer2 },
        { type: NgZone }
    ]; };
    SelectContainerComponent.propDecorators = {
        $selectBox: [{ type: ViewChild, args: ['selectBox', { static: true },] }],
        $ripple: [{ type: ViewChild, args: ['ripple', { static: true },] }],
        $selectableItems: [{ type: ContentChildren, args: [SelectItemDirective, { descendants: true },] }],
        selectedItems: [{ type: Input }],
        selectOnDrag: [{ type: Input }],
        disabled: [{ type: Input }],
        disableDrag: [{ type: Input }],
        selectMode: [{ type: Input }],
        selectWithShortcut: [{ type: Input }],
        longTouch: [{ type: Input }],
        longTouchSeconds: [{ type: Input }],
        isMobileDevice: [{ type: Input }],
        custom: [{ type: Input }, { type: HostBinding, args: ['class.dts-custom',] }],
        selectedItemsChange: [{ type: Output }],
        select: [{ type: Output }],
        itemSelected: [{ type: Output }],
        itemDeselected: [{ type: Output }],
        selectionStarted: [{ type: Output }],
        selectionEnded: [{ type: Output }]
    };
    return SelectContainerComponent;
}());
export { SelectContainerComponent };
if (false) {
    /** @type {?} */
    SelectContainerComponent.prototype.host;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxStyles$;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxClasses$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectBox;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$ripple;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectableItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectOnDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.disabled;
    /** @type {?} */
    SelectContainerComponent.prototype.disableDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.selectMode;
    /** @type {?} */
    SelectContainerComponent.prototype.selectWithShortcut;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouch;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouchSeconds;
    /** @type {?} */
    SelectContainerComponent.prototype.isMobileDevice;
    /** @type {?} */
    SelectContainerComponent.prototype.custom;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItemsChange;
    /** @type {?} */
    SelectContainerComponent.prototype.select;
    /** @type {?} */
    SelectContainerComponent.prototype.itemSelected;
    /** @type {?} */
    SelectContainerComponent.prototype.itemDeselected;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionStarted;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionEnded;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._tmpItems;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._selectedItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.updateItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.destroy$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchTimer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchSeconds;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._disabledByLongTouch;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.shortcuts;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.hostElementRef;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.ngZone;
    /* Skipping unhandled member: ;*/
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNvbnRhaW5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZHJhZy10by1zZWxlY3QvIiwic291cmNlcyI6WyJsaWIvc2VsZWN0LWNvbnRhaW5lci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFVBQVUsRUFDVixNQUFNLEVBQ04sWUFBWSxFQUNaLEtBQUssRUFFTCxTQUFTLEVBQ1QsU0FBUyxFQUNULE1BQU0sRUFDTixlQUFlLEVBQ2YsU0FBUyxFQUNULFdBQVcsRUFJWixNQUFNLGVBQWUsQ0FBQztBQUl2QixPQUFPLEVBQWMsT0FBTyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxlQUFlLEVBQUUsY0FBYyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRW5ILE9BQU8sRUFDTCxTQUFTLEVBQ1QsU0FBUyxFQUNULEdBQUcsRUFDSCxHQUFHLEVBQ0gsTUFBTSxFQUNOLFNBQVMsRUFDVCxLQUFLLEVBQ0wsS0FBSyxFQUNMLGNBQWMsRUFDZCxvQkFBb0IsRUFDcEIsU0FBUyxFQUNULFNBQVMsRUFDVCxXQUFXLEVBQ1gsS0FBSyxFQUNOLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEIsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXJELE9BQU8sRUFBRSxlQUFlLEVBQUUsb0JBQW9CLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFdkYsT0FBTyxFQUNMLE1BQU0sRUFLTixhQUFhLEVBRWQsTUFBTSxVQUFVLENBQUM7QUFFbEIsT0FBTyxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFbEYsT0FBTyxFQUNMLGFBQWEsRUFDYixtQkFBbUIsRUFDbkIsY0FBYyxFQUNkLGFBQWEsRUFDYiwyQkFBMkIsRUFDM0Isd0JBQXdCLEVBQ3hCLGdCQUFnQixFQUNoQixjQUFjLEVBQ2YsTUFBTSxTQUFTLENBQUM7QUFFakI7SUErREUsa0NBQ1UsU0FBMEIsRUFDMUIsY0FBMEIsRUFDMUIsUUFBbUIsRUFDbkIsTUFBYztRQUhkLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFZO1FBQzFCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWxDZixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIscUJBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBSWhDLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFFTCx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ2pDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDekMscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVEsQ0FBQztRQUM1QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFjLENBQUM7UUFFbEQsY0FBUyxHQUFHLElBQUksR0FBRyxFQUErQixDQUFDO1FBRW5ELG9CQUFlLEdBQUcsSUFBSSxlQUFlLENBQWEsRUFBRSxDQUFDLENBQUM7UUFDdEQsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBZ0IsQ0FBQztRQUMzQyxhQUFRLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUkvQix5QkFBb0IsR0FBRyxLQUFLLENBQUM7SUFPakMsQ0FBQzs7OztJQUVMLGtEQUFlOzs7SUFBZjtRQUFBLGlCQThRQztRQTdRQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO1FBRTlDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBRWhDLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDOzs7O1lBS3pCLFFBQVEsR0FBRyxTQUFTLENBQWdCLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQy9ELGlCQUFpQixFQUFFLEVBQ25CLEtBQUssRUFBRSxDQUNSOztZQUVLLE1BQU0sR0FBRyxTQUFTLENBQWdCLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQzNELGlCQUFpQixFQUFFLEVBQ25CLEtBQUssRUFBRSxDQUNSO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7O2dCQUNsQixVQUFRLEdBQUcsU0FBUyxDQUFhLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQzVELE1BQU07OztZQUFDLGNBQU0sT0FBQSxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQWQsQ0FBYyxFQUFDLEVBQzVCLEdBQUc7OztZQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxFQUFFLEVBQWpCLENBQWlCLEVBQUMsRUFDNUIsS0FBSyxFQUFFLENBQ1I7O2dCQUVLLFlBQVUsR0FBRyxTQUFTLENBQWEsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FDaEUsTUFBTTs7O1lBQUMsY0FBTSxPQUFBLENBQUMsS0FBSSxDQUFDLFFBQVEsRUFBZCxDQUFjLEVBQUMsRUFDNUIsS0FBSyxFQUFFLENBQ1I7O2dCQUVLLFVBQVUsR0FBRyxTQUFTLENBQWEsSUFBSSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQ25FLE1BQU07Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFsQixDQUFrQixFQUFDLEVBQUUsdUJBQXVCO1lBQzVELE1BQU07OztZQUFDLGNBQU0sT0FBQSxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQWQsQ0FBYyxFQUFDLEVBQzVCLEdBQUc7Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQXhCLENBQXdCLEVBQUMsRUFDdEMsS0FBSyxFQUFFLENBQ1I7O2dCQUVLLFNBQVMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUMvQixNQUFNOzs7O1lBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEVBQXZDLENBQXVDLEVBQUMsRUFDeEQsTUFBTTs7O1lBQUMsY0FBTSxPQUFBLENBQUMsS0FBSSxDQUFDLFVBQVUsRUFBaEIsQ0FBZ0IsRUFBQyxFQUM5QixNQUFNOzs7WUFBQyxjQUFNLE9BQUEsQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFqQixDQUFpQixFQUFDLEVBQy9CLFNBQVM7OztZQUFDLGNBQU0sT0FBQSxZQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFRLENBQUMsQ0FBQyxFQUFwQyxDQUFvQyxFQUFDLEVBQ3JELEtBQUssRUFBRSxDQUNSOztnQkFFSyxxQkFBcUIsR0FBOEIsVUFBVSxDQUFDLElBQUksQ0FDdEUsR0FBRzs7OztZQUFDLFVBQUMsS0FBaUIsSUFBSyxPQUFBLHdCQUF3QixDQUFDLEtBQUssRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLEVBQTFDLENBQTBDLEVBQUMsQ0FDdkU7O2dCQUVLLEtBQUssR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBQ2hDLEtBQUssR0FBRyxVQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBQy9CLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztnQkFFM0QsVUFBVSxHQUFHLGFBQWEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLHFCQUFxQixDQUFDLENBQUMsSUFBSSxDQUMvRSxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUMxQixLQUFLLEVBQUUsQ0FDUjtZQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLFVBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUN4RSxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFDMUIsR0FBRzs7OztZQUFDLFVBQUMsRUFBa0I7b0JBQWxCLDBCQUFrQixFQUFqQixhQUFLLEVBQUUsaUJBQVM7Z0JBQ3BCLE9BQU87b0JBQ0wsWUFBWSxFQUFFLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7b0JBQzNGLGNBQWMsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQztpQkFDMUQsQ0FBQztZQUNKLENBQUMsRUFBQyxFQUNGLG9CQUFvQjs7Ozs7WUFBQyxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQXZDLENBQXVDLEVBQUMsQ0FDeEUsQ0FBQzs7Z0JBRUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FDckMsTUFBTTs7O1lBQUMsY0FBTSxPQUFBLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBbEIsQ0FBa0IsRUFBQyxFQUNoQyxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFoQixDQUFnQixFQUFDLEVBQzlCLE1BQU07Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsRUFBQyxFQUM5QyxTQUFTOzs7O1lBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxVQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQXRCLENBQXNCLEVBQUMsRUFDdEMsTUFBTTs7OztZQUNKLFVBQUEsS0FBSztnQkFDSCxPQUFBLENBQUMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7WUFEekMsQ0FDeUMsRUFDNUMsQ0FDRjs7Z0JBRUssYUFBYSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQ25DLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFDckIsY0FBYyxDQUFDLFlBQVU7Ozs7O1lBQUUsVUFBQyxTQUFTLEVBQUUsS0FBaUIsSUFBSyxPQUFBLENBQUM7Z0JBQzVELFNBQVMsV0FBQTtnQkFDVCxLQUFLLE9BQUE7YUFDTixDQUFDLEVBSDJELENBRzNELEVBQUMsRUFDSCxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksRUFBakIsQ0FBaUIsRUFBQyxFQUMvQixNQUFNOzs7O1lBQUMsVUFBQyxFQUFhO29CQUFYLHdCQUFTO2dCQUFPLE9BQUEsY0FBYyxDQUFDLFNBQVMsQ0FBQztZQUF6QixDQUF5QixFQUFDLEVBQ3BELEdBQUc7Ozs7WUFBQyxVQUFDLEVBQVM7b0JBQVAsZ0JBQUs7Z0JBQU8sT0FBQSxLQUFLO1lBQUwsQ0FBSyxFQUFDLENBQzFCOztnQkFFSyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDekQsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFDaEMsR0FBRzs7OztZQUFDLFVBQUEsS0FBSztnQkFDUCxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDcEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDeEI7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUNwQjtZQUNILENBQUMsRUFBQyxDQUNIO1lBRUQsS0FBSyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxzQkFBc0IsQ0FBQztpQkFDM0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzlCLFNBQVM7Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQXhCLENBQXdCLEVBQUMsQ0FBQztZQUVoRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FDckMsR0FBRzs7OztZQUFDLFVBQUEsU0FBUyxJQUFJLE9BQUEsQ0FBQztnQkFDaEIsR0FBRyxFQUFLLFNBQVMsQ0FBQyxHQUFHLE9BQUk7Z0JBQ3pCLElBQUksRUFBSyxTQUFTLENBQUMsSUFBSSxPQUFJO2dCQUMzQixLQUFLLEVBQUssU0FBUyxDQUFDLEtBQUssT0FBSTtnQkFDN0IsTUFBTSxFQUFLLFNBQVMsQ0FBQyxNQUFNLE9BQUk7Z0JBQy9CLE9BQU8sRUFBRSxTQUFTLENBQUMsT0FBTzthQUMzQixDQUFDLEVBTmUsQ0FNZixFQUFDLENBQ0osQ0FBQztZQUVGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLEVBQUUsVUFBUSxDQUFDLENBQUM7U0FDbEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDOztnQkFFckMsWUFBVSxHQUFHLFNBQVMsQ0FBYSxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUNoRSxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsQ0FBQyxLQUFJLENBQUMsUUFBUSxFQUFkLENBQWMsRUFBQyxFQUM1QixHQUFHOzs7O1lBQUMsVUFBQyxLQUFLO2dCQUNSLElBQUksS0FBSSxDQUFDLFNBQVMsSUFBSSxLQUFJLENBQUMsZUFBZSxFQUFFO29CQUUxQyxhQUFhLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUVwQyxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsRUFBRTt3QkFDN0IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO3FCQUNsRTtvQkFFRCxLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDN0I7WUFDSCxDQUFDLEVBQUMsRUFDRixLQUFLLEVBQUUsQ0FDUjs7Z0JBRUssV0FBVyxHQUFHLFNBQVMsQ0FBYSxJQUFJLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FDckUsTUFBTTs7OztZQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQWxCLENBQWtCLEVBQUMsRUFBRSx1QkFBdUI7WUFDNUQsTUFBTTs7O1lBQUMsY0FBTSxPQUFBLENBQUMsS0FBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsRUFBakUsQ0FBaUUsRUFBQyxFQUMvRSxHQUFHOzs7O1lBQUMsVUFBQSxLQUFLO2dCQUNQLElBQUksS0FBSSxDQUFDLFNBQVMsRUFBRTtvQkFDbEIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQztvQkFFM0IsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUV0QixLQUFJLENBQUMsZUFBZSxHQUFHLFdBQVc7OztvQkFBQzt3QkFDakMsS0FBSSxDQUFDLGlCQUFpQixJQUFJLEdBQUcsQ0FBQzt3QkFFOUIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFFNUQsSUFBSSxLQUFJLENBQUMsaUJBQWlCLEtBQUssS0FBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUNwRCxLQUFJLENBQUMsZ0NBQWdDLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQzlDO29CQUNILENBQUMsR0FBRSxHQUFHLENBQUMsQ0FBQztpQkFDVDtnQkFFRCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzNCLENBQUMsRUFBQyxFQUNGLEtBQUssRUFBRSxDQUNSOztnQkFFSyxXQUFTLEdBQUcsU0FBUyxDQUFhLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQzlELE1BQU07OztZQUFDLGNBQU0sT0FBQSxDQUFDLEtBQUksQ0FBQyxRQUFRLEVBQWQsQ0FBYyxFQUFDLEVBQzVCLEdBQUc7OztZQUFDO2dCQUNGLElBQUksS0FBSSxDQUFDLFNBQVMsRUFBRTtvQkFDbEIsSUFBSSxLQUFJLENBQUMsZUFBZSxFQUFFO3dCQUN4QixhQUFhLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUNwQyxLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztxQkFDN0I7b0JBRUQsS0FBSSxDQUFDLGtDQUFrQyxFQUFFLENBQUM7aUJBQzNDO2dCQUVELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNwQixDQUFDLEVBQUMsRUFDRixLQUFLLEVBQUUsQ0FDUjs7Z0JBRUssU0FBUyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQ2hDLE1BQU07Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsRUFBdkMsQ0FBdUMsRUFBQyxFQUN4RCxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFoQixDQUFnQixFQUFDLEVBQzlCLE1BQU07OztZQUFDLGNBQU0sT0FBQSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQWpCLENBQWlCLEVBQUMsRUFDL0IsU0FBUzs7O1lBQUMsY0FBTSxPQUFBLFlBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVMsQ0FBQyxDQUFDLEVBQXJDLENBQXFDLEVBQUMsRUFDdEQsS0FBSyxFQUFFLENBQ1I7O2dCQUVLLHFCQUFxQixHQUE4QixXQUFXLENBQUMsSUFBSSxDQUN2RSxHQUFHOzs7O1lBQUMsVUFBQyxLQUFpQixJQUFLLE9BQUEsd0JBQXdCLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsRUFBMUMsQ0FBMEMsRUFBQyxDQUN2RTs7Z0JBRUssS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOztnQkFDaEMsS0FBSyxHQUFHLFdBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOztnQkFDaEMsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7O2dCQUUzRCxVQUFVLEdBQUcsYUFBYSxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQy9FLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSTs7O1lBQUUsY0FBTSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsRUFBekIsQ0FBeUIsRUFBQyxFQUMzRCxLQUFLLEVBQUUsQ0FDUjtZQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLFdBQVMsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUN6RSxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFDMUIsR0FBRzs7OztZQUFDLFVBQUMsRUFBa0I7b0JBQWxCLDBCQUFrQixFQUFqQixhQUFLLEVBQUUsaUJBQVM7Z0JBQ3BCLE9BQU87b0JBQ0wsWUFBWSxFQUFFLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7b0JBQzNGLGNBQWMsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQztpQkFDMUQsQ0FBQztZQUNKLENBQUMsRUFBQyxFQUNGLG9CQUFvQjs7Ozs7WUFBQyxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQXZDLENBQXVDLEVBQUMsQ0FDeEUsQ0FBQzs7Z0JBRUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FDckMsTUFBTTs7O1lBQUMsY0FBTSxPQUFBLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBbEIsQ0FBa0IsRUFBQyxFQUNoQyxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFoQixDQUFnQixFQUFDLEVBQzlCLE1BQU07Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsRUFBQyxFQUM5QyxTQUFTOzs7O1lBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxXQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQXZCLENBQXVCLEVBQUMsRUFDdkMsTUFBTTs7OztZQUNKLFVBQUEsS0FBSztnQkFDSCxPQUFBLENBQUMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEYsS0FBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7WUFEekMsQ0FDeUMsRUFDNUMsQ0FDRjs7Z0JBRUssYUFBYSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQ25DLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFDckIsY0FBYyxDQUFDLFlBQVU7Ozs7O1lBQUUsVUFBQyxTQUFTLEVBQUUsS0FBOEIsSUFBSyxPQUFBLENBQUM7Z0JBQ3pFLFNBQVMsV0FBQTtnQkFDVCxLQUFLLE9BQUE7YUFDTixDQUFDLEVBSHdFLENBR3hFLEVBQUMsRUFDSCxNQUFNOzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksRUFBakIsQ0FBaUIsRUFBQyxFQUMvQixNQUFNOzs7O1lBQUMsVUFBQyxFQUFhO29CQUFYLHdCQUFTO2dCQUFPLE9BQUEsY0FBYyxDQUFDLFNBQVMsQ0FBQztZQUF6QixDQUF5QixFQUFDLEVBQ3BELEdBQUc7Ozs7WUFBQyxVQUFDLEVBQVM7b0JBQVAsZ0JBQUs7Z0JBQU8sT0FBQSxLQUFLO1lBQUwsQ0FBSyxFQUFDLENBQzFCOztnQkFFSyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDekQsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFDaEMsR0FBRzs7OztZQUFDLFVBQUEsS0FBSztnQkFDUCxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDcEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDeEI7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUNwQjtZQUNILENBQUMsRUFBQyxDQUNIO1lBRUQsS0FBSyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxzQkFBc0IsQ0FBQztpQkFDM0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzlCLFNBQVM7Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQXhCLENBQXdCLEVBQUMsQ0FBQztZQUVoRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FDckMsR0FBRzs7OztZQUFDLFVBQUEsU0FBUyxJQUFJLE9BQUEsQ0FBQztnQkFDaEIsR0FBRyxFQUFLLFNBQVMsQ0FBQyxHQUFHLE9BQUk7Z0JBQ3pCLElBQUksRUFBSyxTQUFTLENBQUMsSUFBSSxPQUFJO2dCQUMzQixLQUFLLEVBQUssU0FBUyxDQUFDLEtBQUssT0FBSTtnQkFDN0IsTUFBTSxFQUFLLFNBQVMsQ0FBQyxNQUFNLE9BQUk7Z0JBQy9CLE9BQU8sRUFBRSxTQUFTLENBQUMsT0FBTzthQUMzQixDQUFDLEVBTmUsQ0FNZixFQUFDLENBQ0osQ0FBQztZQUVGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsV0FBUyxDQUFDLENBQUM7U0FDcEQ7SUFDSCxDQUFDOzs7O0lBRUQsNENBQVM7OztJQUFUO1FBQUEsaUJBSUM7UUFIQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsSUFBSTtZQUNoQyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsOENBQVc7Ozs7O0lBQVgsVUFBZSxTQUF5QjtRQUF4QyxpQkFFQztRQURDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxJQUF5QixJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBdEIsQ0FBc0IsRUFBQyxDQUFDO0lBQzFHLENBQUM7Ozs7OztJQUVELDhDQUFXOzs7OztJQUFYLFVBQWUsU0FBeUI7UUFBeEMsaUJBRUM7UUFEQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsSUFBeUIsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQXRCLENBQXNCLEVBQUMsQ0FBQztJQUMxRyxDQUFDOzs7Ozs7SUFFRCxnREFBYTs7Ozs7SUFBYixVQUFpQixTQUF5QjtRQUExQyxpQkFFQztRQURDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxJQUF5QixJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsRUFBQyxDQUFDO0lBQzVHLENBQUM7Ozs7SUFFRCxpREFBYzs7O0lBQWQ7UUFBQSxpQkFJQztRQUhDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxJQUFJO1lBQ2hDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQseUNBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQywyQkFBMkIsRUFBRSxFQUFsQyxDQUFrQyxFQUFDLENBQUM7SUFDNUUsQ0FBQzs7OztJQUVELDhDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7Ozs7O0lBRU8seURBQXNCOzs7Ozs7SUFBOUIsVUFBa0MsU0FBeUI7UUFDekQsOERBQThEO1FBQzlELGlFQUFpRTtRQUNqRSxtQkFBbUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FBQyxDQUFDO0lBQzNGLENBQUM7Ozs7O0lBRU8sMkRBQXdCOzs7O0lBQWhDO1FBQUEsaUJBZUM7UUFkQyxJQUFJLENBQUMsZUFBZTthQUNqQixJQUFJLENBQ0gsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUNyQixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUN6QjthQUNBLFNBQVMsQ0FBQztZQUNULElBQUk7Ozs7WUFBRSxVQUFBLGFBQWE7Z0JBQ2pCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdDLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQTtZQUNELFFBQVE7OztZQUFFO2dCQUNSLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFBO1NBQ0YsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTywwREFBdUI7Ozs7SUFBL0I7UUFBQSxpQkF5Q0M7UUF4Q0MsMkRBQTJEO1FBQzNELElBQUksQ0FBQyxZQUFZO2FBQ2QsSUFBSSxDQUNILGNBQWMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQ3BDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2FBQ0EsU0FBUzs7OztRQUFDLFVBQUMsRUFBOEM7Z0JBQTlDLDBCQUE4QyxFQUE3QyxjQUFNLEVBQUUscUJBQWE7O2dCQUMxQixJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUk7WUFFeEIsUUFBUSxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUNuQixLQUFLLGFBQWEsQ0FBQyxHQUFHO29CQUNwQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxFQUFFO3dCQUN0QyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7cUJBQ2hCO29CQUNELE1BQU07Z0JBQ1IsS0FBSyxhQUFhLENBQUMsTUFBTTtvQkFDdkIsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsRUFBRTt3QkFDekMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO3FCQUNsQjtvQkFDRCxNQUFNO2FBQ1Q7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVMLCtFQUErRTtRQUMvRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTzthQUMxQixJQUFJLENBQ0gsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFDcEMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUN6QixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUN6QjthQUNBLFNBQVM7Ozs7UUFBQyxVQUFDLEVBQStEO2dCQUEvRCwwQkFBK0QsRUFBOUQsYUFBSyxFQUFFLHFCQUFhOztnQkFDekIsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLEVBQUU7O2dCQUN6QixZQUFZLEdBQUcsYUFBYSxDQUFDLE1BQU07Ozs7WUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLEVBQUM7WUFFaEYsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO2dCQUN2QixZQUFZLENBQUMsT0FBTzs7OztnQkFBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxFQUFyQyxDQUFxQyxFQUFDLENBQUM7YUFDckU7WUFFRCxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDaEIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVPLDhEQUEyQjs7OztJQUFuQztRQUFBLGlCQWdCQztRQWZDLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCOzs7UUFBQzs7Z0JBQ3RCLE9BQU8sR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQzs7Z0JBQ3JDLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQzs7Z0JBQzNDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQztZQUV2RCxLQUFLLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDNUMsSUFBSSxDQUNILFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUMzQixTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLFNBQVMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2lCQUNBLFNBQVM7OztZQUFDO2dCQUNULEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNoQixDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLHdEQUFxQjs7Ozs7O0lBQTdCLFVBQ0UsVUFBK0MsRUFDL0MsUUFBNkM7UUFGL0MsaUJBZ0JDO1FBWkMsVUFBVTthQUNQLElBQUksQ0FDSCxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEVBQTdCLENBQTZCLEVBQUMsRUFDOUMsR0FBRzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsRUFBNUIsQ0FBNEIsRUFBQyxFQUN2QyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQ25DLGNBQWMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQ3BDLEdBQUc7Ozs7UUFBQyxVQUFDLEVBQVM7Z0JBQVQsMEJBQVMsRUFBTixhQUFLO1lBQU0sT0FBQSxLQUFLO1FBQUwsQ0FBSyxFQUFDLEVBQ3pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2FBQ0EsU0FBUzs7OztRQUFDLFVBQUEsS0FBSztZQUNkLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTywrREFBNEI7Ozs7SUFBcEM7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4RSxDQUFDOzs7Ozs7SUFFTyxvREFBaUI7Ozs7O0lBQXpCLFVBQTBCLEtBQThCO1FBQ3RELE9BQU8sbUJBQW1CLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7OztJQUVPLDZDQUFVOzs7O0lBQWxCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO0lBQ25FLENBQUM7Ozs7OztJQUVPLCtDQUFZOzs7OztJQUFwQixVQUFxQixLQUE4QjtRQUFuRCxpQkFtREM7UUFsREMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDM0QsT0FBTztTQUNSO1FBRUQsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXZCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEVBQUU7WUFDeEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztTQUN4RDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1NBQy9EOztZQUVLLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7UUFFMUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU87Ozs7O1FBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSzs7Z0JBQ2xDLFFBQVEsR0FBRyxJQUFJLENBQUMscUJBQXFCLEVBQUU7O2dCQUN2QyxpQkFBaUIsR0FBRyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQztZQUU3RCxJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ25ELE9BQU87YUFDUjs7Z0JBRUssU0FBUyxHQUNiLENBQUMsaUJBQWlCO2dCQUNoQixDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO2dCQUN2QyxDQUFDLEtBQUksQ0FBQyxVQUFVO2dCQUNoQixDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQkFDM0IsQ0FBQyxpQkFBaUIsSUFBSSxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDL0UsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDL0UsQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQzs7Z0JBRXBELFlBQVksR0FDaEIsQ0FBQyxDQUFDLGlCQUFpQjtnQkFDakIsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQztnQkFDdkMsQ0FBQyxLQUFJLENBQUMsVUFBVTtnQkFDaEIsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxpQkFBaUIsSUFBSSxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDaEYsQ0FBQyxpQkFBaUIsSUFBSSxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzlFLENBQUMsQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQztnQkFDekQsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUM7WUFFekQsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4QjtpQkFBTSxJQUFJLFlBQVksRUFBRTtnQkFDdkIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sK0NBQVk7Ozs7O0lBQXBCLFVBQXFCLEtBQVk7UUFBakMsaUJBVUM7O1lBVE8sWUFBWSxHQUFHLDJCQUEyQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBRS9FLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxJQUFJO1lBQ2hDLElBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNwQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN4RDtpQkFBTTtnQkFDTCxLQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN0RDtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sdURBQW9COzs7OztJQUE1QixVQUE2QixLQUFZO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzlFLENBQUM7Ozs7Ozs7O0lBRU8sdURBQW9COzs7Ozs7O0lBQTVCLFVBQTZCLFNBQVMsRUFBRSxJQUF5QixFQUFFLEtBQVk7O1lBQ3ZFLFdBQVcsR0FBRyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDOztZQUVwRSxTQUFTLEdBQUcsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDOztZQUV2RixZQUFZLEdBQ2hCLENBQUMsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hFLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU3RSxJQUFJLFNBQVMsRUFBRTtZQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7YUFBTSxJQUFJLFlBQVksRUFBRTtZQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzFCO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFTyx5REFBc0I7Ozs7Ozs7SUFBOUIsVUFBK0IsU0FBUyxFQUFFLElBQXlCLEVBQUUsS0FBWTs7WUFDekUsV0FBVyxHQUFHLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7O1lBRXBFLFNBQVMsR0FDYixDQUFDLFdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7O1lBRXBHLFlBQVksR0FDaEIsQ0FBQyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25HLENBQUMsQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFM0csSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7Z0JBRTVDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQztnQkFDdEQsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNO2dCQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUM7b0JBQ3BDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRztvQkFDWixDQUFDLENBQUMsTUFBTSxDQUFDLElBQUk7WUFFakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxZQUFZLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDOUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7SUFDSCxDQUFDOzs7OztJQUVPLDhDQUFXOzs7O0lBQW5CO1FBQUEsaUJBWUM7UUFYQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87Ozs7O1FBQUMsVUFBQyxNQUFNLEVBQUUsSUFBSTtZQUNsQyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsR0FBRyxFQUFFO2dCQUN6QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hCO1lBRUQsSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLE1BQU0sRUFBRTtnQkFDNUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7Ozs7O0lBRU8sMkNBQVE7Ozs7OztJQUFoQixVQUFpQixJQUF5QixFQUFFLGFBQXlCOztZQUMvRCxPQUFPLEdBQUcsS0FBSztRQUVuQixJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEVBQUU7WUFDckUsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNmLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQztRQUVELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7Ozs7Ozs7SUFFTyw4Q0FBVzs7Ozs7O0lBQW5CLFVBQW9CLElBQXlCLEVBQUUsYUFBeUI7O1lBQ2xFLE9BQU8sR0FBRyxLQUFLOztZQUNiLEtBQUssR0FBRyxJQUFJLFlBQVksbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7O1lBQy9ELEtBQUssR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUUxQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNkLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDZixhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEM7UUFFRCxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDOzs7Ozs7SUFFTyw4Q0FBVzs7Ozs7SUFBbkIsVUFBb0IsSUFBeUI7UUFDM0MsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDMUI7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7SUFDSCxDQUFDOzs7Ozs7SUFFTyw4Q0FBVzs7Ozs7SUFBbkIsVUFBb0IsSUFBeUI7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsYUFBYSxDQUFDLEdBQUcsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDNUQsQ0FBQzs7Ozs7O0lBRU8sZ0RBQWE7Ozs7O0lBQXJCLFVBQXNCLElBQXlCO1FBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLGFBQWEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7Ozs7SUFFTywyQ0FBUTs7Ozs7O0lBQWhCLFVBQWlCLElBQXlCLEVBQUUsYUFBeUI7UUFDbkUsT0FBTyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7Ozs7SUFFTyw4Q0FBVzs7Ozs7SUFBbkIsVUFBb0IsS0FBaUI7O1lBQzdCLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7O1lBRWxDLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQzs7WUFFeEMsT0FBTyxHQUFHLG1CQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQWU7UUFDbkQsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQzNDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUUxQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUFBLENBQUM7Ozs7OztJQUVNLG1FQUFnQzs7Ozs7SUFBeEMsVUFBeUMsS0FBaUI7UUFDeEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztRQUNsQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1NBQy9EO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVPLHFFQUFrQzs7OztJQUExQztRQUNFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDaEQsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQztJQUN4QyxDQUFDOzs7Ozs7SUFFTyxzQ0FBRzs7Ozs7SUFBWCxVQUFZLEdBQVc7UUFDckIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztRQUNoRSxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO0lBQzdELENBQUM7O2dCQS9yQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsc0JBQXNCO3FCQUM5QjtvQkFDRCxRQUFRLEVBQUUscVBBU1Q7O2lCQUVGOzs7O2dCQTVDUSxlQUFlO2dCQXRDdEIsVUFBVTtnQkFLVixTQUFTO2dCQUVULE1BQU07Ozs2QkFpRkwsU0FBUyxTQUFDLFdBQVcsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7MEJBR3ZDLFNBQVMsU0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO21DQUdwQyxlQUFlLFNBQUMsbUJBQW1CLEVBQUUsRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO2dDQUcxRCxLQUFLOytCQUNMLEtBQUs7MkJBQ0wsS0FBSzs4QkFDTCxLQUFLOzZCQUNMLEtBQUs7cUNBQ0wsS0FBSzs0QkFDTCxLQUFLO21DQUNMLEtBQUs7aUNBQ0wsS0FBSzt5QkFFTCxLQUFLLFlBQ0wsV0FBVyxTQUFDLGtCQUFrQjtzQ0FHOUIsTUFBTTt5QkFDTixNQUFNOytCQUNOLE1BQU07aUNBQ04sTUFBTTttQ0FDTixNQUFNO2lDQUNOLE1BQU07O0lBNm9CVCwrQkFBQztDQUFBLEFBaHNCRCxJQWdzQkM7U0E5cUJZLHdCQUF3Qjs7O0lBQ25DLHdDQUEwQjs7SUFDMUIsb0RBQWdEOztJQUNoRCxxREFBMEQ7Ozs7O0lBRTFELDhDQUMrQjs7Ozs7SUFFL0IsMkNBQzRCOzs7OztJQUU1QixvREFDeUQ7O0lBRXpELGlEQUE0Qjs7SUFDNUIsZ0RBQTZCOztJQUM3Qiw0Q0FBMEI7O0lBQzFCLCtDQUE2Qjs7SUFDN0IsOENBQTRCOztJQUM1QixzREFBb0M7O0lBQ3BDLDZDQUEyQjs7SUFDM0Isb0RBQThCOztJQUM5QixrREFBZ0M7O0lBRWhDLDBDQUVlOztJQUVmLHVEQUF3RDs7SUFDeEQsMENBQTJDOztJQUMzQyxnREFBaUQ7O0lBQ2pELGtEQUFtRDs7SUFDbkQsb0RBQXNEOztJQUN0RCxrREFBMEQ7Ozs7O0lBRTFELDZDQUEyRDs7Ozs7SUFFM0QsbURBQThEOzs7OztJQUM5RCxnREFBbUQ7Ozs7O0lBQ25ELDRDQUF1Qzs7Ozs7SUFFdkMsbURBQTZCOzs7OztJQUM3QixxREFBa0M7Ozs7O0lBQ2xDLHdEQUFxQzs7Ozs7SUFHbkMsNkNBQWtDOzs7OztJQUNsQyxrREFBa0M7Ozs7O0lBQ2xDLDRDQUEyQjs7Ozs7SUFDM0IsMENBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgRWxlbWVudFJlZixcclxuICBPdXRwdXQsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIElucHV0LFxyXG4gIE9uRGVzdHJveSxcclxuICBSZW5kZXJlcjIsXHJcbiAgVmlld0NoaWxkLFxyXG4gIE5nWm9uZSxcclxuICBDb250ZW50Q2hpbGRyZW4sXHJcbiAgUXVlcnlMaXN0LFxyXG4gIEhvc3RCaW5kaW5nLFxyXG4gIEFmdGVyVmlld0luaXQsXHJcbiAgUExBVEZPUk1fSUQsXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBpc1BsYXRmb3JtQnJvd3NlciB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0LCBjb21iaW5lTGF0ZXN0LCBtZXJnZSwgZnJvbSwgZnJvbUV2ZW50LCBCZWhhdmlvclN1YmplY3QsIGFzeW5jU2NoZWR1bGVyIH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQge1xyXG4gIHN3aXRjaE1hcCxcclxuICB0YWtlVW50aWwsXHJcbiAgbWFwLFxyXG4gIHRhcCxcclxuICBmaWx0ZXIsXHJcbiAgYXVkaXRUaW1lLFxyXG4gIG1hcFRvLFxyXG4gIHNoYXJlLFxyXG4gIHdpdGhMYXRlc3RGcm9tLFxyXG4gIGRpc3RpbmN0VW50aWxDaGFuZ2VkLFxyXG4gIG9ic2VydmVPbixcclxuICBzdGFydFdpdGgsXHJcbiAgY29uY2F0TWFwVG8sXHJcbiAgZmlyc3RcclxufSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBTZWxlY3RJdGVtRGlyZWN0aXZlIH0gZnJvbSAnLi9zZWxlY3QtaXRlbS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTaG9ydGN1dFNlcnZpY2UgfSBmcm9tICcuL3Nob3J0Y3V0LnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgY3JlYXRlU2VsZWN0Qm94LCB3aGVuU2VsZWN0Qm94VmlzaWJsZSwgZGlzdGluY3RLZXlFdmVudHMgfSBmcm9tICcuL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQge1xyXG4gIEFjdGlvbixcclxuICBTZWxlY3RCb3gsXHJcbiAgTW91c2VQb3NpdGlvbixcclxuICBTZWxlY3RDb250YWluZXJIb3N0LFxyXG4gIFVwZGF0ZUFjdGlvbixcclxuICBVcGRhdGVBY3Rpb25zLFxyXG4gIFByZWRpY2F0ZUZuXHJcbn0gZnJvbSAnLi9tb2RlbHMnO1xyXG5cclxuaW1wb3J0IHsgQVVESVRfVElNRSwgTk9fU0VMRUNUX0NMQVNTLCBOT19TRUxFQ1RfQ0xBU1NfTU9CSUxFIH0gZnJvbSAnLi9jb25zdGFudHMnO1xyXG5cclxuaW1wb3J0IHtcclxuICBpbkJvdW5kaW5nQm94LFxyXG4gIGN1cnNvcldpdGhpbkVsZW1lbnQsXHJcbiAgY2xlYXJTZWxlY3Rpb24sXHJcbiAgYm94SW50ZXJzZWN0cyxcclxuICBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QsXHJcbiAgZ2V0UmVsYXRpdmVNb3VzZVBvc2l0aW9uLFxyXG4gIGdldE1vdXNlUG9zaXRpb24sXHJcbiAgaGFzTWluaW11bVNpemVcclxufSBmcm9tICcuL3V0aWxzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZHRzLXNlbGVjdC1jb250YWluZXInLFxyXG4gIGV4cG9ydEFzOiAnZHRzLXNlbGVjdC1jb250YWluZXInLFxyXG4gIGhvc3Q6IHtcclxuICAgIGNsYXNzOiAnZHRzLXNlbGVjdC1jb250YWluZXInXHJcbiAgfSxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgPGRpdiBjbGFzcz1cInJpcHBsZVwiICNyaXBwbGU+PC9kaXY+XHJcbiAgICA8ZGl2XHJcbiAgICAgIGNsYXNzPVwiZHRzLXNlbGVjdC1ib3hcIlxyXG4gICAgICAjc2VsZWN0Qm94XHJcbiAgICAgIFtuZ0NsYXNzXT1cInNlbGVjdEJveENsYXNzZXMkIHwgYXN5bmNcIlxyXG4gICAgICBbbmdTdHlsZV09XCJzZWxlY3RCb3hTdHlsZXMkIHwgYXN5bmNcIlxyXG4gICAgPjwvZGl2PlxyXG4gIGAsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2VsZWN0LWNvbnRhaW5lci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RDb250YWluZXJDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG4gIGhvc3Q6IFNlbGVjdENvbnRhaW5lckhvc3Q7XHJcbiAgc2VsZWN0Qm94U3R5bGVzJDogT2JzZXJ2YWJsZTxTZWxlY3RCb3g8c3RyaW5nPj47XHJcbiAgc2VsZWN0Qm94Q2xhc3NlcyQ6IE9ic2VydmFibGU8eyBba2V5OiBzdHJpbmddOiBib29sZWFuIH0+O1xyXG5cclxuICBAVmlld0NoaWxkKCdzZWxlY3RCb3gnLCB7IHN0YXRpYzogdHJ1ZSB9KVxyXG4gIHByaXZhdGUgJHNlbGVjdEJveDogRWxlbWVudFJlZjtcclxuXHJcbiAgQFZpZXdDaGlsZCgncmlwcGxlJywgeyBzdGF0aWM6IHRydWUgfSlcclxuICBwcml2YXRlICRyaXBwbGU6IEVsZW1lbnRSZWY7XHJcblxyXG4gIEBDb250ZW50Q2hpbGRyZW4oU2VsZWN0SXRlbURpcmVjdGl2ZSwgeyBkZXNjZW5kYW50czogdHJ1ZSB9KVxyXG4gIHByaXZhdGUgJHNlbGVjdGFibGVJdGVtczogUXVlcnlMaXN0PFNlbGVjdEl0ZW1EaXJlY3RpdmU+O1xyXG5cclxuICBASW5wdXQoKSBzZWxlY3RlZEl0ZW1zOiBhbnk7XHJcbiAgQElucHV0KCkgc2VsZWN0T25EcmFnID0gdHJ1ZTtcclxuICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGRpc2FibGVEcmFnID0gZmFsc2U7XHJcbiAgQElucHV0KCkgc2VsZWN0TW9kZSA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHNlbGVjdFdpdGhTaG9ydGN1dCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGxvbmdUb3VjaCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIGxvbmdUb3VjaFNlY29uZHMgPSAxO1xyXG4gIEBJbnB1dCgpIGlzTW9iaWxlRGV2aWNlID0gZmFsc2U7XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kdHMtY3VzdG9tJylcclxuICBjdXN0b20gPSBmYWxzZTtcclxuXHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkSXRlbXNDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgc2VsZWN0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGl0ZW1TZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBpdGVtRGVzZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3Rpb25TdGFydGVkID0gbmV3IEV2ZW50RW1pdHRlcjx2b2lkPigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3Rpb25FbmRlZCA9IG5ldyBFdmVudEVtaXR0ZXI8QXJyYXk8YW55Pj4oKTtcclxuXHJcbiAgcHJpdmF0ZSBfdG1wSXRlbXMgPSBuZXcgTWFwPFNlbGVjdEl0ZW1EaXJlY3RpdmUsIEFjdGlvbj4oKTtcclxuXHJcbiAgcHJpdmF0ZSBfc2VsZWN0ZWRJdGVtcyQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PEFycmF5PGFueT4+KFtdKTtcclxuICBwcml2YXRlIHVwZGF0ZUl0ZW1zJCA9IG5ldyBTdWJqZWN0PFVwZGF0ZUFjdGlvbj4oKTtcclxuICBwcml2YXRlIGRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuXHJcbiAgcHJpdmF0ZSBfbG9uZ1RvdWNoVGltZXI6IGFueTtcclxuICBwcml2YXRlIF9sb25nVG91Y2hTZWNvbmRzOiBudW1iZXI7XHJcbiAgcHJpdmF0ZSBfZGlzYWJsZWRCeUxvbmdUb3VjaCA9IGZhbHNlO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgc2hvcnRjdXRzOiBTaG9ydGN1dFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGhvc3RFbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZVxyXG4gICkgeyB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIHRoaXMuaG9zdCA9IHRoaXMuaG9zdEVsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcclxuXHJcbiAgICB0aGlzLl9pbml0U2VsZWN0ZWRJdGVtc0NoYW5nZSgpO1xyXG5cclxuICAgIHRoaXMuX2NhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgdGhpcy5fb2JzZXJ2ZUJvdW5kaW5nUmVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMuX29ic2VydmVTZWxlY3RhYmxlSXRlbXMoKTtcclxuXHJcbiAgICAvLyBkaXN0aW5jdEtleUV2ZW50cyBpcyB1c2VkIHRvIHByZXZlbnQgbXVsdGlwbGUga2V5IGV2ZW50cyB0byBiZSBmaXJlZCByZXBlYXRlZGx5XHJcbiAgICAvLyBvbiBXaW5kb3dzIHdoZW4gYSBrZXkgaXMgYmVpbmcgcHJlc3NlZFxyXG5cclxuICAgIGNvbnN0IGtleWRvd24kID0gZnJvbUV2ZW50PEtleWJvYXJkRXZlbnQ+KHdpbmRvdywgJ2tleWRvd24nKS5waXBlKFxyXG4gICAgICBkaXN0aW5jdEtleUV2ZW50cygpLFxyXG4gICAgICBzaGFyZSgpXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IGtleXVwJCA9IGZyb21FdmVudDxLZXlib2FyZEV2ZW50Pih3aW5kb3csICdrZXl1cCcpLnBpcGUoXHJcbiAgICAgIGRpc3RpbmN0S2V5RXZlbnRzKCksXHJcbiAgICAgIHNoYXJlKClcclxuICAgICk7XHJcblxyXG4gICAgaWYgKCF0aGlzLmlzTW9iaWxlRGV2aWNlKSB7XHJcbiAgICAgIGNvbnN0IG1vdXNldXAkID0gZnJvbUV2ZW50PE1vdXNlRXZlbnQ+KHdpbmRvdywgJ21vdXNldXAnKS5waXBlKFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlZCksXHJcbiAgICAgICAgdGFwKCgpID0+IHRoaXMuX29uTW91c2VVcCgpKSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBtb3VzZW1vdmUkID0gZnJvbUV2ZW50PE1vdXNlRXZlbnQ+KHdpbmRvdywgJ21vdXNlbW92ZScpLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLmRpc2FibGVkKSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBtb3VzZWRvd24kID0gZnJvbUV2ZW50PE1vdXNlRXZlbnQ+KHRoaXMuaG9zdCwgJ21vdXNlZG93bicpLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50LmJ1dHRvbiA9PT0gMCksIC8vIG9ubHkgZW1pdCBsZWZ0IG1vdXNlXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLmRpc2FibGVkKSxcclxuICAgICAgICB0YXAoZXZlbnQgPT4gdGhpcy5fb25Nb3VzZURvd24oZXZlbnQpKSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBkcmFnZ2luZyQgPSBtb3VzZWRvd24kLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+ICF0aGlzLnNob3J0Y3V0cy5kaXNhYmxlU2VsZWN0aW9uKGV2ZW50KSksXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLnNlbGVjdE1vZGUpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlRHJhZyksXHJcbiAgICAgICAgc3dpdGNoTWFwKCgpID0+IG1vdXNlbW92ZSQucGlwZSh0YWtlVW50aWwobW91c2V1cCQpKSksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgY3VycmVudE1vdXNlUG9zaXRpb24kOiBPYnNlcnZhYmxlPE1vdXNlUG9zaXRpb24+ID0gbW91c2Vkb3duJC5waXBlKFxyXG4gICAgICAgIG1hcCgoZXZlbnQ6IE1vdXNlRXZlbnQpID0+IGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbihldmVudCwgdGhpcy5ob3N0KSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNob3ckID0gZHJhZ2dpbmckLnBpcGUobWFwVG8oMSkpO1xyXG4gICAgICBjb25zdCBoaWRlJCA9IG1vdXNldXAkLnBpcGUobWFwVG8oMCkpO1xyXG4gICAgICBjb25zdCBvcGFjaXR5JCA9IG1lcmdlKHNob3ckLCBoaWRlJCkucGlwZShkaXN0aW5jdFVudGlsQ2hhbmdlZCgpKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdEJveCQgPSBjb21iaW5lTGF0ZXN0KGRyYWdnaW5nJCwgb3BhY2l0eSQsIGN1cnJlbnRNb3VzZVBvc2l0aW9uJCkucGlwZShcclxuICAgICAgICBjcmVhdGVTZWxlY3RCb3godGhpcy5ob3N0KSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLnNlbGVjdEJveENsYXNzZXMkID0gbWVyZ2UoZHJhZ2dpbmckLCBtb3VzZXVwJCwga2V5ZG93biQsIGtleXVwJCkucGlwZShcclxuICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgd2l0aExhdGVzdEZyb20oc2VsZWN0Qm94JCksXHJcbiAgICAgICAgbWFwKChbZXZlbnQsIHNlbGVjdEJveF0pID0+IHtcclxuICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICdkdHMtYWRkaW5nJzogaGFzTWluaW11bVNpemUoc2VsZWN0Qm94LCAwLCAwKSAmJiAhdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCksXHJcbiAgICAgICAgICAgICdkdHMtcmVtb3ZpbmcnOiB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KVxyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KSxcclxuICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgoYSwgYikgPT4gSlNPTi5zdHJpbmdpZnkoYSkgPT09IEpTT04uc3RyaW5naWZ5KGIpKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3Qgc2VsZWN0T25Nb3VzZVVwJCA9IGRyYWdnaW5nJC5waXBlKFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5zZWxlY3RPbkRyYWcpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5zZWxlY3RNb2RlKSxcclxuICAgICAgICBmaWx0ZXIoZXZlbnQgPT4gdGhpcy5fY3Vyc29yV2l0aGluSG9zdChldmVudCkpLFxyXG4gICAgICAgIHN3aXRjaE1hcChfID0+IG1vdXNldXAkLnBpcGUoZmlyc3QoKSkpLFxyXG4gICAgICAgIGZpbHRlcihcclxuICAgICAgICAgIGV2ZW50ID0+XHJcbiAgICAgICAgICAgICghdGhpcy5zaG9ydGN1dHMuZGlzYWJsZVNlbGVjdGlvbihldmVudCkgJiYgIXRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpKSB8fFxyXG4gICAgICAgICAgICB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KVxyXG4gICAgICAgIClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uRHJhZyQgPSBzZWxlY3RCb3gkLnBpcGUoXHJcbiAgICAgICAgYXVkaXRUaW1lKEFVRElUX1RJTUUpLFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKG1vdXNlbW92ZSQsIChzZWxlY3RCb3gsIGV2ZW50OiBNb3VzZUV2ZW50KSA9PiAoe1xyXG4gICAgICAgICAgc2VsZWN0Qm94LFxyXG4gICAgICAgICAgZXZlbnRcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgZmlsdGVyKCgpID0+IHRoaXMuc2VsZWN0T25EcmFnKSxcclxuICAgICAgICBmaWx0ZXIoKHsgc2VsZWN0Qm94IH0pID0+IGhhc01pbmltdW1TaXplKHNlbGVjdEJveCkpLFxyXG4gICAgICAgIG1hcCgoeyBldmVudCB9KSA9PiBldmVudClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uS2V5Ym9hcmRFdmVudCQgPSBtZXJnZShrZXlkb3duJCwga2V5dXAkKS5waXBlKFxyXG4gICAgICAgIGF1ZGl0VGltZShBVURJVF9USU1FKSxcclxuICAgICAgICB3aGVuU2VsZWN0Qm94VmlzaWJsZShzZWxlY3RCb3gkKSxcclxuICAgICAgICB0YXAoZXZlbnQgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuX2lzRXh0ZW5kZWRTZWxlY3Rpb24oZXZlbnQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RtcEl0ZW1zLmNsZWFyKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9mbHVzaEl0ZW1zKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIG1lcmdlKHNlbGVjdE9uTW91c2VVcCQsIHNlbGVjdE9uRHJhZyQsIHNlbGVjdE9uS2V5Ym9hcmRFdmVudCQpXHJcbiAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveSQpKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4gdGhpcy5fc2VsZWN0SXRlbXMoZXZlbnQpKTtcclxuXHJcbiAgICAgIHRoaXMuc2VsZWN0Qm94U3R5bGVzJCA9IHNlbGVjdEJveCQucGlwZShcclxuICAgICAgICBtYXAoc2VsZWN0Qm94ID0+ICh7XHJcbiAgICAgICAgICB0b3A6IGAke3NlbGVjdEJveC50b3B9cHhgLFxyXG4gICAgICAgICAgbGVmdDogYCR7c2VsZWN0Qm94LmxlZnR9cHhgLFxyXG4gICAgICAgICAgd2lkdGg6IGAke3NlbGVjdEJveC53aWR0aH1weGAsXHJcbiAgICAgICAgICBoZWlnaHQ6IGAke3NlbGVjdEJveC5oZWlnaHR9cHhgLFxyXG4gICAgICAgICAgb3BhY2l0eTogc2VsZWN0Qm94Lm9wYWNpdHlcclxuICAgICAgICB9KSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIHRoaXMuX2luaXRTZWxlY3Rpb25PdXRwdXRzKG1vdXNlZG93biQsIG1vdXNldXAkKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2ggPSB0aGlzLmxvbmdUb3VjaDtcclxuXHJcbiAgICAgIGNvbnN0IHRvdWNobW92ZSQgPSBmcm9tRXZlbnQ8VG91Y2hFdmVudD4od2luZG93LCAndG91Y2htb3ZlJykucGlwZShcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZWQpLFxyXG4gICAgICAgIHRhcCgoZXZlbnQpID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLmxvbmdUb3VjaCAmJiB0aGlzLl9sb25nVG91Y2hUaW1lcikge1xyXG5cclxuICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLl9sb25nVG91Y2hUaW1lcik7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5fZGlzYWJsZWRCeUxvbmdUb3VjaCkge1xyXG4gICAgICAgICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3MoZG9jdW1lbnQuYm9keSwgTk9fU0VMRUNUX0NMQVNTX01PQklMRSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2xvbmdUb3VjaFRpbWVyID0gbnVsbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCB0b3VjaHN0YXJ0JCA9IGZyb21FdmVudDxUb3VjaEV2ZW50Pih0aGlzLmhvc3QsICd0b3VjaHN0YXJ0JykucGlwZShcclxuICAgICAgICBmaWx0ZXIoZXZlbnQgPT4gZXZlbnQuZGV0YWlsID09PSAwKSwgLy8gb25seSBlbWl0IGxlZnQgbW91c2VcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZWQgJiYgKCF0aGlzLmlzTW9iaWxlRGV2aWNlIHx8ICF0aGlzLl9sb25nVG91Y2hUaW1lcikpLFxyXG4gICAgICAgIHRhcChldmVudCA9PiB7XHJcbiAgICAgICAgICBpZiAodGhpcy5sb25nVG91Y2gpIHtcclxuICAgICAgICAgICAgdGhpcy5fbG9uZ1RvdWNoU2Vjb25kcyA9IDA7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNsZWFyU2VsZWN0aW9uKCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9sb25nVG91Y2hUaW1lciA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLl9sb25nVG91Y2hTZWNvbmRzICs9IDAuMTtcclxuXHJcbiAgICAgICAgICAgICAgdGhpcy5fbG9uZ1RvdWNoU2Vjb25kcyA9ICt0aGlzLl9sb25nVG91Y2hTZWNvbmRzLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMuX2xvbmdUb3VjaFNlY29uZHMgPT09IHRoaXMubG9uZ1RvdWNoU2Vjb25kcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYWN0aXZhdGVTZWxlY3RpbmdBZnRlckxvbmdUb3VjaChldmVudCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCAxMDApO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuX29uTW91c2VEb3duKGV2ZW50KTtcclxuICAgICAgICB9KSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCB0b3VjaGVuZCQgPSBmcm9tRXZlbnQ8VG91Y2hFdmVudD4od2luZG93LCAndG91Y2hlbmQnKS5waXBlKFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlZCksXHJcbiAgICAgICAgdGFwKCgpID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLmxvbmdUb3VjaCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fbG9uZ1RvdWNoVGltZXIpIHtcclxuICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuX2xvbmdUb3VjaFRpbWVyKTtcclxuICAgICAgICAgICAgICB0aGlzLl9sb25nVG91Y2hUaW1lciA9IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuX2RlYWN0aXZhdGVTZWxlY3RpbmdBZnRlckxvbmdUb3VjaCgpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuX29uTW91c2VVcCgpO1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIHNoYXJlKClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IGRyYWdnaW5nJCA9IHRvdWNoc3RhcnQkLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+ICF0aGlzLnNob3J0Y3V0cy5kaXNhYmxlU2VsZWN0aW9uKGV2ZW50KSksXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLnNlbGVjdE1vZGUpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlRHJhZyksXHJcbiAgICAgICAgc3dpdGNoTWFwKCgpID0+IHRvdWNobW92ZSQucGlwZSh0YWtlVW50aWwodG91Y2hlbmQkKSkpLFxyXG4gICAgICAgIHNoYXJlKClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IGN1cnJlbnRNb3VzZVBvc2l0aW9uJDogT2JzZXJ2YWJsZTxNb3VzZVBvc2l0aW9uPiA9IHRvdWNoc3RhcnQkLnBpcGUoXHJcbiAgICAgICAgbWFwKChldmVudDogVG91Y2hFdmVudCkgPT4gZ2V0UmVsYXRpdmVNb3VzZVBvc2l0aW9uKGV2ZW50LCB0aGlzLmhvc3QpKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3Qgc2hvdyQgPSBkcmFnZ2luZyQucGlwZShtYXBUbygxKSk7XHJcbiAgICAgIGNvbnN0IGhpZGUkID0gdG91Y2hlbmQkLnBpcGUobWFwVG8oMCkpO1xyXG4gICAgICBjb25zdCBvcGFjaXR5JCA9IG1lcmdlKHNob3ckLCBoaWRlJCkucGlwZShkaXN0aW5jdFVudGlsQ2hhbmdlZCgpKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdEJveCQgPSBjb21iaW5lTGF0ZXN0KGRyYWdnaW5nJCwgb3BhY2l0eSQsIGN1cnJlbnRNb3VzZVBvc2l0aW9uJCkucGlwZShcclxuICAgICAgICBjcmVhdGVTZWxlY3RCb3godGhpcy5ob3N0LCAoKSA9PiB0aGlzLl9kaXNhYmxlZEJ5TG9uZ1RvdWNoKSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLnNlbGVjdEJveENsYXNzZXMkID0gbWVyZ2UoZHJhZ2dpbmckLCB0b3VjaGVuZCQsIGtleWRvd24kLCBrZXl1cCQpLnBpcGUoXHJcbiAgICAgICAgYXVkaXRUaW1lKEFVRElUX1RJTUUpLFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHNlbGVjdEJveCQpLFxyXG4gICAgICAgIG1hcCgoW2V2ZW50LCBzZWxlY3RCb3hdKSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAnZHRzLWFkZGluZyc6IGhhc01pbmltdW1TaXplKHNlbGVjdEJveCwgMCwgMCkgJiYgIXRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpLFxyXG4gICAgICAgICAgICAnZHRzLXJlbW92aW5nJzogdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudClcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgZGlzdGluY3RVbnRpbENoYW5nZWQoKGEsIGIpID0+IEpTT04uc3RyaW5naWZ5KGEpID09PSBKU09OLnN0cmluZ2lmeShiKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uTW91c2VVcCQgPSBkcmFnZ2luZyQucGlwZShcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuc2VsZWN0T25EcmFnKSxcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuc2VsZWN0TW9kZSksXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IHRoaXMuX2N1cnNvcldpdGhpbkhvc3QoZXZlbnQpKSxcclxuICAgICAgICBzd2l0Y2hNYXAoXyA9PiB0b3VjaGVuZCQucGlwZShmaXJzdCgpKSksXHJcbiAgICAgICAgZmlsdGVyKFxyXG4gICAgICAgICAgZXZlbnQgPT5cclxuICAgICAgICAgICAgKCF0aGlzLnNob3J0Y3V0cy5kaXNhYmxlU2VsZWN0aW9uKGV2ZW50KSAmJiAhdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkpIHx8XHJcbiAgICAgICAgICAgIHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpXHJcbiAgICAgICAgKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3Qgc2VsZWN0T25EcmFnJCA9IHNlbGVjdEJveCQucGlwZShcclxuICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgd2l0aExhdGVzdEZyb20odG91Y2htb3ZlJCwgKHNlbGVjdEJveCwgZXZlbnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSA9PiAoe1xyXG4gICAgICAgICAgc2VsZWN0Qm94LFxyXG4gICAgICAgICAgZXZlbnRcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgZmlsdGVyKCgpID0+IHRoaXMuc2VsZWN0T25EcmFnKSxcclxuICAgICAgICBmaWx0ZXIoKHsgc2VsZWN0Qm94IH0pID0+IGhhc01pbmltdW1TaXplKHNlbGVjdEJveCkpLFxyXG4gICAgICAgIG1hcCgoeyBldmVudCB9KSA9PiBldmVudClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uS2V5Ym9hcmRFdmVudCQgPSBtZXJnZShrZXlkb3duJCwga2V5dXAkKS5waXBlKFxyXG4gICAgICAgIGF1ZGl0VGltZShBVURJVF9USU1FKSxcclxuICAgICAgICB3aGVuU2VsZWN0Qm94VmlzaWJsZShzZWxlY3RCb3gkKSxcclxuICAgICAgICB0YXAoZXZlbnQgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuX2lzRXh0ZW5kZWRTZWxlY3Rpb24oZXZlbnQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RtcEl0ZW1zLmNsZWFyKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9mbHVzaEl0ZW1zKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIG1lcmdlKHNlbGVjdE9uTW91c2VVcCQsIHNlbGVjdE9uRHJhZyQsIHNlbGVjdE9uS2V5Ym9hcmRFdmVudCQpXHJcbiAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuZGVzdHJveSQpKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4gdGhpcy5fc2VsZWN0SXRlbXMoZXZlbnQpKTtcclxuXHJcbiAgICAgIHRoaXMuc2VsZWN0Qm94U3R5bGVzJCA9IHNlbGVjdEJveCQucGlwZShcclxuICAgICAgICBtYXAoc2VsZWN0Qm94ID0+ICh7XHJcbiAgICAgICAgICB0b3A6IGAke3NlbGVjdEJveC50b3B9cHhgLFxyXG4gICAgICAgICAgbGVmdDogYCR7c2VsZWN0Qm94LmxlZnR9cHhgLFxyXG4gICAgICAgICAgd2lkdGg6IGAke3NlbGVjdEJveC53aWR0aH1weGAsXHJcbiAgICAgICAgICBoZWlnaHQ6IGAke3NlbGVjdEJveC5oZWlnaHR9cHhgLFxyXG4gICAgICAgICAgb3BhY2l0eTogc2VsZWN0Qm94Lm9wYWNpdHlcclxuICAgICAgICB9KSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIHRoaXMuX2luaXRTZWxlY3Rpb25PdXRwdXRzKHRvdWNoc3RhcnQkLCB0b3VjaGVuZCQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2VsZWN0QWxsKCkge1xyXG4gICAgdGhpcy4kc2VsZWN0YWJsZUl0ZW1zLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIHRoaXMuX3NlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUl0ZW1zPFQ+KHByZWRpY2F0ZTogUHJlZGljYXRlRm48VD4pIHtcclxuICAgIHRoaXMuX2ZpbHRlclNlbGVjdGFibGVJdGVtcyhwcmVkaWNhdGUpLnN1YnNjcmliZSgoaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSkgPT4gdGhpcy5fdG9nZ2xlSXRlbShpdGVtKSk7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RJdGVtczxUPihwcmVkaWNhdGU6IFByZWRpY2F0ZUZuPFQ+KSB7XHJcbiAgICB0aGlzLl9maWx0ZXJTZWxlY3RhYmxlSXRlbXMocHJlZGljYXRlKS5zdWJzY3JpYmUoKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpID0+IHRoaXMuX3NlbGVjdEl0ZW0oaXRlbSkpO1xyXG4gIH1cclxuXHJcbiAgZGVzZWxlY3RJdGVtczxUPihwcmVkaWNhdGU6IFByZWRpY2F0ZUZuPFQ+KSB7XHJcbiAgICB0aGlzLl9maWx0ZXJTZWxlY3RhYmxlSXRlbXMocHJlZGljYXRlKS5zdWJzY3JpYmUoKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpID0+IHRoaXMuX2Rlc2VsZWN0SXRlbShpdGVtKSk7XHJcbiAgfVxyXG5cclxuICBjbGVhclNlbGVjdGlvbigpIHtcclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICB0aGlzLl9kZXNlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZSgpIHtcclxuICAgIHRoaXMuX2NhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgdGhpcy4kc2VsZWN0YWJsZUl0ZW1zLmZvckVhY2goaXRlbSA9PiBpdGVtLmNhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCgpKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5kZXN0cm95JC5uZXh0KCk7XHJcbiAgICB0aGlzLmRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9maWx0ZXJTZWxlY3RhYmxlSXRlbXM8VD4ocHJlZGljYXRlOiBQcmVkaWNhdGVGbjxUPikge1xyXG4gICAgLy8gV3JhcCBzZWxlY3QgaXRlbXMgaW4gYW4gb2JzZXJ2YWJsZSBmb3IgYmV0dGVyIGVmZmljaWVuY3kgYXNcclxuICAgIC8vIG5vIGludGVybWVkaWF0ZSBhcnJheXMgYXJlIGNyZWF0ZWQgYW5kIHdlIG9ubHkgbmVlZCB0byBwcm9jZXNzXHJcbiAgICAvLyBldmVyeSBpdGVtIG9uY2UuXHJcbiAgICByZXR1cm4gZnJvbSh0aGlzLiRzZWxlY3RhYmxlSXRlbXMudG9BcnJheSgpKS5waXBlKGZpbHRlcihpdGVtID0+IHByZWRpY2F0ZShpdGVtLnZhbHVlKSkpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfaW5pdFNlbGVjdGVkSXRlbXNDaGFuZ2UoKSB7XHJcbiAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zJFxyXG4gICAgICAucGlwZShcclxuICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgdGFrZVVudGlsKHRoaXMuZGVzdHJveSQpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSh7XHJcbiAgICAgICAgbmV4dDogc2VsZWN0ZWRJdGVtcyA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkSXRlbXNDaGFuZ2UuZW1pdChzZWxlY3RlZEl0ZW1zKTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0LmVtaXQoc2VsZWN0ZWRJdGVtcyk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb21wbGV0ZTogKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zQ2hhbmdlLmVtaXQoW10pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9vYnNlcnZlU2VsZWN0YWJsZUl0ZW1zKCkge1xyXG4gICAgLy8gTGlzdGVuIGZvciB1cGRhdGVzIGFuZCBlaXRoZXIgc2VsZWN0IG9yIGRlc2VsZWN0IGFuIGl0ZW1cclxuICAgIHRoaXMudXBkYXRlSXRlbXMkXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHRoaXMuX3NlbGVjdGVkSXRlbXMkKSxcclxuICAgICAgICB0YWtlVW50aWwodGhpcy5kZXN0cm95JClcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKChbdXBkYXRlLCBzZWxlY3RlZEl0ZW1zXTogW1VwZGF0ZUFjdGlvbiwgYW55W11dKSA9PiB7XHJcbiAgICAgICAgY29uc3QgaXRlbSA9IHVwZGF0ZS5pdGVtO1xyXG5cclxuICAgICAgICBzd2l0Y2ggKHVwZGF0ZS50eXBlKSB7XHJcbiAgICAgICAgICBjYXNlIFVwZGF0ZUFjdGlvbnMuQWRkOlxyXG4gICAgICAgICAgICBpZiAodGhpcy5fYWRkSXRlbShpdGVtLCBzZWxlY3RlZEl0ZW1zKSkge1xyXG4gICAgICAgICAgICAgIGl0ZW0uX3NlbGVjdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgY2FzZSBVcGRhdGVBY3Rpb25zLlJlbW92ZTpcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3JlbW92ZUl0ZW0oaXRlbSwgc2VsZWN0ZWRJdGVtcykpIHtcclxuICAgICAgICAgICAgICBpdGVtLl9kZXNlbGVjdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gVXBkYXRlIHRoZSBjb250YWluZXIgYXMgd2VsbCBhcyBhbGwgc2VsZWN0YWJsZSBpdGVtcyBpZiB0aGUgbGlzdCBoYXMgY2hhbmdlZFxyXG4gICAgdGhpcy4kc2VsZWN0YWJsZUl0ZW1zLmNoYW5nZXNcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgd2l0aExhdGVzdEZyb20odGhpcy5fc2VsZWN0ZWRJdGVtcyQpLFxyXG4gICAgICAgIG9ic2VydmVPbihhc3luY1NjaGVkdWxlciksXHJcbiAgICAgICAgdGFrZVVudGlsKHRoaXMuZGVzdHJveSQpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgoW2l0ZW1zLCBzZWxlY3RlZEl0ZW1zXTogW1F1ZXJ5TGlzdDxTZWxlY3RJdGVtRGlyZWN0aXZlPiwgYW55W11dKSA9PiB7XHJcbiAgICAgICAgY29uc3QgbmV3TGlzdCA9IGl0ZW1zLnRvQXJyYXkoKTtcclxuICAgICAgICBjb25zdCByZW1vdmVkSXRlbXMgPSBzZWxlY3RlZEl0ZW1zLmZpbHRlcihpdGVtID0+ICFuZXdMaXN0LmluY2x1ZGVzKGl0ZW0udmFsdWUpKTtcclxuXHJcbiAgICAgICAgaWYgKHJlbW92ZWRJdGVtcy5sZW5ndGgpIHtcclxuICAgICAgICAgIHJlbW92ZWRJdGVtcy5mb3JFYWNoKGl0ZW0gPT4gdGhpcy5fcmVtb3ZlSXRlbShpdGVtLCBzZWxlY3RlZEl0ZW1zKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX29ic2VydmVCb3VuZGluZ1JlY3RDaGFuZ2VzKCkge1xyXG4gICAgdGhpcy5uZ1pvbmUucnVuT3V0c2lkZUFuZ3VsYXIoKCkgPT4ge1xyXG4gICAgICBjb25zdCByZXNpemUkID0gZnJvbUV2ZW50KHdpbmRvdywgJ3Jlc2l6ZScpO1xyXG4gICAgICBjb25zdCB3aW5kb3dTY3JvbGwkID0gZnJvbUV2ZW50KHdpbmRvdywgJ3Njcm9sbCcpO1xyXG4gICAgICBjb25zdCBjb250YWluZXJTY3JvbGwkID0gZnJvbUV2ZW50KHRoaXMuaG9zdCwgJ3Njcm9sbCcpO1xyXG5cclxuICAgICAgbWVyZ2UocmVzaXplJCwgd2luZG93U2Nyb2xsJCwgY29udGFpbmVyU2Nyb2xsJClcclxuICAgICAgICAucGlwZShcclxuICAgICAgICAgIHN0YXJ0V2l0aCgnSU5JVElBTF9VUERBVEUnKSxcclxuICAgICAgICAgIGF1ZGl0VGltZShBVURJVF9USU1FKSxcclxuICAgICAgICAgIHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKVxyXG4gICAgICAgIClcclxuICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgIHRoaXMudXBkYXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2luaXRTZWxlY3Rpb25PdXRwdXRzKFxyXG4gICAgbW91c2Vkb3duJDogT2JzZXJ2YWJsZTxNb3VzZUV2ZW50IHwgVG91Y2hFdmVudD4sXHJcbiAgICBtb3VzZXVwJDogT2JzZXJ2YWJsZTxNb3VzZUV2ZW50IHwgVG91Y2hFdmVudD5cclxuICApIHtcclxuICAgIG1vdXNlZG93biRcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IHRoaXMuX2N1cnNvcldpdGhpbkhvc3QoZXZlbnQpKSxcclxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5zZWxlY3Rpb25TdGFydGVkLmVtaXQoKSksXHJcbiAgICAgICAgY29uY2F0TWFwVG8obW91c2V1cCQucGlwZShmaXJzdCgpKSksXHJcbiAgICAgICAgd2l0aExhdGVzdEZyb20odGhpcy5fc2VsZWN0ZWRJdGVtcyQpLFxyXG4gICAgICAgIG1hcCgoWywgaXRlbXNdKSA9PiBpdGVtcyksXHJcbiAgICAgICAgdGFrZVVudGlsKHRoaXMuZGVzdHJveSQpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZShpdGVtcyA9PiB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3Rpb25FbmRlZC5lbWl0KGl0ZW1zKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKSB7XHJcbiAgICB0aGlzLmhvc3QuYm91bmRpbmdDbGllbnRSZWN0ID0gY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0KHRoaXMuaG9zdCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9jdXJzb3JXaXRoaW5Ib3N0KGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xyXG4gICAgcmV0dXJuIGN1cnNvcldpdGhpbkVsZW1lbnQoZXZlbnQsIHRoaXMuaG9zdCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9vbk1vdXNlVXAoKSB7XHJcbiAgICB0aGlzLl9mbHVzaEl0ZW1zKCk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUNsYXNzKGRvY3VtZW50LmJvZHksIE5PX1NFTEVDVF9DTEFTUyk7XHJcbiAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUNsYXNzKGRvY3VtZW50LmJvZHksIE5PX1NFTEVDVF9DTEFTU19NT0JJTEUpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfb25Nb3VzZURvd24oZXZlbnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSB7XHJcbiAgICBpZiAodGhpcy5zaG9ydGN1dHMuZGlzYWJsZVNlbGVjdGlvbihldmVudCkgfHwgdGhpcy5kaXNhYmxlZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgY2xlYXJTZWxlY3Rpb24od2luZG93KTtcclxuXHJcbiAgICBpZiAoIXRoaXMuZGlzYWJsZURyYWcgJiYgKCF0aGlzLmxvbmdUb3VjaCB8fCAhdGhpcy5fZGlzYWJsZWRCeUxvbmdUb3VjaCkpIHtcclxuICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhkb2N1bWVudC5ib2R5LCBOT19TRUxFQ1RfQ0xBU1MpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghdGhpcy5kaXNhYmxlRHJhZyAmJiB0aGlzLmlzTW9iaWxlRGV2aWNlKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3MoZG9jdW1lbnQuYm9keSwgTk9fU0VMRUNUX0NMQVNTX01PQklMRSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgbW91c2VQb2ludCA9IGdldE1vdXNlUG9zaXRpb24oZXZlbnQpO1xyXG5cclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICBjb25zdCBpdGVtUmVjdCA9IGl0ZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgIGNvbnN0IHdpdGhpbkJvdW5kaW5nQm94ID0gaW5Cb3VuZGluZ0JveChtb3VzZVBvaW50LCBpdGVtUmVjdCk7XHJcblxyXG4gICAgICBpZiAodGhpcy5zaG9ydGN1dHMuZXh0ZW5kZWRTZWxlY3Rpb25TaG9ydGN1dChldmVudCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IHNob3VsZEFkZCA9XHJcbiAgICAgICAgKHdpdGhpbkJvdW5kaW5nQm94ICYmXHJcbiAgICAgICAgICAhdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkgJiZcclxuICAgICAgICAgICF0aGlzLnNlbGVjdE1vZGUgJiZcclxuICAgICAgICAgICF0aGlzLnNlbGVjdFdpdGhTaG9ydGN1dCkgfHxcclxuICAgICAgICAod2l0aGluQm91bmRpbmdCb3ggJiYgdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkgJiYgIWl0ZW0uc2VsZWN0ZWQpIHx8XHJcbiAgICAgICAgKCF3aXRoaW5Cb3VuZGluZ0JveCAmJiB0aGlzLnNob3J0Y3V0cy50b2dnbGVTaW5nbGVJdGVtKGV2ZW50KSAmJiBpdGVtLnNlbGVjdGVkKSB8fFxyXG4gICAgICAgICh3aXRoaW5Cb3VuZGluZ0JveCAmJiAhaXRlbS5zZWxlY3RlZCAmJiB0aGlzLnNlbGVjdE1vZGUpIHx8XHJcbiAgICAgICAgKCF3aXRoaW5Cb3VuZGluZ0JveCAmJiBpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2VsZWN0TW9kZSk7XHJcblxyXG4gICAgICBjb25zdCBzaG91bGRSZW1vdmUgPVxyXG4gICAgICAgICghd2l0aGluQm91bmRpbmdCb3ggJiZcclxuICAgICAgICAgICF0aGlzLnNob3J0Y3V0cy50b2dnbGVTaW5nbGVJdGVtKGV2ZW50KSAmJlxyXG4gICAgICAgICAgIXRoaXMuc2VsZWN0TW9kZSAmJlxyXG4gICAgICAgICAgIXRoaXMuc2VsZWN0V2l0aFNob3J0Y3V0KSB8fFxyXG4gICAgICAgICghd2l0aGluQm91bmRpbmdCb3ggJiYgdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkgJiYgIWl0ZW0uc2VsZWN0ZWQpIHx8XHJcbiAgICAgICAgKHdpdGhpbkJvdW5kaW5nQm94ICYmIHRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpICYmIGl0ZW0uc2VsZWN0ZWQpIHx8XHJcbiAgICAgICAgKCF3aXRoaW5Cb3VuZGluZ0JveCAmJiAhaXRlbS5zZWxlY3RlZCAmJiB0aGlzLnNlbGVjdE1vZGUpIHx8XHJcbiAgICAgICAgKHdpdGhpbkJvdW5kaW5nQm94ICYmIGl0ZW0uc2VsZWN0ZWQgJiYgdGhpcy5zZWxlY3RNb2RlKTtcclxuXHJcbiAgICAgIGlmIChzaG91bGRBZGQpIHtcclxuICAgICAgICB0aGlzLl9zZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgICB9IGVsc2UgaWYgKHNob3VsZFJlbW92ZSkge1xyXG4gICAgICAgIHRoaXMuX2Rlc2VsZWN0SXRlbShpdGVtKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9zZWxlY3RJdGVtcyhldmVudDogRXZlbnQpIHtcclxuICAgIGNvbnN0IHNlbGVjdGlvbkJveCA9IGNhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCh0aGlzLiRzZWxlY3RCb3gubmF0aXZlRWxlbWVudCk7XHJcblxyXG4gICAgdGhpcy4kc2VsZWN0YWJsZUl0ZW1zLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLl9pc0V4dGVuZGVkU2VsZWN0aW9uKGV2ZW50KSkge1xyXG4gICAgICAgIHRoaXMuX2V4dGVuZGVkU2VsZWN0aW9uTW9kZShzZWxlY3Rpb25Cb3gsIGl0ZW0sIGV2ZW50KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLl9ub3JtYWxTZWxlY3Rpb25Nb2RlKHNlbGVjdGlvbkJveCwgaXRlbSwgZXZlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2lzRXh0ZW5kZWRTZWxlY3Rpb24oZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICByZXR1cm4gdGhpcy5zaG9ydGN1dHMuZXh0ZW5kZWRTZWxlY3Rpb25TaG9ydGN1dChldmVudCkgJiYgdGhpcy5zZWxlY3RPbkRyYWc7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9ub3JtYWxTZWxlY3Rpb25Nb2RlKHNlbGVjdEJveCwgaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSwgZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBjb25zdCBpblNlbGVjdGlvbiA9IGJveEludGVyc2VjdHMoc2VsZWN0Qm94LCBpdGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpKTtcclxuXHJcbiAgICBjb25zdCBzaG91bGRBZGQgPSBpblNlbGVjdGlvbiAmJiAhaXRlbS5zZWxlY3RlZCAmJiAhdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCk7XHJcblxyXG4gICAgY29uc3Qgc2hvdWxkUmVtb3ZlID1cclxuICAgICAgKCFpblNlbGVjdGlvbiAmJiBpdGVtLnNlbGVjdGVkICYmICF0aGlzLnNob3J0Y3V0cy5hZGRUb1NlbGVjdGlvbihldmVudCkpIHx8XHJcbiAgICAgIChpblNlbGVjdGlvbiAmJiBpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpKTtcclxuXHJcbiAgICBpZiAoc2hvdWxkQWRkKSB7XHJcbiAgICAgIHRoaXMuX3NlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICB9IGVsc2UgaWYgKHNob3VsZFJlbW92ZSkge1xyXG4gICAgICB0aGlzLl9kZXNlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9leHRlbmRlZFNlbGVjdGlvbk1vZGUoc2VsZWN0Qm94LCBpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlLCBldmVudDogRXZlbnQpIHtcclxuICAgIGNvbnN0IGluU2VsZWN0aW9uID0gYm94SW50ZXJzZWN0cyhzZWxlY3RCb3gsIGl0ZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkpO1xyXG5cclxuICAgIGNvbnN0IHNob3VkbEFkZCA9XHJcbiAgICAgIChpblNlbGVjdGlvbiAmJiAhaXRlbS5zZWxlY3RlZCAmJiAhdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCkgJiYgIXRoaXMuX3RtcEl0ZW1zLmhhcyhpdGVtKSkgfHxcclxuICAgICAgKGluU2VsZWN0aW9uICYmIGl0ZW0uc2VsZWN0ZWQgJiYgdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCkgJiYgIXRoaXMuX3RtcEl0ZW1zLmhhcyhpdGVtKSk7XHJcblxyXG4gICAgY29uc3Qgc2hvdWxkUmVtb3ZlID1cclxuICAgICAgKCFpblNlbGVjdGlvbiAmJiBpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2hvcnRjdXRzLmFkZFRvU2VsZWN0aW9uKGV2ZW50KSAmJiB0aGlzLl90bXBJdGVtcy5oYXMoaXRlbSkpIHx8XHJcbiAgICAgICghaW5TZWxlY3Rpb24gJiYgIWl0ZW0uc2VsZWN0ZWQgJiYgdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCkgJiYgdGhpcy5fdG1wSXRlbXMuaGFzKGl0ZW0pKTtcclxuXHJcbiAgICBpZiAoc2hvdWRsQWRkKSB7XHJcbiAgICAgIGl0ZW0uc2VsZWN0ZWQgPyBpdGVtLl9kZXNlbGVjdCgpIDogaXRlbS5fc2VsZWN0KCk7XHJcblxyXG4gICAgICBjb25zdCBhY3Rpb24gPSB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KVxyXG4gICAgICAgID8gQWN0aW9uLkRlbGV0ZVxyXG4gICAgICAgIDogdGhpcy5zaG9ydGN1dHMuYWRkVG9TZWxlY3Rpb24oZXZlbnQpXHJcbiAgICAgICAgICA/IEFjdGlvbi5BZGRcclxuICAgICAgICAgIDogQWN0aW9uLk5vbmU7XHJcblxyXG4gICAgICB0aGlzLl90bXBJdGVtcy5zZXQoaXRlbSwgYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoc2hvdWxkUmVtb3ZlKSB7XHJcbiAgICAgIHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpID8gaXRlbS5fc2VsZWN0KCkgOiBpdGVtLl9kZXNlbGVjdCgpO1xyXG4gICAgICB0aGlzLl90bXBJdGVtcy5kZWxldGUoaXRlbSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9mbHVzaEl0ZW1zKCkge1xyXG4gICAgdGhpcy5fdG1wSXRlbXMuZm9yRWFjaCgoYWN0aW9uLCBpdGVtKSA9PiB7XHJcbiAgICAgIGlmIChhY3Rpb24gPT09IEFjdGlvbi5BZGQpIHtcclxuICAgICAgICB0aGlzLl9zZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoYWN0aW9uID09PSBBY3Rpb24uRGVsZXRlKSB7XHJcbiAgICAgICAgdGhpcy5fZGVzZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLl90bXBJdGVtcy5jbGVhcigpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfYWRkSXRlbShpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlLCBzZWxlY3RlZEl0ZW1zOiBBcnJheTxhbnk+KSB7XHJcbiAgICBsZXQgc3VjY2VzcyA9IGZhbHNlO1xyXG5cclxuICAgIGlmICghdGhpcy5fZGlzYWJsZWRCeUxvbmdUb3VjaCAmJiAhdGhpcy5faGFzSXRlbShpdGVtLCBzZWxlY3RlZEl0ZW1zKSkge1xyXG4gICAgICBzdWNjZXNzID0gdHJ1ZTtcclxuICAgICAgc2VsZWN0ZWRJdGVtcy5wdXNoKGl0ZW0udmFsdWUpO1xyXG4gICAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zJC5uZXh0KHNlbGVjdGVkSXRlbXMpO1xyXG4gICAgICB0aGlzLml0ZW1TZWxlY3RlZC5lbWl0KGl0ZW0udmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBzdWNjZXNzO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfcmVtb3ZlSXRlbShpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlLCBzZWxlY3RlZEl0ZW1zOiBBcnJheTxhbnk+KSB7XHJcbiAgICBsZXQgc3VjY2VzcyA9IGZhbHNlO1xyXG4gICAgY29uc3QgdmFsdWUgPSBpdGVtIGluc3RhbmNlb2YgU2VsZWN0SXRlbURpcmVjdGl2ZSA/IGl0ZW0udmFsdWUgOiBpdGVtO1xyXG4gICAgY29uc3QgaW5kZXggPSBzZWxlY3RlZEl0ZW1zLmluZGV4T2YodmFsdWUpO1xyXG5cclxuICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgIHN1Y2Nlc3MgPSB0cnVlO1xyXG4gICAgICBzZWxlY3RlZEl0ZW1zLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgIHRoaXMuX3NlbGVjdGVkSXRlbXMkLm5leHQoc2VsZWN0ZWRJdGVtcyk7XHJcbiAgICAgIHRoaXMuaXRlbURlc2VsZWN0ZWQuZW1pdChpdGVtLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gc3VjY2VzcztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3RvZ2dsZUl0ZW0oaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSkge1xyXG4gICAgaWYgKGl0ZW0uc2VsZWN0ZWQpIHtcclxuICAgICAgdGhpcy5fZGVzZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5fc2VsZWN0SXRlbShpdGVtKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3NlbGVjdEl0ZW0oaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSkge1xyXG4gICAgdGhpcy51cGRhdGVJdGVtcyQubmV4dCh7IHR5cGU6IFVwZGF0ZUFjdGlvbnMuQWRkLCBpdGVtIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZGVzZWxlY3RJdGVtKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpIHtcclxuICAgIHRoaXMudXBkYXRlSXRlbXMkLm5leHQoeyB0eXBlOiBVcGRhdGVBY3Rpb25zLlJlbW92ZSwgaXRlbSB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2hhc0l0ZW0oaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSwgc2VsZWN0ZWRJdGVtczogQXJyYXk8YW55Pikge1xyXG4gICAgcmV0dXJuIHNlbGVjdGVkSXRlbXMuaW5jbHVkZXMoaXRlbS52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9kcmF3UmlwcGxlKGV2ZW50OiBUb3VjaEV2ZW50KSB7XHJcbiAgICBjb25zdCBwb3NpdGlvbiA9IGdldE1vdXNlUG9zaXRpb24oZXZlbnQpO1xyXG5cclxuICAgIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucmlwcGxlJyk7XHJcblxyXG4gICAgY29uc3QgbmV3Tm9kZSA9IG5vZGUuY2xvbmVOb2RlKHRydWUpIGFzIEhUTUxFbGVtZW50O1xyXG4gICAgbmV3Tm9kZS5jbGFzc0xpc3QuYWRkKCdhbmltYXRlJyk7XHJcbiAgICBuZXdOb2RlLnN0eWxlLmxlZnQgPSBwb3NpdGlvbi54IC0gNSArICdweCc7XHJcbiAgICBuZXdOb2RlLnN0eWxlLnRvcCA9IHBvc2l0aW9uLnkgLSA1ICsgJ3B4JztcclxuXHJcbiAgICBub2RlLnBhcmVudE5vZGUucmVwbGFjZUNoaWxkKG5ld05vZGUsIG5vZGUpO1xyXG4gIH07XHJcblxyXG4gIHByaXZhdGUgX2FjdGl2YXRlU2VsZWN0aW5nQWZ0ZXJMb25nVG91Y2goZXZlbnQ6IFRvdWNoRXZlbnQpIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2ggPSBmYWxzZTtcclxuICAgIGNsZWFySW50ZXJ2YWwodGhpcy5fbG9uZ1RvdWNoVGltZXIpO1xyXG4gICAgdGhpcy5fbG9uZ1RvdWNoVGltZXIgPSBudWxsO1xyXG4gICAgdGhpcy4kc2VsZWN0Qm94Lm5hdGl2ZUVsZW1lbnQuc3R5bGUub3BhY2l0eSA9IDE7XHJcblxyXG4gICAgaWYgKCF0aGlzLmRpc2FibGVEcmFnKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3MoZG9jdW1lbnQuYm9keSwgTk9fU0VMRUNUX0NMQVNTKTtcclxuICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhkb2N1bWVudC5ib2R5LCBOT19TRUxFQ1RfQ0xBU1NfTU9CSUxFKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl9kcmF3UmlwcGxlKGV2ZW50KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2RlYWN0aXZhdGVTZWxlY3RpbmdBZnRlckxvbmdUb3VjaCgpIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2ggPSB0cnVlO1xyXG4gICAgdGhpcy4kc2VsZWN0Qm94Lm5hdGl2ZUVsZW1lbnQuc3R5bGUub3BhY2l0eSA9IDA7XHJcbiAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gJ2F1dG8nO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBsb2cobXNnOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXNzYWdlcycpLnN0eWxlLndoaXRlU3BhY2UgPSAnbm9ybWFsJztcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXNzYWdlcycpLmlubmVySFRNTCArPSAnICcgKyBtc2c7XHJcbiAgfVxyXG59XHJcbiJdfQ==