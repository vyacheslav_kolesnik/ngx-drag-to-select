/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export var AUDIT_TIME = 16;
/** @type {?} */
export var MIN_WIDTH = 5;
/** @type {?} */
export var MIN_HEIGHT = 5;
/** @type {?} */
export var NO_SELECT_CLASS = 'dts-no-select';
/** @type {?} */
export var NO_SELECT_CLASS_MOBILE = 'dts-no-select-mobile';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL2NvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE1BQU0sS0FBTyxVQUFVLEdBQUcsRUFBRTs7QUFDNUIsTUFBTSxLQUFPLFNBQVMsR0FBRyxDQUFDOztBQUMxQixNQUFNLEtBQU8sVUFBVSxHQUFHLENBQUM7O0FBQzNCLE1BQU0sS0FBTyxlQUFlLEdBQUcsZUFBZTs7QUFDOUMsTUFBTSxLQUFPLHNCQUFzQixHQUFHLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBBVURJVF9USU1FID0gMTY7XHJcbmV4cG9ydCBjb25zdCBNSU5fV0lEVEggPSA1O1xyXG5leHBvcnQgY29uc3QgTUlOX0hFSUdIVCA9IDU7XHJcbmV4cG9ydCBjb25zdCBOT19TRUxFQ1RfQ0xBU1MgPSAnZHRzLW5vLXNlbGVjdCc7XHJcbmV4cG9ydCBjb25zdCBOT19TRUxFQ1RfQ0xBU1NfTU9CSUxFID0gJ2R0cy1uby1zZWxlY3QtbW9iaWxlJztcclxuIl19