/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { isPlatformBrowser } from '@angular/common';
import { Directive, ElementRef, Inject, Input, PLATFORM_ID, Renderer2 } from '@angular/core';
import { CONFIG } from './tokens';
import { calculateBoundingClientRect } from './utils';
var SelectItemDirective = /** @class */ (function () {
    function SelectItemDirective(config, platformId, host, renderer) {
        this.config = config;
        this.platformId = platformId;
        this.host = host;
        this.renderer = renderer;
        this.selected = false;
    }
    Object.defineProperty(SelectItemDirective.prototype, "value", {
        get: /**
         * @return {?}
         */
        function () {
            return this.dtsSelectItem ? this.dtsSelectItem : this;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    SelectItemDirective.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        this.applySelectedClass();
    };
    /**
     * @return {?}
     */
    SelectItemDirective.prototype.getBoundingClientRect = /**
     * @return {?}
     */
    function () {
        if (isPlatformBrowser(this.platformId) && !this._boundingClientRect) {
            this.calculateBoundingClientRect();
        }
        return this._boundingClientRect;
    };
    /**
     * @return {?}
     */
    SelectItemDirective.prototype.calculateBoundingClientRect = /**
     * @return {?}
     */
    function () {
        this._boundingClientRect = calculateBoundingClientRect(this.host.nativeElement);
    };
    /**
     * @return {?}
     */
    SelectItemDirective.prototype._select = /**
     * @return {?}
     */
    function () {
        this.selected = true;
    };
    /**
     * @return {?}
     */
    SelectItemDirective.prototype._deselect = /**
     * @return {?}
     */
    function () {
        this.selected = false;
    };
    /**
     * @private
     * @return {?}
     */
    SelectItemDirective.prototype.applySelectedClass = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.selected) {
            this.renderer.addClass(this.host.nativeElement, this.config.selectedClass);
        }
        else {
            this.renderer.removeClass(this.host.nativeElement, this.config.selectedClass);
        }
    };
    SelectItemDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[dtsSelectItem]',
                    exportAs: 'dtsSelectItem',
                    host: {
                        class: 'dts-select-item'
                    }
                },] }
    ];
    /** @nocollapse */
    SelectItemDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] },
        { type: undefined, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] },
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    SelectItemDirective.propDecorators = {
        dtsSelectItem: [{ type: Input }]
    };
    return SelectItemDirective;
}());
export { SelectItemDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype._boundingClientRect;
    /** @type {?} */
    SelectItemDirective.prototype.selected;
    /** @type {?} */
    SelectItemDirective.prototype.dtsSelectItem;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.platformId;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.host;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWl0ZW0uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL3NlbGVjdC1pdGVtLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFcEQsT0FBTyxFQUFFLFNBQVMsRUFBVyxVQUFVLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR3RHLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDbEMsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXREO0lBbUJFLDZCQUMwQixNQUEwQixFQUNyQixVQUFVLEVBQy9CLElBQWdCLEVBQ2hCLFFBQW1CO1FBSEgsV0FBTSxHQUFOLE1BQU0sQ0FBb0I7UUFDckIsZUFBVSxHQUFWLFVBQVUsQ0FBQTtRQUMvQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFiN0IsYUFBUSxHQUFHLEtBQUssQ0FBQztJQWNkLENBQUM7SUFUSixzQkFBSSxzQ0FBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDeEQsQ0FBQzs7O09BQUE7Ozs7SUFTRCx1Q0FBUzs7O0lBQVQ7UUFDRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsbURBQXFCOzs7SUFBckI7UUFDRSxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUNuRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztTQUNwQztRQUNELE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCx5REFBMkI7OztJQUEzQjtRQUNFLElBQUksQ0FBQyxtQkFBbUIsR0FBRywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ2xGLENBQUM7Ozs7SUFFRCxxQ0FBTzs7O0lBQVA7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN2QixDQUFDOzs7O0lBRUQsdUNBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFTyxnREFBa0I7Ozs7SUFBMUI7UUFDRSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUM1RTthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUMvRTtJQUNILENBQUM7O2dCQXZERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsaUJBQWlCO3FCQUN6QjtpQkFDRjs7OztnREFjSSxNQUFNLFNBQUMsTUFBTTtnREFDYixNQUFNLFNBQUMsV0FBVztnQkEzQk0sVUFBVTtnQkFBOEIsU0FBUzs7O2dDQWtCM0UsS0FBSzs7SUE0Q1IsMEJBQUM7Q0FBQSxBQXhERCxJQXdEQztTQWpEWSxtQkFBbUI7Ozs7OztJQUM5QixrREFBNEI7O0lBRTVCLHVDQUFpQjs7SUFFakIsNENBQ2M7Ozs7O0lBT1oscUNBQWtEOzs7OztJQUNsRCx5Q0FBdUM7Ozs7O0lBQ3ZDLG1DQUF3Qjs7Ozs7SUFDeEIsdUNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaXNQbGF0Zm9ybUJyb3dzZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgRGlyZWN0aXZlLCBEb0NoZWNrLCBFbGVtZW50UmVmLCBJbmplY3QsIElucHV0LCBQTEFURk9STV9JRCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBEcmFnVG9TZWxlY3RDb25maWcgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IENPTkZJRyB9IGZyb20gJy4vdG9rZW5zJztcclxuaW1wb3J0IHsgY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0IH0gZnJvbSAnLi91dGlscyc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tkdHNTZWxlY3RJdGVtXScsXHJcbiAgZXhwb3J0QXM6ICdkdHNTZWxlY3RJdGVtJyxcclxuICBob3N0OiB7XHJcbiAgICBjbGFzczogJ2R0cy1zZWxlY3QtaXRlbSdcclxuICB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RJdGVtRGlyZWN0aXZlIGltcGxlbWVudHMgRG9DaGVjayB7XHJcbiAgcHJpdmF0ZSBfYm91bmRpbmdDbGllbnRSZWN0O1xyXG5cclxuICBzZWxlY3RlZCA9IGZhbHNlO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIGR0c1NlbGVjdEl0ZW07XHJcblxyXG4gIGdldCB2YWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLmR0c1NlbGVjdEl0ZW0gPyB0aGlzLmR0c1NlbGVjdEl0ZW0gOiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPTkZJRykgcHJpdmF0ZSBjb25maWc6IERyYWdUb1NlbGVjdENvbmZpZyxcclxuICAgIEBJbmplY3QoUExBVEZPUk1fSUQpIHByaXZhdGUgcGxhdGZvcm1JZCxcclxuICAgIHByaXZhdGUgaG9zdDogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICkge31cclxuXHJcbiAgbmdEb0NoZWNrKCkge1xyXG4gICAgdGhpcy5hcHBseVNlbGVjdGVkQ2xhc3MoKTtcclxuICB9XHJcblxyXG4gIGdldEJvdW5kaW5nQ2xpZW50UmVjdCgpIHtcclxuICAgIGlmIChpc1BsYXRmb3JtQnJvd3Nlcih0aGlzLnBsYXRmb3JtSWQpICYmICF0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3QpIHtcclxuICAgICAgdGhpcy5jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3Q7XHJcbiAgfVxyXG5cclxuICBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKSB7XHJcbiAgICB0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3QgPSBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QodGhpcy5ob3N0Lm5hdGl2ZUVsZW1lbnQpO1xyXG4gIH1cclxuXHJcbiAgX3NlbGVjdCgpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWQgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgX2Rlc2VsZWN0KCkge1xyXG4gICAgdGhpcy5zZWxlY3RlZCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhcHBseVNlbGVjdGVkQ2xhc3MoKSB7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZCkge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuaG9zdC5uYXRpdmVFbGVtZW50LCB0aGlzLmNvbmZpZy5zZWxlY3RlZENsYXNzKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5ob3N0Lm5hdGl2ZUVsZW1lbnQsIHRoaXMuY29uZmlnLnNlbGVjdGVkQ2xhc3MpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=