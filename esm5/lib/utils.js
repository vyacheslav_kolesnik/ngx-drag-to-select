/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MIN_HEIGHT, MIN_WIDTH } from './constants';
/** @type {?} */
export var isObject = (/**
 * @param {?} item
 * @return {?}
 */
function (item) {
    return item && typeof item === 'object' && !Array.isArray(item) && item !== null;
});
/**
 * @param {?} target
 * @param {?} source
 * @return {?}
 */
export function mergeDeep(target, source) {
    if (isObject(target) && isObject(source)) {
        Object.keys(source).forEach((/**
         * @param {?} key
         * @return {?}
         */
        function (key) {
            var _a, _b;
            if (isObject(source[key])) {
                if (!target[key]) {
                    Object.assign(target, (_a = {}, _a[key] = {}, _a));
                }
                mergeDeep(target[key], source[key]);
            }
            else {
                Object.assign(target, (_b = {}, _b[key] = source[key], _b));
            }
        }));
    }
    return target;
}
/** @type {?} */
export var hasMinimumSize = (/**
 * @param {?} selectBox
 * @param {?=} minWidth
 * @param {?=} minHeight
 * @return {?}
 */
function (selectBox, minWidth, minHeight) {
    if (minWidth === void 0) { minWidth = MIN_WIDTH; }
    if (minHeight === void 0) { minHeight = MIN_HEIGHT; }
    return selectBox.width > minWidth || selectBox.height > minHeight;
});
/** @type {?} */
export var clearSelection = (/**
 * @param {?} window
 * @return {?}
 */
function (window) {
    /** @type {?} */
    var selection = window.getSelection();
    if (!selection) {
        return;
    }
    if (selection.removeAllRanges) {
        selection.removeAllRanges();
    }
    else if (selection.empty) {
        selection.empty();
    }
});
/** @type {?} */
export var inBoundingBox = (/**
 * @param {?} point
 * @param {?} box
 * @return {?}
 */
function (point, box) {
    return (box.left <= point.x && point.x <= box.left + box.width && box.top <= point.y && point.y <= box.top + box.height);
});
/** @type {?} */
export var boxIntersects = (/**
 * @param {?} boxA
 * @param {?} boxB
 * @return {?}
 */
function (boxA, boxB) {
    return (boxA.left <= boxB.left + boxB.width &&
        boxA.left + boxA.width >= boxB.left &&
        boxA.top <= boxB.top + boxB.height &&
        boxA.top + boxA.height >= boxB.top);
});
/** @type {?} */
export var calculateBoundingClientRect = (/**
 * @param {?} element
 * @return {?}
 */
function (element) {
    return element.getBoundingClientRect();
});
/** @type {?} */
export var getMousePosition = (/**
 * @param {?} event
 * @return {?}
 */
function (event) {
    return event instanceof MouseEvent
        ? {
            x: event.clientX,
            y: event.clientY
        }
        : {
            x: event.touches[0].clientX,
            y: event.touches[0].clientY
        };
});
/** @type {?} */
export var getScroll = (/**
 * @return {?}
 */
function () {
    if (!document || !document.documentElement) {
        return {
            x: 0,
            y: 0
        };
    }
    return {
        x: document.documentElement.scrollLeft || document.body.scrollLeft,
        y: document.documentElement.scrollTop || document.body.scrollTop
    };
});
/** @type {?} */
export var getRelativeMousePosition = (/**
 * @param {?} event
 * @param {?} container
 * @return {?}
 */
function (event, container) {
    var _a = getMousePosition(event), clientX = _a.x, clientY = _a.y;
    /** @type {?} */
    var scroll = getScroll();
    /** @type {?} */
    var borderSize = (container.boundingClientRect.width - container.clientWidth) / 2;
    /** @type {?} */
    var offsetLeft = container.boundingClientRect.left + scroll.x;
    /** @type {?} */
    var offsetTop = container.boundingClientRect.top + scroll.y;
    return {
        x: clientX - borderSize - (offsetLeft - window.pageXOffset) + container.scrollLeft,
        y: clientY - borderSize - (offsetTop - window.pageYOffset) + container.scrollTop
    };
});
/** @type {?} */
export var cursorWithinElement = (/**
 * @param {?} event
 * @param {?} element
 * @return {?}
 */
function (event, element) {
    /** @type {?} */
    var mousePoint = getMousePosition(event);
    return inBoundingBox(mousePoint, calculateBoundingClientRect(element));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZHJhZy10by1zZWxlY3QvIiwic291cmNlcyI6WyJsaWIvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDOztBQUdwRCxNQUFNLEtBQU8sUUFBUTs7OztBQUFHLFVBQUMsSUFBUztJQUNoQyxPQUFPLElBQUksSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxJQUFJLENBQUM7QUFDbkYsQ0FBQyxDQUFBOzs7Ozs7QUFFRCxNQUFNLFVBQVUsU0FBUyxDQUFDLE1BQWMsRUFBRSxNQUFjO0lBQ3RELElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRTtRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLEdBQUc7O1lBQzdCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNoQixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sWUFBSSxHQUFDLEdBQUcsSUFBRyxFQUFFLE1BQUcsQ0FBQztpQkFDdEM7Z0JBQ0QsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sWUFBSSxHQUFDLEdBQUcsSUFBRyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQUcsQ0FBQzthQUMvQztRQUNILENBQUMsRUFBQyxDQUFDO0tBQ0o7SUFFRCxPQUFPLE1BQU0sQ0FBQztBQUNoQixDQUFDOztBQUVELE1BQU0sS0FBTyxjQUFjOzs7Ozs7QUFBRyxVQUFDLFNBQTRCLEVBQUUsUUFBb0IsRUFBRSxTQUFzQjtJQUE1Qyx5QkFBQSxFQUFBLG9CQUFvQjtJQUFFLDBCQUFBLEVBQUEsc0JBQXNCO0lBQ3ZHLE9BQU8sU0FBUyxDQUFDLEtBQUssR0FBRyxRQUFRLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUM7QUFDcEUsQ0FBQyxDQUFBOztBQUVELE1BQU0sS0FBTyxjQUFjOzs7O0FBQUcsVUFBQyxNQUFjOztRQUNyQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFlBQVksRUFBRTtJQUV2QyxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ2QsT0FBTztLQUNSO0lBRUQsSUFBSSxTQUFTLENBQUMsZUFBZSxFQUFFO1FBQzdCLFNBQVMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztLQUM3QjtTQUFNLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRTtRQUMxQixTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDbkI7QUFDSCxDQUFDLENBQUE7O0FBRUQsTUFBTSxLQUFPLGFBQWE7Ozs7O0FBQUcsVUFBQyxLQUFvQixFQUFFLEdBQWdCO0lBQ2xFLE9BQU8sQ0FDTCxHQUFHLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUNoSCxDQUFDO0FBQ0osQ0FBQyxDQUFBOztBQUVELE1BQU0sS0FBTyxhQUFhOzs7OztBQUFHLFVBQUMsSUFBaUIsRUFBRSxJQUFpQjtJQUNoRSxPQUFPLENBQ0wsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLO1FBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSTtRQUNuQyxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU07UUFDbEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQ25DLENBQUM7QUFDSixDQUFDLENBQUE7O0FBRUQsTUFBTSxLQUFPLDJCQUEyQjs7OztBQUFHLFVBQUMsT0FBb0I7SUFDOUQsT0FBTyxPQUFPLENBQUMscUJBQXFCLEVBQUUsQ0FBQztBQUN6QyxDQUFDLENBQUE7O0FBRUQsTUFBTSxLQUFPLGdCQUFnQjs7OztBQUFHLFVBQUMsS0FBOEI7SUFDN0QsT0FBTyxLQUFLLFlBQVksVUFBVTtRQUNoQyxDQUFDLENBQUM7WUFDRSxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU87WUFDaEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPO1NBQ2pCO1FBQ0gsQ0FBQyxDQUFDO1lBQ0UsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUMzQixDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO1NBQzVCLENBQUM7QUFDUixDQUFDLENBQUE7O0FBRUQsTUFBTSxLQUFPLFNBQVM7OztBQUFHO0lBQ3ZCLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFO1FBQzFDLE9BQU87WUFDTCxDQUFDLEVBQUUsQ0FBQztZQUNKLENBQUMsRUFBRSxDQUFDO1NBQ0wsQ0FBQztLQUNIO0lBRUQsT0FBTztRQUNMLENBQUMsRUFBRSxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVU7UUFDbEUsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUztLQUNqRSxDQUFDO0FBQ0osQ0FBQyxDQUFBOztBQUVELE1BQU0sS0FBTyx3QkFBd0I7Ozs7O0FBQUcsVUFDdEMsS0FBOEIsRUFDOUIsU0FBOEI7SUFFeEIsSUFBQSw0QkFBb0QsRUFBbEQsY0FBVSxFQUFFLGNBQXNDOztRQUNwRCxNQUFNLEdBQUcsU0FBUyxFQUFFOztRQUVwQixVQUFVLEdBQUcsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDOztRQUM3RSxVQUFVLEdBQUcsU0FBUyxDQUFDLGtCQUFrQixDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQzs7UUFDekQsU0FBUyxHQUFHLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUM7SUFFN0QsT0FBTztRQUNMLENBQUMsRUFBRSxPQUFPLEdBQUcsVUFBVSxHQUFHLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUMsVUFBVTtRQUNsRixDQUFDLEVBQUUsT0FBTyxHQUFHLFVBQVUsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsU0FBUyxDQUFDLFNBQVM7S0FDakYsQ0FBQztBQUNKLENBQUMsQ0FBQTs7QUFFRCxNQUFNLEtBQU8sbUJBQW1COzs7OztBQUFHLFVBQUMsS0FBOEIsRUFBRSxPQUFvQjs7UUFDaEYsVUFBVSxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQztJQUMxQyxPQUFPLGFBQWEsQ0FBQyxVQUFVLEVBQUUsMkJBQTJCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUN6RSxDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNSU5fSEVJR0hULCBNSU5fV0lEVEggfSBmcm9tICcuL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IEJvdW5kaW5nQm94LCBNb3VzZVBvc2l0aW9uLCBTZWxlY3RCb3gsIFNlbGVjdENvbnRhaW5lckhvc3QgfSBmcm9tICcuL21vZGVscyc7XHJcblxyXG5leHBvcnQgY29uc3QgaXNPYmplY3QgPSAoaXRlbTogYW55KSA9PiB7XHJcbiAgcmV0dXJuIGl0ZW0gJiYgdHlwZW9mIGl0ZW0gPT09ICdvYmplY3QnICYmICFBcnJheS5pc0FycmF5KGl0ZW0pICYmIGl0ZW0gIT09IG51bGw7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbWVyZ2VEZWVwKHRhcmdldDogT2JqZWN0LCBzb3VyY2U6IE9iamVjdCkge1xyXG4gIGlmIChpc09iamVjdCh0YXJnZXQpICYmIGlzT2JqZWN0KHNvdXJjZSkpIHtcclxuICAgIE9iamVjdC5rZXlzKHNvdXJjZSkuZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICBpZiAoaXNPYmplY3Qoc291cmNlW2tleV0pKSB7XHJcbiAgICAgICAgaWYgKCF0YXJnZXRba2V5XSkge1xyXG4gICAgICAgICAgT2JqZWN0LmFzc2lnbih0YXJnZXQsIHsgW2tleV06IHt9IH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBtZXJnZURlZXAodGFyZ2V0W2tleV0sIHNvdXJjZVtrZXldKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBPYmplY3QuYXNzaWduKHRhcmdldCwgeyBba2V5XTogc291cmNlW2tleV0gfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHRhcmdldDtcclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGhhc01pbmltdW1TaXplID0gKHNlbGVjdEJveDogU2VsZWN0Qm94PG51bWJlcj4sIG1pbldpZHRoID0gTUlOX1dJRFRILCBtaW5IZWlnaHQgPSBNSU5fSEVJR0hUKSA9PiB7XHJcbiAgcmV0dXJuIHNlbGVjdEJveC53aWR0aCA+IG1pbldpZHRoIHx8IHNlbGVjdEJveC5oZWlnaHQgPiBtaW5IZWlnaHQ7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY2xlYXJTZWxlY3Rpb24gPSAod2luZG93OiBXaW5kb3cpID0+IHtcclxuICBjb25zdCBzZWxlY3Rpb24gPSB3aW5kb3cuZ2V0U2VsZWN0aW9uKCk7XHJcblxyXG4gIGlmICghc2VsZWN0aW9uKSB7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBpZiAoc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcykge1xyXG4gICAgc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xyXG4gIH0gZWxzZSBpZiAoc2VsZWN0aW9uLmVtcHR5KSB7XHJcbiAgICBzZWxlY3Rpb24uZW1wdHkoKTtcclxuICB9XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgaW5Cb3VuZGluZ0JveCA9IChwb2ludDogTW91c2VQb3NpdGlvbiwgYm94OiBCb3VuZGluZ0JveCkgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICBib3gubGVmdCA8PSBwb2ludC54ICYmIHBvaW50LnggPD0gYm94LmxlZnQgKyBib3gud2lkdGggJiYgYm94LnRvcCA8PSBwb2ludC55ICYmIHBvaW50LnkgPD0gYm94LnRvcCArIGJveC5oZWlnaHRcclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGJveEludGVyc2VjdHMgPSAoYm94QTogQm91bmRpbmdCb3gsIGJveEI6IEJvdW5kaW5nQm94KSA9PiB7XHJcbiAgcmV0dXJuIChcclxuICAgIGJveEEubGVmdCA8PSBib3hCLmxlZnQgKyBib3hCLndpZHRoICYmXHJcbiAgICBib3hBLmxlZnQgKyBib3hBLndpZHRoID49IGJveEIubGVmdCAmJlxyXG4gICAgYm94QS50b3AgPD0gYm94Qi50b3AgKyBib3hCLmhlaWdodCAmJlxyXG4gICAgYm94QS50b3AgKyBib3hBLmhlaWdodCA+PSBib3hCLnRvcFxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0ID0gKGVsZW1lbnQ6IEhUTUxFbGVtZW50KTogQm91bmRpbmdCb3ggPT4ge1xyXG4gIHJldHVybiBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldE1vdXNlUG9zaXRpb24gPSAoZXZlbnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSA9PiB7XHJcbiAgcmV0dXJuIGV2ZW50IGluc3RhbmNlb2YgTW91c2VFdmVudFxyXG4gICAgPyB7XHJcbiAgICAgICAgeDogZXZlbnQuY2xpZW50WCxcclxuICAgICAgICB5OiBldmVudC5jbGllbnRZXHJcbiAgICAgIH1cclxuICAgIDoge1xyXG4gICAgICAgIHg6IGV2ZW50LnRvdWNoZXNbMF0uY2xpZW50WCxcclxuICAgICAgICB5OiBldmVudC50b3VjaGVzWzBdLmNsaWVudFlcclxuICAgICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTY3JvbGwgPSAoKSA9PiB7XHJcbiAgaWYgKCFkb2N1bWVudCB8fCAhZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICB4OiAwLFxyXG4gICAgICB5OiAwXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHg6IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0IHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdCxcclxuICAgIHk6IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgfHwgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3BcclxuICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbiA9IChcclxuICBldmVudDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQsXHJcbiAgY29udGFpbmVyOiBTZWxlY3RDb250YWluZXJIb3N0XHJcbik6IE1vdXNlUG9zaXRpb24gPT4ge1xyXG4gIGNvbnN0IHsgeDogY2xpZW50WCwgeTogY2xpZW50WSB9ID0gZ2V0TW91c2VQb3NpdGlvbihldmVudCk7XHJcbiAgY29uc3Qgc2Nyb2xsID0gZ2V0U2Nyb2xsKCk7XHJcblxyXG4gIGNvbnN0IGJvcmRlclNpemUgPSAoY29udGFpbmVyLmJvdW5kaW5nQ2xpZW50UmVjdC53aWR0aCAtIGNvbnRhaW5lci5jbGllbnRXaWR0aCkgLyAyO1xyXG4gIGNvbnN0IG9mZnNldExlZnQgPSBjb250YWluZXIuYm91bmRpbmdDbGllbnRSZWN0LmxlZnQgKyBzY3JvbGwueDtcclxuICBjb25zdCBvZmZzZXRUb3AgPSBjb250YWluZXIuYm91bmRpbmdDbGllbnRSZWN0LnRvcCArIHNjcm9sbC55O1xyXG5cclxuICByZXR1cm4ge1xyXG4gICAgeDogY2xpZW50WCAtIGJvcmRlclNpemUgLSAob2Zmc2V0TGVmdCAtIHdpbmRvdy5wYWdlWE9mZnNldCkgKyBjb250YWluZXIuc2Nyb2xsTGVmdCxcclxuICAgIHk6IGNsaWVudFkgLSBib3JkZXJTaXplIC0gKG9mZnNldFRvcCAtIHdpbmRvdy5wYWdlWU9mZnNldCkgKyBjb250YWluZXIuc2Nyb2xsVG9wXHJcbiAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjdXJzb3JXaXRoaW5FbGVtZW50ID0gKGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCwgZWxlbWVudDogSFRNTEVsZW1lbnQpID0+IHtcclxuICBjb25zdCBtb3VzZVBvaW50ID0gZ2V0TW91c2VQb3NpdGlvbihldmVudCk7XHJcbiAgcmV0dXJuIGluQm91bmRpbmdCb3gobW91c2VQb2ludCwgY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0KGVsZW1lbnQpKTtcclxufTtcclxuIl19