/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectContainerComponent } from './select-container.component';
import { SelectItemDirective } from './select-item.directive';
import { ShortcutService } from './shortcut.service';
import { CONFIG, USER_CONFIG } from './tokens';
import { DEFAULT_CONFIG } from './config';
import { mergeDeep } from './utils';
/** @type {?} */
var COMPONENTS = [SelectContainerComponent, SelectItemDirective];
/**
 * @param {?} config
 * @return {?}
 */
export function CONFIG_FACTORY(config) {
    return mergeDeep(DEFAULT_CONFIG, config);
}
var DragToSelectModule = /** @class */ (function () {
    function DragToSelectModule() {
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    DragToSelectModule.forRoot = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: DragToSelectModule,
            providers: [
                ShortcutService,
                { provide: USER_CONFIG, useValue: config },
                {
                    provide: CONFIG,
                    useFactory: CONFIG_FACTORY,
                    deps: [USER_CONFIG]
                }
            ]
        };
    };
    DragToSelectModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule],
                    declarations: tslib_1.__spread(COMPONENTS),
                    exports: tslib_1.__spread(COMPONENTS)
                },] }
    ];
    return DragToSelectModule;
}());
export { DragToSelectModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhZy10by1zZWxlY3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL2RyYWctdG8tc2VsZWN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN4RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFckQsT0FBTyxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDL0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUMxQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sU0FBUyxDQUFDOztJQUU5QixVQUFVLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxtQkFBbUIsQ0FBQzs7Ozs7QUFFbEUsTUFBTSxVQUFVLGNBQWMsQ0FBQyxNQUFtQztJQUNoRSxPQUFPLFNBQVMsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDM0MsQ0FBQztBQUVEO0lBQUE7SUFvQkEsQ0FBQzs7Ozs7SUFkUSwwQkFBTzs7OztJQUFkLFVBQWUsTUFBd0M7UUFBeEMsdUJBQUEsRUFBQSxXQUF3QztRQUNyRCxPQUFPO1lBQ0wsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixTQUFTLEVBQUU7Z0JBQ1QsZUFBZTtnQkFDZixFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtnQkFDMUM7b0JBQ0UsT0FBTyxFQUFFLE1BQU07b0JBQ2YsVUFBVSxFQUFFLGNBQWM7b0JBQzFCLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQztpQkFDcEI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOztnQkFuQkYsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsWUFBWSxtQkFBTSxVQUFVLENBQUM7b0JBQzdCLE9BQU8sbUJBQU0sVUFBVSxDQUFDO2lCQUN6Qjs7SUFnQkQseUJBQUM7Q0FBQSxBQXBCRCxJQW9CQztTQWZZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBTZWxlY3RDb250YWluZXJDb21wb25lbnQgfSBmcm9tICcuL3NlbGVjdC1jb250YWluZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2VsZWN0SXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vc2VsZWN0LWl0ZW0uZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU2hvcnRjdXRTZXJ2aWNlIH0gZnJvbSAnLi9zaG9ydGN1dC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRHJhZ1RvU2VsZWN0Q29uZmlnIH0gZnJvbSAnLi9tb2RlbHMnO1xyXG5pbXBvcnQgeyBDT05GSUcsIFVTRVJfQ09ORklHIH0gZnJvbSAnLi90b2tlbnMnO1xyXG5pbXBvcnQgeyBERUZBVUxUX0NPTkZJRyB9IGZyb20gJy4vY29uZmlnJztcclxuaW1wb3J0IHsgbWVyZ2VEZWVwIH0gZnJvbSAnLi91dGlscyc7XHJcblxyXG5jb25zdCBDT01QT05FTlRTID0gW1NlbGVjdENvbnRhaW5lckNvbXBvbmVudCwgU2VsZWN0SXRlbURpcmVjdGl2ZV07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gQ09ORklHX0ZBQ1RPUlkoY29uZmlnOiBQYXJ0aWFsPERyYWdUb1NlbGVjdENvbmZpZz4pIHtcclxuICByZXR1cm4gbWVyZ2VEZWVwKERFRkFVTFRfQ09ORklHLCBjb25maWcpO1xyXG59XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gIGRlY2xhcmF0aW9uczogWy4uLkNPTVBPTkVOVFNdLFxyXG4gIGV4cG9ydHM6IFsuLi5DT01QT05FTlRTXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJhZ1RvU2VsZWN0TW9kdWxlIHtcclxuICBzdGF0aWMgZm9yUm9vdChjb25maWc6IFBhcnRpYWw8RHJhZ1RvU2VsZWN0Q29uZmlnPiA9IHt9KTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBuZ01vZHVsZTogRHJhZ1RvU2VsZWN0TW9kdWxlLFxyXG4gICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBTaG9ydGN1dFNlcnZpY2UsXHJcbiAgICAgICAgeyBwcm92aWRlOiBVU0VSX0NPTkZJRywgdXNlVmFsdWU6IGNvbmZpZyB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IENPTkZJRyxcclxuICAgICAgICAgIHVzZUZhY3Rvcnk6IENPTkZJR19GQUNUT1JZLFxyXG4gICAgICAgICAgZGVwczogW1VTRVJfQ09ORklHXVxyXG4gICAgICAgIH1cclxuICAgICAgXVxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIl19