/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';
import { getRelativeMousePosition, hasMinimumSize } from './utils';
/** @type {?} */
export var createSelectBox = (/**
 * @param {?} container
 * @param {?=} disabled
 * @return {?}
 */
function (container, disabled) { return (/**
 * @param {?} source
 * @return {?}
 */
function (source) {
    return source.pipe(map((/**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 3), event = _b[0], opacity = _b[1], _c = _b[2], x = _c.x, y = _c.y;
        // Type annotation is required here, because `getRelativeMousePosition` returns a `MousePosition`,
        // the TS compiler cannot figure out the shape of this type.
        /** @type {?} */
        var mousePosition = getRelativeMousePosition(event, container);
        /** @type {?} */
        var width = opacity > 0 ? mousePosition.x - x : 0;
        /** @type {?} */
        var height = opacity > 0 ? mousePosition.y - y : 0;
        return {
            top: height < 0 ? mousePosition.y : y,
            left: width < 0 ? mousePosition.x : x,
            width: Math.abs(width),
            height: Math.abs(height),
            opacity: disabled && disabled() ? 0 : opacity
        };
    })));
}); });
/** @type {?} */
export var whenSelectBoxVisible = (/**
 * @param {?} selectBox$
 * @return {?}
 */
function (selectBox$) { return (/**
 * @param {?} source
 * @return {?}
 */
function (source) {
    return source.pipe(withLatestFrom(selectBox$), filter((/**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 2), selectBox = _b[1];
        return hasMinimumSize(selectBox, 0, 0);
    })), map((/**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 2), event = _b[0], _ = _b[1];
        return event;
    })));
}); });
/** @type {?} */
export var distinctKeyEvents = (/**
 * @return {?}
 */
function () { return (/**
 * @param {?} source
 * @return {?}
 */
function (source) {
    return source.pipe(distinctUntilChanged((/**
     * @param {?} prev
     * @param {?} curr
     * @return {?}
     */
    function (prev, curr) {
        return prev.keyCode === curr.keyCode;
    })));
}); });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0b3JzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdG9ycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRW5GLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxjQUFjLEVBQUUsTUFBTSxTQUFTLENBQUM7O0FBRW5FLE1BQU0sS0FBTyxlQUFlOzs7OztBQUFHLFVBQUMsU0FBOEIsRUFBRSxRQUF3Qjs7OztBQUFLLFVBQzNGLE1BQWtDO0lBRWxDLE9BQUEsTUFBTSxDQUFDLElBQUksQ0FDVCxHQUFHOzs7O0lBQUMsVUFBQyxFQUEwQjtZQUExQiwwQkFBMEIsRUFBekIsYUFBSyxFQUFFLGVBQU8sRUFBRSxVQUFRLEVBQU4sUUFBQyxFQUFFLFFBQUM7Ozs7WUFHcEIsYUFBYSxHQUFrQix3QkFBd0IsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDOztZQUV6RSxLQUFLLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7O1lBQzdDLE1BQU0sR0FBRyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVwRCxPQUFPO1lBQ0wsR0FBRyxFQUFFLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3RCLE1BQU0sRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztZQUN4QixPQUFPLEVBQUUsUUFBUSxJQUFJLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87U0FDOUMsQ0FBQztJQUNKLENBQUMsRUFBQyxDQUNIO0FBakJELENBaUJDLElBQUEsQ0FBQTs7QUFFSCxNQUFNLEtBQU8sb0JBQW9COzs7O0FBQUcsVUFBQyxVQUF5Qzs7OztBQUFLLFVBQUMsTUFBeUI7SUFDM0csT0FBQSxNQUFNLENBQUMsSUFBSSxDQUNULGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFDMUIsTUFBTTs7OztJQUFDLFVBQUMsRUFBYTtZQUFiLDBCQUFhLEVBQVYsaUJBQVM7UUFBTSxPQUFBLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUEvQixDQUErQixFQUFDLEVBQzFELEdBQUc7Ozs7SUFBQyxVQUFDLEVBQVU7WUFBViwwQkFBVSxFQUFULGFBQUssRUFBRSxTQUFDO1FBQU0sT0FBQSxLQUFLO0lBQUwsQ0FBSyxFQUFDLENBQzNCO0FBSkQsQ0FJQyxJQUFBLENBQUE7O0FBRUgsTUFBTSxLQUFPLGlCQUFpQjs7O0FBQUc7Ozs7QUFBTSxVQUFDLE1BQWlDO0lBQ3ZFLE9BQUEsTUFBTSxDQUFDLElBQUksQ0FDVCxvQkFBb0I7Ozs7O0lBQUMsVUFBQyxJQUFJLEVBQUUsSUFBSTtRQUM5QixPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN2QyxDQUFDLEVBQUMsQ0FDSDtBQUpELENBSUMsSUFBQSxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgZmlsdGVyLCBtYXAsIHdpdGhMYXRlc3RGcm9tIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBNb3VzZVBvc2l0aW9uLCBTZWxlY3RCb3gsIFNlbGVjdEJveElucHV0LCBTZWxlY3RDb250YWluZXJIb3N0IH0gZnJvbSAnLi9tb2RlbHMnO1xyXG5pbXBvcnQgeyBnZXRSZWxhdGl2ZU1vdXNlUG9zaXRpb24sIGhhc01pbmltdW1TaXplIH0gZnJvbSAnLi91dGlscyc7XHJcblxyXG5leHBvcnQgY29uc3QgY3JlYXRlU2VsZWN0Qm94ID0gKGNvbnRhaW5lcjogU2VsZWN0Q29udGFpbmVySG9zdCwgZGlzYWJsZWQ/OiAoKSA9PiBib29sZWFuKSA9PiAoXHJcbiAgc291cmNlOiBPYnNlcnZhYmxlPFNlbGVjdEJveElucHV0PlxyXG4pOiBPYnNlcnZhYmxlPFNlbGVjdEJveDxudW1iZXI+PiA9PlxyXG4gIHNvdXJjZS5waXBlKFxyXG4gICAgbWFwKChbZXZlbnQsIG9wYWNpdHksIHsgeCwgeSB9XSkgPT4ge1xyXG4gICAgICAvLyBUeXBlIGFubm90YXRpb24gaXMgcmVxdWlyZWQgaGVyZSwgYmVjYXVzZSBgZ2V0UmVsYXRpdmVNb3VzZVBvc2l0aW9uYCByZXR1cm5zIGEgYE1vdXNlUG9zaXRpb25gLFxyXG4gICAgICAvLyB0aGUgVFMgY29tcGlsZXIgY2Fubm90IGZpZ3VyZSBvdXQgdGhlIHNoYXBlIG9mIHRoaXMgdHlwZS5cclxuICAgICAgY29uc3QgbW91c2VQb3NpdGlvbjogTW91c2VQb3NpdGlvbiA9IGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbihldmVudCwgY29udGFpbmVyKTtcclxuXHJcbiAgICAgIGNvbnN0IHdpZHRoID0gb3BhY2l0eSA+IDAgPyBtb3VzZVBvc2l0aW9uLnggLSB4IDogMDtcclxuICAgICAgY29uc3QgaGVpZ2h0ID0gb3BhY2l0eSA+IDAgPyBtb3VzZVBvc2l0aW9uLnkgLSB5IDogMDtcclxuXHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgdG9wOiBoZWlnaHQgPCAwID8gbW91c2VQb3NpdGlvbi55IDogeSxcclxuICAgICAgICBsZWZ0OiB3aWR0aCA8IDAgPyBtb3VzZVBvc2l0aW9uLnggOiB4LFxyXG4gICAgICAgIHdpZHRoOiBNYXRoLmFicyh3aWR0aCksXHJcbiAgICAgICAgaGVpZ2h0OiBNYXRoLmFicyhoZWlnaHQpLFxyXG4gICAgICAgIG9wYWNpdHk6IGRpc2FibGVkICYmIGRpc2FibGVkKCkgPyAwIDogb3BhY2l0eVxyXG4gICAgICB9O1xyXG4gICAgfSlcclxuICApO1xyXG5cclxuZXhwb3J0IGNvbnN0IHdoZW5TZWxlY3RCb3hWaXNpYmxlID0gKHNlbGVjdEJveCQ6IE9ic2VydmFibGU8U2VsZWN0Qm94PG51bWJlcj4+KSA9PiAoc291cmNlOiBPYnNlcnZhYmxlPEV2ZW50PikgPT5cclxuICBzb3VyY2UucGlwZShcclxuICAgIHdpdGhMYXRlc3RGcm9tKHNlbGVjdEJveCQpLFxyXG4gICAgZmlsdGVyKChbLCBzZWxlY3RCb3hdKSA9PiBoYXNNaW5pbXVtU2l6ZShzZWxlY3RCb3gsIDAsIDApKSxcclxuICAgIG1hcCgoW2V2ZW50LCBfXSkgPT4gZXZlbnQpXHJcbiAgKTtcclxuXHJcbmV4cG9ydCBjb25zdCBkaXN0aW5jdEtleUV2ZW50cyA9ICgpID0+IChzb3VyY2U6IE9ic2VydmFibGU8S2V5Ym9hcmRFdmVudD4pID0+XHJcbiAgc291cmNlLnBpcGUoXHJcbiAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgocHJldiwgY3VycikgPT4ge1xyXG4gICAgICByZXR1cm4gcHJldi5rZXlDb2RlID09PSBjdXJyLmtleUNvZGU7XHJcbiAgICB9KVxyXG4gICk7XHJcbiJdfQ==