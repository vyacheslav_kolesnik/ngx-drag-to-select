/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var UpdateActions = {
    Add: 0,
    Remove: 1,
};
export { UpdateActions };
UpdateActions[UpdateActions.Add] = 'Add';
UpdateActions[UpdateActions.Remove] = 'Remove';
/**
 * @record
 */
export function UpdateAction() { }
if (false) {
    /** @type {?} */
    UpdateAction.prototype.type;
    /** @type {?} */
    UpdateAction.prototype.item;
}
/**
 * @record
 * @template T
 */
export function ObservableProxy() { }
if (false) {
    /** @type {?} */
    ObservableProxy.prototype.proxy$;
    /** @type {?} */
    ObservableProxy.prototype.proxy;
}
/**
 * @record
 */
export function SelectContainerHost() { }
if (false) {
    /** @type {?} */
    SelectContainerHost.prototype.boundingClientRect;
}
/**
 * @record
 */
export function Shortcuts() { }
if (false) {
    /** @type {?} */
    Shortcuts.prototype.disableSelection;
    /** @type {?} */
    Shortcuts.prototype.toggleSingleItem;
    /** @type {?} */
    Shortcuts.prototype.addToSelection;
    /** @type {?} */
    Shortcuts.prototype.removeFromSelection;
}
/**
 * @record
 */
export function DragToSelectConfig() { }
if (false) {
    /** @type {?} */
    DragToSelectConfig.prototype.selectedClass;
    /** @type {?} */
    DragToSelectConfig.prototype.shortcuts;
}
/**
 * @record
 */
export function MousePosition() { }
if (false) {
    /** @type {?} */
    MousePosition.prototype.x;
    /** @type {?} */
    MousePosition.prototype.y;
}
/**
 * @record
 */
export function BoundingBox() { }
if (false) {
    /** @type {?} */
    BoundingBox.prototype.top;
    /** @type {?} */
    BoundingBox.prototype.left;
    /** @type {?} */
    BoundingBox.prototype.width;
    /** @type {?} */
    BoundingBox.prototype.height;
}
/**
 * @record
 * @template T
 */
export function SelectBox() { }
if (false) {
    /** @type {?} */
    SelectBox.prototype.top;
    /** @type {?} */
    SelectBox.prototype.left;
    /** @type {?} */
    SelectBox.prototype.width;
    /** @type {?} */
    SelectBox.prototype.height;
    /** @type {?} */
    SelectBox.prototype.opacity;
}
/** @enum {number} */
var Action = {
    Add: 0,
    Delete: 1,
    None: 2,
};
export { Action };
Action[Action.Add] = 'Add';
Action[Action.Delete] = 'Delete';
Action[Action.None] = 'None';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL21vZGVscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFNRSxNQUFHO0lBQ0gsU0FBTTs7Ozs7Ozs7QUFHUixrQ0FHQzs7O0lBRkMsNEJBQW9COztJQUNwQiw0QkFBMEI7Ozs7OztBQUc1QixxQ0FHQzs7O0lBRkMsaUNBQXdCOztJQUN4QixnQ0FBUzs7Ozs7QUFHWCx5Q0FFQzs7O0lBREMsaURBQWdDOzs7OztBQUdsQywrQkFLQzs7O0lBSkMscUNBQXlCOztJQUN6QixxQ0FBeUI7O0lBQ3pCLG1DQUF1Qjs7SUFDdkIsd0NBQTRCOzs7OztBQUc5Qix3Q0FHQzs7O0lBRkMsMkNBQXNCOztJQUN0Qix1Q0FBOEI7Ozs7O0FBR2hDLG1DQUdDOzs7SUFGQywwQkFBVTs7SUFDViwwQkFBVTs7Ozs7QUFHWixpQ0FLQzs7O0lBSkMsMEJBQVk7O0lBQ1osMkJBQWE7O0lBQ2IsNEJBQWM7O0lBQ2QsNkJBQWU7Ozs7OztBQUtqQiwrQkFNQzs7O0lBTEMsd0JBQU87O0lBQ1AseUJBQVE7O0lBQ1IsMEJBQVM7O0lBQ1QsMkJBQVU7O0lBQ1YsNEJBQWdCOzs7O0lBSWhCLE1BQUc7SUFDSCxTQUFNO0lBQ04sT0FBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2VsZWN0SXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vc2VsZWN0LWl0ZW0uZGlyZWN0aXZlJztcclxuXHJcbmV4cG9ydCB0eXBlIFByZWRpY2F0ZUZuPFQ+ID0gKGl0ZW06IFQpID0+IGJvb2xlYW47XHJcblxyXG5leHBvcnQgZW51bSBVcGRhdGVBY3Rpb25zIHtcclxuICBBZGQsXHJcbiAgUmVtb3ZlXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVXBkYXRlQWN0aW9uIHtcclxuICB0eXBlOiBVcGRhdGVBY3Rpb25zO1xyXG4gIGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmU7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgT2JzZXJ2YWJsZVByb3h5PFQ+IHtcclxuICBwcm94eSQ6IE9ic2VydmFibGU8YW55PjtcclxuICBwcm94eTogVDtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWxlY3RDb250YWluZXJIb3N0IGV4dGVuZHMgSFRNTEVsZW1lbnQge1xyXG4gIGJvdW5kaW5nQ2xpZW50UmVjdDogQm91bmRpbmdCb3g7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgU2hvcnRjdXRzIHtcclxuICBkaXNhYmxlU2VsZWN0aW9uOiBzdHJpbmc7XHJcbiAgdG9nZ2xlU2luZ2xlSXRlbTogc3RyaW5nO1xyXG4gIGFkZFRvU2VsZWN0aW9uOiBzdHJpbmc7XHJcbiAgcmVtb3ZlRnJvbVNlbGVjdGlvbjogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERyYWdUb1NlbGVjdENvbmZpZyB7XHJcbiAgc2VsZWN0ZWRDbGFzczogc3RyaW5nO1xyXG4gIHNob3J0Y3V0czogUGFydGlhbDxTaG9ydGN1dHM+O1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE1vdXNlUG9zaXRpb24ge1xyXG4gIHg6IG51bWJlcjtcclxuICB5OiBudW1iZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQm91bmRpbmdCb3gge1xyXG4gIHRvcDogbnVtYmVyO1xyXG4gIGxlZnQ6IG51bWJlcjtcclxuICB3aWR0aDogbnVtYmVyO1xyXG4gIGhlaWdodDogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgdHlwZSBTZWxlY3RCb3hJbnB1dCA9IFtNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCwgbnVtYmVyLCBNb3VzZVBvc2l0aW9uXTtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgU2VsZWN0Qm94PFQ+IHtcclxuICB0b3A6IFQ7XHJcbiAgbGVmdDogVDtcclxuICB3aWR0aDogVDtcclxuICBoZWlnaHQ6IFQ7XHJcbiAgb3BhY2l0eTogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBBY3Rpb24ge1xyXG4gIEFkZCxcclxuICBEZWxldGUsXHJcbiAgTm9uZVxyXG59XHJcbiJdfQ==