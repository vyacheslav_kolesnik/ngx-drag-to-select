import * as tslib_1 from "tslib";
var _a;
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { CONFIG } from './tokens';
/** @type {?} */
var SUPPORTED_KEYS = {
    alt: true,
    shift: true,
    meta: true,
    ctrl: true
};
/** @type {?} */
var META_KEY = 'meta';
/** @type {?} */
var KEY_ALIASES = (_a = {},
    _a[META_KEY] = ['ctrl', 'meta'],
    _a);
/** @type {?} */
var SUPPORTED_SHORTCUTS = {
    disableSelection: true,
    toggleSingleItem: true,
    addToSelection: true,
    removeFromSelection: true
};
/** @type {?} */
var ERROR_PREFIX = '[ShortcutService]';
var ShortcutService = /** @class */ (function () {
    function ShortcutService(config) {
        this.config = config;
        this._shortcuts = {};
        this._shortcuts = this.createShortcutsFromConfig(config.shortcuts);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.disableSelection = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.isShortcutPressed('disableSelection', event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.toggleSingleItem = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.isShortcutPressed('toggleSingleItem', event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.addToSelection = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.isShortcutPressed('addToSelection', event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.removeFromSelection = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.isShortcutPressed('removeFromSelection', event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.extendedSelectionShortcut = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this.addToSelection(event) || this.removeFromSelection(event);
    };
    /**
     * @private
     * @param {?} shortcuts
     * @return {?}
     */
    ShortcutService.prototype.createShortcutsFromConfig = /**
     * @private
     * @param {?} shortcuts
     * @return {?}
     */
    function (shortcuts) {
        var _this = this;
        var e_1, _a;
        /** @type {?} */
        var shortcutMap = {};
        var _loop_1 = function (key, shortcutsForCommand) {
            if (!this_1.isSupportedShortcut(key)) {
                throw new Error(this_1.getErrorMessage("Shortcut " + key + " not supported"));
            }
            shortcutsForCommand
                .replace(/ /g, '')
                .split(',')
                .forEach((/**
             * @param {?} shortcut
             * @return {?}
             */
            function (shortcut) {
                if (!shortcutMap[key]) {
                    shortcutMap[key] = [];
                }
                /** @type {?} */
                var combo = shortcut.split('+');
                /** @type {?} */
                var cleanCombos = _this.substituteKey(shortcut, combo, META_KEY);
                cleanCombos.forEach((/**
                 * @param {?} cleanCombo
                 * @return {?}
                 */
                function (cleanCombo) {
                    /** @type {?} */
                    var unsupportedKey = _this.isSupportedCombo(cleanCombo);
                    if (unsupportedKey) {
                        throw new Error(_this.getErrorMessage("Key '" + unsupportedKey + "' in shortcut " + shortcut + " not supported"));
                    }
                    shortcutMap[key].push(cleanCombo.map((/**
                     * @param {?} comboKey
                     * @return {?}
                     */
                    function (comboKey) { return comboKey + "Key"; })));
                }));
            }));
        };
        var this_1 = this;
        try {
            for (var _b = tslib_1.__values(Object.entries(shortcuts)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var _d = tslib_1.__read(_c.value, 2), key = _d[0], shortcutsForCommand = _d[1];
                _loop_1(key, shortcutsForCommand);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return shortcutMap;
    };
    /**
     * @private
     * @param {?} shortcut
     * @param {?} combo
     * @param {?} substituteKey
     * @return {?}
     */
    ShortcutService.prototype.substituteKey = /**
     * @private
     * @param {?} shortcut
     * @param {?} combo
     * @param {?} substituteKey
     * @return {?}
     */
    function (shortcut, combo, substituteKey) {
        /** @type {?} */
        var hasSpecialKey = shortcut.includes(substituteKey);
        /** @type {?} */
        var substitutedShortcut = [];
        if (hasSpecialKey) {
            /** @type {?} */
            var cleanShortcut_1 = combo.filter((/**
             * @param {?} element
             * @return {?}
             */
            function (element) { return element !== META_KEY; }));
            KEY_ALIASES.meta.forEach((/**
             * @param {?} alias
             * @return {?}
             */
            function (alias) {
                substitutedShortcut.push(tslib_1.__spread(cleanShortcut_1, [alias]));
            }));
        }
        else {
            substitutedShortcut.push(combo);
        }
        return substitutedShortcut;
    };
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    ShortcutService.prototype.getErrorMessage = /**
     * @private
     * @param {?} message
     * @return {?}
     */
    function (message) {
        return ERROR_PREFIX + " " + message;
    };
    /**
     * @private
     * @param {?} shortcutName
     * @param {?} event
     * @return {?}
     */
    ShortcutService.prototype.isShortcutPressed = /**
     * @private
     * @param {?} shortcutName
     * @param {?} event
     * @return {?}
     */
    function (shortcutName, event) {
        /** @type {?} */
        var shortcuts = this._shortcuts[shortcutName];
        return shortcuts.some((/**
         * @param {?} shortcut
         * @return {?}
         */
        function (shortcut) {
            return shortcut.every((/**
             * @param {?} key
             * @return {?}
             */
            function (key) { return event[key]; }));
        }));
    };
    /**
     * @private
     * @param {?} combo
     * @return {?}
     */
    ShortcutService.prototype.isSupportedCombo = /**
     * @private
     * @param {?} combo
     * @return {?}
     */
    function (combo) {
        /** @type {?} */
        var unsupportedKey = null;
        combo.forEach((/**
         * @param {?} key
         * @return {?}
         */
        function (key) {
            if (!SUPPORTED_KEYS[key]) {
                unsupportedKey = key;
                return;
            }
        }));
        return unsupportedKey;
    };
    /**
     * @private
     * @param {?} shortcut
     * @return {?}
     */
    ShortcutService.prototype.isSupportedShortcut = /**
     * @private
     * @param {?} shortcut
     * @return {?}
     */
    function (shortcut) {
        return SUPPORTED_SHORTCUTS[shortcut];
    };
    ShortcutService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ShortcutService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] }
    ]; };
    return ShortcutService;
}());
export { ShortcutService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype._shortcuts;
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcnRjdXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1kcmFnLXRvLXNlbGVjdC8iLCJzb3VyY2VzIjpbImxpYi9zaG9ydGN1dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5ELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUM7O0lBRTVCLGNBQWMsR0FBRztJQUNyQixHQUFHLEVBQUUsSUFBSTtJQUNULEtBQUssRUFBRSxJQUFJO0lBQ1gsSUFBSSxFQUFFLElBQUk7SUFDVixJQUFJLEVBQUUsSUFBSTtDQUNYOztJQUVLLFFBQVEsR0FBRyxNQUFNOztJQUVqQixXQUFXO0lBQ2YsR0FBQyxRQUFRLElBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDO09BQzdCOztJQUVLLG1CQUFtQixHQUFHO0lBQzFCLGdCQUFnQixFQUFFLElBQUk7SUFDdEIsZ0JBQWdCLEVBQUUsSUFBSTtJQUN0QixjQUFjLEVBQUUsSUFBSTtJQUNwQixtQkFBbUIsRUFBRSxJQUFJO0NBQzFCOztJQUVLLFlBQVksR0FBRyxtQkFBbUI7QUFFeEM7SUFJRSx5QkFBb0MsTUFBMEI7UUFBMUIsV0FBTSxHQUFOLE1BQU0sQ0FBb0I7UUFGdEQsZUFBVSxHQUFrQyxFQUFFLENBQUM7UUFHckQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7Ozs7O0lBRUQsMENBQWdCOzs7O0lBQWhCLFVBQWlCLEtBQVk7UUFDM0IsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0QsQ0FBQzs7Ozs7SUFFRCwwQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBWTtRQUMzQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELHdDQUFjOzs7O0lBQWQsVUFBZSxLQUFZO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3pELENBQUM7Ozs7O0lBRUQsNkNBQW1COzs7O0lBQW5CLFVBQW9CLEtBQVk7UUFDOUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7SUFFRCxtREFBeUI7Ozs7SUFBekIsVUFBMEIsS0FBWTtRQUNwQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7OztJQUVPLG1EQUF5Qjs7Ozs7SUFBakMsVUFBa0MsU0FBb0M7UUFBdEUsaUJBZ0NDOzs7WUEvQk8sV0FBVyxHQUFHLEVBQUU7Z0NBRVYsR0FBRyxFQUFFLG1CQUFtQjtZQUNsQyxJQUFJLENBQUMsT0FBSyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFLLGVBQWUsQ0FBQyxjQUFZLEdBQUcsbUJBQWdCLENBQUMsQ0FBQyxDQUFDO2FBQ3hFO1lBRUQsbUJBQW1CO2lCQUNoQixPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQztpQkFDakIsS0FBSyxDQUFDLEdBQUcsQ0FBQztpQkFDVixPQUFPOzs7O1lBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ3JCLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3ZCOztvQkFFSyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7O29CQUMzQixXQUFXLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQztnQkFFakUsV0FBVyxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQSxVQUFVOzt3QkFDdEIsY0FBYyxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7b0JBRXhELElBQUksY0FBYyxFQUFFO3dCQUNsQixNQUFNLElBQUksS0FBSyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBUSxjQUFjLHNCQUFpQixRQUFRLG1CQUFnQixDQUFDLENBQUMsQ0FBQztxQkFDeEc7b0JBRUQsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRzs7OztvQkFBQyxVQUFBLFFBQVEsSUFBSSxPQUFHLFFBQVEsUUFBSyxFQUFoQixDQUFnQixFQUFDLENBQUMsQ0FBQztnQkFDdEUsQ0FBQyxFQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQzs7OztZQXpCUCxLQUF5QyxJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQSxnQkFBQTtnQkFBdkQsSUFBQSxnQ0FBMEIsRUFBekIsV0FBRyxFQUFFLDJCQUFtQjt3QkFBeEIsR0FBRyxFQUFFLG1CQUFtQjthQTBCbkM7Ozs7Ozs7OztRQUVELE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7Ozs7Ozs7O0lBRU8sdUNBQWE7Ozs7Ozs7SUFBckIsVUFBc0IsUUFBZ0IsRUFBRSxLQUFvQixFQUFFLGFBQXFCOztZQUMzRSxhQUFhLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7O1lBQ2hELG1CQUFtQixHQUFHLEVBQUU7UUFFOUIsSUFBSSxhQUFhLEVBQUU7O2dCQUNYLGVBQWEsR0FBRyxLQUFLLENBQUMsTUFBTTs7OztZQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxLQUFLLFFBQVEsRUFBcEIsQ0FBb0IsRUFBQztZQUVuRSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLEtBQUs7Z0JBQzVCLG1CQUFtQixDQUFDLElBQUksa0JBQUssZUFBYSxHQUFFLEtBQUssR0FBRSxDQUFDO1lBQ3RELENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLG1CQUFtQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqQztRQUVELE9BQU8sbUJBQW1CLENBQUM7SUFDN0IsQ0FBQzs7Ozs7O0lBRU8seUNBQWU7Ozs7O0lBQXZCLFVBQXdCLE9BQWU7UUFDckMsT0FBVSxZQUFZLFNBQUksT0FBUyxDQUFDO0lBQ3RDLENBQUM7Ozs7Ozs7SUFFTywyQ0FBaUI7Ozs7OztJQUF6QixVQUEwQixZQUFvQixFQUFFLEtBQVk7O1lBQ3BELFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUUvQyxPQUFPLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQSxRQUFRO1lBQzVCLE9BQU8sUUFBUSxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBVixDQUFVLEVBQUMsQ0FBQztRQUMzQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLDBDQUFnQjs7Ozs7SUFBeEIsVUFBeUIsS0FBb0I7O1lBQ3ZDLGNBQWMsR0FBRyxJQUFJO1FBRXpCLEtBQUssQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxHQUFHO1lBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDeEIsY0FBYyxHQUFHLEdBQUcsQ0FBQztnQkFDckIsT0FBTzthQUNSO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCxPQUFPLGNBQWMsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFFTyw2Q0FBbUI7Ozs7O0lBQTNCLFVBQTRCLFFBQWdCO1FBQzFDLE9BQU8sbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Z0JBMUdGLFVBQVU7Ozs7Z0RBSUksTUFBTSxTQUFDLE1BQU07O0lBdUc1QixzQkFBQztDQUFBLEFBM0dELElBMkdDO1NBMUdZLGVBQWU7Ozs7OztJQUMxQixxQ0FBdUQ7Ozs7O0lBRTNDLGlDQUFrRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEcmFnVG9TZWxlY3RDb25maWcgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IENPTkZJRyB9IGZyb20gJy4vdG9rZW5zJztcclxuXHJcbmNvbnN0IFNVUFBPUlRFRF9LRVlTID0ge1xyXG4gIGFsdDogdHJ1ZSxcclxuICBzaGlmdDogdHJ1ZSxcclxuICBtZXRhOiB0cnVlLFxyXG4gIGN0cmw6IHRydWVcclxufTtcclxuXHJcbmNvbnN0IE1FVEFfS0VZID0gJ21ldGEnO1xyXG5cclxuY29uc3QgS0VZX0FMSUFTRVMgPSB7XHJcbiAgW01FVEFfS0VZXTogWydjdHJsJywgJ21ldGEnXVxyXG59O1xyXG5cclxuY29uc3QgU1VQUE9SVEVEX1NIT1JUQ1VUUyA9IHtcclxuICBkaXNhYmxlU2VsZWN0aW9uOiB0cnVlLFxyXG4gIHRvZ2dsZVNpbmdsZUl0ZW06IHRydWUsXHJcbiAgYWRkVG9TZWxlY3Rpb246IHRydWUsXHJcbiAgcmVtb3ZlRnJvbVNlbGVjdGlvbjogdHJ1ZVxyXG59O1xyXG5cclxuY29uc3QgRVJST1JfUFJFRklYID0gJ1tTaG9ydGN1dFNlcnZpY2VdJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFNob3J0Y3V0U2VydmljZSB7XHJcbiAgcHJpdmF0ZSBfc2hvcnRjdXRzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZ1tdW10gfSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihASW5qZWN0KENPTkZJRykgcHJpdmF0ZSBjb25maWc6IERyYWdUb1NlbGVjdENvbmZpZykge1xyXG4gICAgdGhpcy5fc2hvcnRjdXRzID0gdGhpcy5jcmVhdGVTaG9ydGN1dHNGcm9tQ29uZmlnKGNvbmZpZy5zaG9ydGN1dHMpO1xyXG4gIH1cclxuXHJcbiAgZGlzYWJsZVNlbGVjdGlvbihldmVudDogRXZlbnQpIHtcclxuICAgIHJldHVybiB0aGlzLmlzU2hvcnRjdXRQcmVzc2VkKCdkaXNhYmxlU2VsZWN0aW9uJywgZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlU2luZ2xlSXRlbShldmVudDogRXZlbnQpIHtcclxuICAgIHJldHVybiB0aGlzLmlzU2hvcnRjdXRQcmVzc2VkKCd0b2dnbGVTaW5nbGVJdGVtJywgZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgYWRkVG9TZWxlY3Rpb24oZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICByZXR1cm4gdGhpcy5pc1Nob3J0Y3V0UHJlc3NlZCgnYWRkVG9TZWxlY3Rpb24nLCBldmVudCk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50OiBFdmVudCkge1xyXG4gICAgcmV0dXJuIHRoaXMuaXNTaG9ydGN1dFByZXNzZWQoJ3JlbW92ZUZyb21TZWxlY3Rpb24nLCBldmVudCk7XHJcbiAgfVxyXG5cclxuICBleHRlbmRlZFNlbGVjdGlvblNob3J0Y3V0KGV2ZW50OiBFdmVudCkge1xyXG4gICAgcmV0dXJuIHRoaXMuYWRkVG9TZWxlY3Rpb24oZXZlbnQpIHx8IHRoaXMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZVNob3J0Y3V0c0Zyb21Db25maWcoc2hvcnRjdXRzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9KSB7XHJcbiAgICBjb25zdCBzaG9ydGN1dE1hcCA9IHt9O1xyXG5cclxuICAgIGZvciAoY29uc3QgW2tleSwgc2hvcnRjdXRzRm9yQ29tbWFuZF0gb2YgT2JqZWN0LmVudHJpZXMoc2hvcnRjdXRzKSkge1xyXG4gICAgICBpZiAoIXRoaXMuaXNTdXBwb3J0ZWRTaG9ydGN1dChrZXkpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKHRoaXMuZ2V0RXJyb3JNZXNzYWdlKGBTaG9ydGN1dCAke2tleX0gbm90IHN1cHBvcnRlZGApKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgc2hvcnRjdXRzRm9yQ29tbWFuZFxyXG4gICAgICAgIC5yZXBsYWNlKC8gL2csICcnKVxyXG4gICAgICAgIC5zcGxpdCgnLCcpXHJcbiAgICAgICAgLmZvckVhY2goc2hvcnRjdXQgPT4ge1xyXG4gICAgICAgICAgaWYgKCFzaG9ydGN1dE1hcFtrZXldKSB7XHJcbiAgICAgICAgICAgIHNob3J0Y3V0TWFwW2tleV0gPSBbXTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBjb25zdCBjb21ibyA9IHNob3J0Y3V0LnNwbGl0KCcrJyk7XHJcbiAgICAgICAgICBjb25zdCBjbGVhbkNvbWJvcyA9IHRoaXMuc3Vic3RpdHV0ZUtleShzaG9ydGN1dCwgY29tYm8sIE1FVEFfS0VZKTtcclxuXHJcbiAgICAgICAgICBjbGVhbkNvbWJvcy5mb3JFYWNoKGNsZWFuQ29tYm8gPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB1bnN1cHBvcnRlZEtleSA9IHRoaXMuaXNTdXBwb3J0ZWRDb21ibyhjbGVhbkNvbWJvKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh1bnN1cHBvcnRlZEtleSkge1xyXG4gICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcih0aGlzLmdldEVycm9yTWVzc2FnZShgS2V5ICcke3Vuc3VwcG9ydGVkS2V5fScgaW4gc2hvcnRjdXQgJHtzaG9ydGN1dH0gbm90IHN1cHBvcnRlZGApKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2hvcnRjdXRNYXBba2V5XS5wdXNoKGNsZWFuQ29tYm8ubWFwKGNvbWJvS2V5ID0+IGAke2NvbWJvS2V5fUtleWApKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBzaG9ydGN1dE1hcDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3Vic3RpdHV0ZUtleShzaG9ydGN1dDogc3RyaW5nLCBjb21ibzogQXJyYXk8c3RyaW5nPiwgc3Vic3RpdHV0ZUtleTogc3RyaW5nKSB7XHJcbiAgICBjb25zdCBoYXNTcGVjaWFsS2V5ID0gc2hvcnRjdXQuaW5jbHVkZXMoc3Vic3RpdHV0ZUtleSk7XHJcbiAgICBjb25zdCBzdWJzdGl0dXRlZFNob3J0Y3V0ID0gW107XHJcblxyXG4gICAgaWYgKGhhc1NwZWNpYWxLZXkpIHtcclxuICAgICAgY29uc3QgY2xlYW5TaG9ydGN1dCA9IGNvbWJvLmZpbHRlcihlbGVtZW50ID0+IGVsZW1lbnQgIT09IE1FVEFfS0VZKTtcclxuXHJcbiAgICAgIEtFWV9BTElBU0VTLm1ldGEuZm9yRWFjaChhbGlhcyA9PiB7XHJcbiAgICAgICAgc3Vic3RpdHV0ZWRTaG9ydGN1dC5wdXNoKFsuLi5jbGVhblNob3J0Y3V0LCBhbGlhc10pO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHN1YnN0aXR1dGVkU2hvcnRjdXQucHVzaChjb21ibyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHN1YnN0aXR1dGVkU2hvcnRjdXQ7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldEVycm9yTWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBgJHtFUlJPUl9QUkVGSVh9ICR7bWVzc2FnZX1gO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpc1Nob3J0Y3V0UHJlc3NlZChzaG9ydGN1dE5hbWU6IHN0cmluZywgZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBjb25zdCBzaG9ydGN1dHMgPSB0aGlzLl9zaG9ydGN1dHNbc2hvcnRjdXROYW1lXTtcclxuXHJcbiAgICByZXR1cm4gc2hvcnRjdXRzLnNvbWUoc2hvcnRjdXQgPT4ge1xyXG4gICAgICByZXR1cm4gc2hvcnRjdXQuZXZlcnkoa2V5ID0+IGV2ZW50W2tleV0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGlzU3VwcG9ydGVkQ29tYm8oY29tYm86IEFycmF5PHN0cmluZz4pIHtcclxuICAgIGxldCB1bnN1cHBvcnRlZEtleSA9IG51bGw7XHJcblxyXG4gICAgY29tYm8uZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICBpZiAoIVNVUFBPUlRFRF9LRVlTW2tleV0pIHtcclxuICAgICAgICB1bnN1cHBvcnRlZEtleSA9IGtleTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiB1bnN1cHBvcnRlZEtleTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaXNTdXBwb3J0ZWRTaG9ydGN1dChzaG9ydGN1dDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gU1VQUE9SVEVEX1NIT1JUQ1VUU1tzaG9ydGN1dF07XHJcbiAgfVxyXG59XHJcbiJdfQ==