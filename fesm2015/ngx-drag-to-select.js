import { InjectionToken, Directive, Inject, PLATFORM_ID, ElementRef, Renderer2, Input, Injectable, EventEmitter, Component, NgZone, ViewChild, ContentChildren, HostBinding, Output, NgModule } from '@angular/core';
import { isPlatformBrowser, CommonModule } from '@angular/common';
import { BehaviorSubject, Subject, fromEvent, merge, combineLatest, from, asyncScheduler } from 'rxjs';
import { map, withLatestFrom, filter, distinctUntilChanged, share, tap, switchMap, takeUntil, mapTo, auditTime, first, observeOn, startWith, concatMapTo } from 'rxjs/operators';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const CONFIG = new InjectionToken('DRAG_TO_SELECT_CONFIG');
/** @type {?} */
const USER_CONFIG = new InjectionToken('USER_CONFIG');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const AUDIT_TIME = 16;
/** @type {?} */
const MIN_WIDTH = 5;
/** @type {?} */
const MIN_HEIGHT = 5;
/** @type {?} */
const NO_SELECT_CLASS = 'dts-no-select';
/** @type {?} */
const NO_SELECT_CLASS_MOBILE = 'dts-no-select-mobile';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const isObject = (/**
 * @param {?} item
 * @return {?}
 */
(item) => {
    return item && typeof item === 'object' && !Array.isArray(item) && item !== null;
});
/**
 * @param {?} target
 * @param {?} source
 * @return {?}
 */
function mergeDeep(target, source) {
    if (isObject(target) && isObject(source)) {
        Object.keys(source).forEach((/**
         * @param {?} key
         * @return {?}
         */
        key => {
            if (isObject(source[key])) {
                if (!target[key]) {
                    Object.assign(target, { [key]: {} });
                }
                mergeDeep(target[key], source[key]);
            }
            else {
                Object.assign(target, { [key]: source[key] });
            }
        }));
    }
    return target;
}
/** @type {?} */
const hasMinimumSize = (/**
 * @param {?} selectBox
 * @param {?=} minWidth
 * @param {?=} minHeight
 * @return {?}
 */
(selectBox, minWidth = MIN_WIDTH, minHeight = MIN_HEIGHT) => {
    return selectBox.width > minWidth || selectBox.height > minHeight;
});
/** @type {?} */
const clearSelection = (/**
 * @param {?} window
 * @return {?}
 */
(window) => {
    /** @type {?} */
    const selection = window.getSelection();
    if (!selection) {
        return;
    }
    if (selection.removeAllRanges) {
        selection.removeAllRanges();
    }
    else if (selection.empty) {
        selection.empty();
    }
});
/** @type {?} */
const inBoundingBox = (/**
 * @param {?} point
 * @param {?} box
 * @return {?}
 */
(point, box) => {
    return (box.left <= point.x && point.x <= box.left + box.width && box.top <= point.y && point.y <= box.top + box.height);
});
/** @type {?} */
const boxIntersects = (/**
 * @param {?} boxA
 * @param {?} boxB
 * @return {?}
 */
(boxA, boxB) => {
    return (boxA.left <= boxB.left + boxB.width &&
        boxA.left + boxA.width >= boxB.left &&
        boxA.top <= boxB.top + boxB.height &&
        boxA.top + boxA.height >= boxB.top);
});
/** @type {?} */
const calculateBoundingClientRect = (/**
 * @param {?} element
 * @return {?}
 */
(element) => {
    return element.getBoundingClientRect();
});
/** @type {?} */
const getMousePosition = (/**
 * @param {?} event
 * @return {?}
 */
(event) => {
    return event instanceof MouseEvent
        ? {
            x: event.clientX,
            y: event.clientY
        }
        : {
            x: event.touches[0].clientX,
            y: event.touches[0].clientY
        };
});
/** @type {?} */
const getScroll = (/**
 * @return {?}
 */
() => {
    if (!document || !document.documentElement) {
        return {
            x: 0,
            y: 0
        };
    }
    return {
        x: document.documentElement.scrollLeft || document.body.scrollLeft,
        y: document.documentElement.scrollTop || document.body.scrollTop
    };
});
/** @type {?} */
const getRelativeMousePosition = (/**
 * @param {?} event
 * @param {?} container
 * @return {?}
 */
(event, container) => {
    const { x: clientX, y: clientY } = getMousePosition(event);
    /** @type {?} */
    const scroll = getScroll();
    /** @type {?} */
    const borderSize = (container.boundingClientRect.width - container.clientWidth) / 2;
    /** @type {?} */
    const offsetLeft = container.boundingClientRect.left + scroll.x;
    /** @type {?} */
    const offsetTop = container.boundingClientRect.top + scroll.y;
    return {
        x: clientX - borderSize - (offsetLeft - window.pageXOffset) + container.scrollLeft,
        y: clientY - borderSize - (offsetTop - window.pageYOffset) + container.scrollTop
    };
});
/** @type {?} */
const cursorWithinElement = (/**
 * @param {?} event
 * @param {?} element
 * @return {?}
 */
(event, element) => {
    /** @type {?} */
    const mousePoint = getMousePosition(event);
    return inBoundingBox(mousePoint, calculateBoundingClientRect(element));
});

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectItemDirective {
    /**
     * @param {?} config
     * @param {?} platformId
     * @param {?} host
     * @param {?} renderer
     */
    constructor(config, platformId, host, renderer) {
        this.config = config;
        this.platformId = platformId;
        this.host = host;
        this.renderer = renderer;
        this.selected = false;
    }
    /**
     * @return {?}
     */
    get value() {
        return this.dtsSelectItem ? this.dtsSelectItem : this;
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        this.applySelectedClass();
    }
    /**
     * @return {?}
     */
    getBoundingClientRect() {
        if (isPlatformBrowser(this.platformId) && !this._boundingClientRect) {
            this.calculateBoundingClientRect();
        }
        return this._boundingClientRect;
    }
    /**
     * @return {?}
     */
    calculateBoundingClientRect() {
        this._boundingClientRect = calculateBoundingClientRect(this.host.nativeElement);
    }
    /**
     * @return {?}
     */
    _select() {
        this.selected = true;
    }
    /**
     * @return {?}
     */
    _deselect() {
        this.selected = false;
    }
    /**
     * @private
     * @return {?}
     */
    applySelectedClass() {
        if (this.selected) {
            this.renderer.addClass(this.host.nativeElement, this.config.selectedClass);
        }
        else {
            this.renderer.removeClass(this.host.nativeElement, this.config.selectedClass);
        }
    }
}
SelectItemDirective.decorators = [
    { type: Directive, args: [{
                selector: '[dtsSelectItem]',
                exportAs: 'dtsSelectItem',
                host: {
                    class: 'dts-select-item'
                }
            },] }
];
/** @nocollapse */
SelectItemDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] },
    { type: undefined, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] },
    { type: ElementRef },
    { type: Renderer2 }
];
SelectItemDirective.propDecorators = {
    dtsSelectItem: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype._boundingClientRect;
    /** @type {?} */
    SelectItemDirective.prototype.selected;
    /** @type {?} */
    SelectItemDirective.prototype.dtsSelectItem;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.platformId;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.host;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SUPPORTED_KEYS = {
    alt: true,
    shift: true,
    meta: true,
    ctrl: true
};
/** @type {?} */
const META_KEY = 'meta';
/** @type {?} */
const KEY_ALIASES = {
    [META_KEY]: ['ctrl', 'meta']
};
/** @type {?} */
const SUPPORTED_SHORTCUTS = {
    disableSelection: true,
    toggleSingleItem: true,
    addToSelection: true,
    removeFromSelection: true
};
/** @type {?} */
const ERROR_PREFIX = '[ShortcutService]';
class ShortcutService {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this._shortcuts = {};
        this._shortcuts = this.createShortcutsFromConfig(config.shortcuts);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    disableSelection(event) {
        return this.isShortcutPressed('disableSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    toggleSingleItem(event) {
        return this.isShortcutPressed('toggleSingleItem', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    addToSelection(event) {
        return this.isShortcutPressed('addToSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    removeFromSelection(event) {
        return this.isShortcutPressed('removeFromSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    extendedSelectionShortcut(event) {
        return this.addToSelection(event) || this.removeFromSelection(event);
    }
    /**
     * @private
     * @param {?} shortcuts
     * @return {?}
     */
    createShortcutsFromConfig(shortcuts) {
        /** @type {?} */
        const shortcutMap = {};
        for (const [key, shortcutsForCommand] of Object.entries(shortcuts)) {
            if (!this.isSupportedShortcut(key)) {
                throw new Error(this.getErrorMessage(`Shortcut ${key} not supported`));
            }
            shortcutsForCommand
                .replace(/ /g, '')
                .split(',')
                .forEach((/**
             * @param {?} shortcut
             * @return {?}
             */
            shortcut => {
                if (!shortcutMap[key]) {
                    shortcutMap[key] = [];
                }
                /** @type {?} */
                const combo = shortcut.split('+');
                /** @type {?} */
                const cleanCombos = this.substituteKey(shortcut, combo, META_KEY);
                cleanCombos.forEach((/**
                 * @param {?} cleanCombo
                 * @return {?}
                 */
                cleanCombo => {
                    /** @type {?} */
                    const unsupportedKey = this.isSupportedCombo(cleanCombo);
                    if (unsupportedKey) {
                        throw new Error(this.getErrorMessage(`Key '${unsupportedKey}' in shortcut ${shortcut} not supported`));
                    }
                    shortcutMap[key].push(cleanCombo.map((/**
                     * @param {?} comboKey
                     * @return {?}
                     */
                    comboKey => `${comboKey}Key`)));
                }));
            }));
        }
        return shortcutMap;
    }
    /**
     * @private
     * @param {?} shortcut
     * @param {?} combo
     * @param {?} substituteKey
     * @return {?}
     */
    substituteKey(shortcut, combo, substituteKey) {
        /** @type {?} */
        const hasSpecialKey = shortcut.includes(substituteKey);
        /** @type {?} */
        const substitutedShortcut = [];
        if (hasSpecialKey) {
            /** @type {?} */
            const cleanShortcut = combo.filter((/**
             * @param {?} element
             * @return {?}
             */
            element => element !== META_KEY));
            KEY_ALIASES.meta.forEach((/**
             * @param {?} alias
             * @return {?}
             */
            alias => {
                substitutedShortcut.push([...cleanShortcut, alias]);
            }));
        }
        else {
            substitutedShortcut.push(combo);
        }
        return substitutedShortcut;
    }
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    getErrorMessage(message) {
        return `${ERROR_PREFIX} ${message}`;
    }
    /**
     * @private
     * @param {?} shortcutName
     * @param {?} event
     * @return {?}
     */
    isShortcutPressed(shortcutName, event) {
        /** @type {?} */
        const shortcuts = this._shortcuts[shortcutName];
        return shortcuts.some((/**
         * @param {?} shortcut
         * @return {?}
         */
        shortcut => {
            return shortcut.every((/**
             * @param {?} key
             * @return {?}
             */
            key => event[key]));
        }));
    }
    /**
     * @private
     * @param {?} combo
     * @return {?}
     */
    isSupportedCombo(combo) {
        /** @type {?} */
        let unsupportedKey = null;
        combo.forEach((/**
         * @param {?} key
         * @return {?}
         */
        key => {
            if (!SUPPORTED_KEYS[key]) {
                unsupportedKey = key;
                return;
            }
        }));
        return unsupportedKey;
    }
    /**
     * @private
     * @param {?} shortcut
     * @return {?}
     */
    isSupportedShortcut(shortcut) {
        return SUPPORTED_SHORTCUTS[shortcut];
    }
}
ShortcutService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ShortcutService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype._shortcuts;
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype.config;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const createSelectBox = (/**
 * @param {?} container
 * @param {?=} disabled
 * @return {?}
 */
(container, disabled) => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(map((/**
 * @param {?} __0
 * @return {?}
 */
([event, opacity, { x, y }]) => {
    // Type annotation is required here, because `getRelativeMousePosition` returns a `MousePosition`,
    // the TS compiler cannot figure out the shape of this type.
    /** @type {?} */
    const mousePosition = getRelativeMousePosition(event, container);
    /** @type {?} */
    const width = opacity > 0 ? mousePosition.x - x : 0;
    /** @type {?} */
    const height = opacity > 0 ? mousePosition.y - y : 0;
    return {
        top: height < 0 ? mousePosition.y : y,
        left: width < 0 ? mousePosition.x : x,
        width: Math.abs(width),
        height: Math.abs(height),
        opacity: disabled && disabled() ? 0 : opacity
    };
})))));
/** @type {?} */
const whenSelectBoxVisible = (/**
 * @param {?} selectBox$
 * @return {?}
 */
(selectBox$) => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(withLatestFrom(selectBox$), filter((/**
 * @param {?} __0
 * @return {?}
 */
([, selectBox]) => hasMinimumSize(selectBox, 0, 0))), map((/**
 * @param {?} __0
 * @return {?}
 */
([event, _]) => event)))));
/** @type {?} */
const distinctKeyEvents = (/**
 * @return {?}
 */
() => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(distinctUntilChanged((/**
 * @param {?} prev
 * @param {?} curr
 * @return {?}
 */
(prev, curr) => {
    return prev.keyCode === curr.keyCode;
})))));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const UpdateActions = {
    Add: 0,
    Remove: 1,
};
UpdateActions[UpdateActions.Add] = 'Add';
UpdateActions[UpdateActions.Remove] = 'Remove';
/**
 * @record
 */
function UpdateAction() { }
if (false) {
    /** @type {?} */
    UpdateAction.prototype.type;
    /** @type {?} */
    UpdateAction.prototype.item;
}
/**
 * @record
 * @template T
 */
function ObservableProxy() { }
if (false) {
    /** @type {?} */
    ObservableProxy.prototype.proxy$;
    /** @type {?} */
    ObservableProxy.prototype.proxy;
}
/**
 * @record
 */
function SelectContainerHost() { }
if (false) {
    /** @type {?} */
    SelectContainerHost.prototype.boundingClientRect;
}
/**
 * @record
 */
function Shortcuts() { }
if (false) {
    /** @type {?} */
    Shortcuts.prototype.disableSelection;
    /** @type {?} */
    Shortcuts.prototype.toggleSingleItem;
    /** @type {?} */
    Shortcuts.prototype.addToSelection;
    /** @type {?} */
    Shortcuts.prototype.removeFromSelection;
}
/**
 * @record
 */
function DragToSelectConfig() { }
if (false) {
    /** @type {?} */
    DragToSelectConfig.prototype.selectedClass;
    /** @type {?} */
    DragToSelectConfig.prototype.shortcuts;
}
/**
 * @record
 */
function MousePosition() { }
if (false) {
    /** @type {?} */
    MousePosition.prototype.x;
    /** @type {?} */
    MousePosition.prototype.y;
}
/**
 * @record
 */
function BoundingBox() { }
if (false) {
    /** @type {?} */
    BoundingBox.prototype.top;
    /** @type {?} */
    BoundingBox.prototype.left;
    /** @type {?} */
    BoundingBox.prototype.width;
    /** @type {?} */
    BoundingBox.prototype.height;
}
/**
 * @record
 * @template T
 */
function SelectBox() { }
if (false) {
    /** @type {?} */
    SelectBox.prototype.top;
    /** @type {?} */
    SelectBox.prototype.left;
    /** @type {?} */
    SelectBox.prototype.width;
    /** @type {?} */
    SelectBox.prototype.height;
    /** @type {?} */
    SelectBox.prototype.opacity;
}
/** @enum {number} */
const Action = {
    Add: 0,
    Delete: 1,
    None: 2,
};
Action[Action.Add] = 'Add';
Action[Action.Delete] = 'Delete';
Action[Action.None] = 'None';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SelectContainerComponent {
    /**
     * @param {?} shortcuts
     * @param {?} hostElementRef
     * @param {?} renderer
     * @param {?} ngZone
     */
    constructor(shortcuts, hostElementRef, renderer, ngZone) {
        this.shortcuts = shortcuts;
        this.hostElementRef = hostElementRef;
        this.renderer = renderer;
        this.ngZone = ngZone;
        this.selectOnDrag = true;
        this.disabled = false;
        this.disableDrag = false;
        this.selectMode = false;
        this.selectWithShortcut = false;
        this.longTouch = false;
        this.longTouchSeconds = 1;
        this.isMobileDevice = false;
        this.custom = false;
        this.selectedItemsChange = new EventEmitter();
        this.select = new EventEmitter();
        this.itemSelected = new EventEmitter();
        this.itemDeselected = new EventEmitter();
        this.selectionStarted = new EventEmitter();
        this.selectionEnded = new EventEmitter();
        this._tmpItems = new Map();
        this._selectedItems$ = new BehaviorSubject([]);
        this.updateItems$ = new Subject();
        this.destroy$ = new Subject();
        this._disabledByLongTouch = false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.host = this.hostElementRef.nativeElement;
        this._initSelectedItemsChange();
        this._calculateBoundingClientRect();
        this._observeBoundingRectChanges();
        this._observeSelectableItems();
        // distinctKeyEvents is used to prevent multiple key events to be fired repeatedly
        // on Windows when a key is being pressed
        /** @type {?} */
        const keydown$ = fromEvent(window, 'keydown').pipe(distinctKeyEvents(), share());
        /** @type {?} */
        const keyup$ = fromEvent(window, 'keyup').pipe(distinctKeyEvents(), share());
        if (!this.isMobileDevice) {
            /** @type {?} */
            const mouseup$ = fromEvent(window, 'mouseup').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @return {?}
             */
            () => this._onMouseUp())), share());
            /** @type {?} */
            const mousemove$ = fromEvent(window, 'mousemove').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), share());
            /** @type {?} */
            const mousedown$ = fromEvent(this.host, 'mousedown').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => event.button === 0)), // only emit left mouse
            filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => this._onMouseDown(event))), share());
            /** @type {?} */
            const dragging$ = mousedown$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => !this.shortcuts.disableSelection(event))), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @return {?}
             */
            () => !this.disableDrag)), switchMap((/**
             * @return {?}
             */
            () => mousemove$.pipe(takeUntil(mouseup$)))), share());
            /** @type {?} */
            const currentMousePosition$ = mousedown$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            (event) => getRelativeMousePosition(event, this.host))));
            /** @type {?} */
            const show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            const hide$ = mouseup$.pipe(mapTo(0));
            /** @type {?} */
            const opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            const selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host), share());
            this.selectBoxClasses$ = merge(dragging$, mouseup$, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            ([event, selectBox]) => {
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !this.shortcuts.removeFromSelection(event),
                    'dts-removing': this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => JSON.stringify(a) === JSON.stringify(b))));
            /** @type {?} */
            const selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            () => !this.selectOnDrag)), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => this._cursorWithinHost(event))), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            _ => mouseup$.pipe(first()))), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => (!this.shortcuts.disableSelection(event) && !this.shortcuts.toggleSingleItem(event)) ||
                this.shortcuts.removeFromSelection(event))));
            /** @type {?} */
            const selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(mousemove$, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            (selectBox, event) => ({
                selectBox,
                event
            }))), filter((/**
             * @return {?}
             */
            () => this.selectOnDrag)), filter((/**
             * @param {?} __0
             * @return {?}
             */
            ({ selectBox }) => hasMinimumSize(selectBox))), map((/**
             * @param {?} __0
             * @return {?}
             */
            ({ event }) => event)));
            /** @type {?} */
            const selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this._isExtendedSelection(event)) {
                    this._tmpItems.clear();
                }
                else {
                    this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            event => this._selectItems(event)));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            selectBox => ({
                top: `${selectBox.top}px`,
                left: `${selectBox.left}px`,
                width: `${selectBox.width}px`,
                height: `${selectBox.height}px`,
                opacity: selectBox.opacity
            }))));
            this._initSelectionOutputs(mousedown$, mouseup$);
        }
        else {
            this._disabledByLongTouch = this.longTouch;
            /** @type {?} */
            const touchmove$ = fromEvent(window, 'touchmove').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                if (this.longTouch && this._longTouchTimer) {
                    clearInterval(this._longTouchTimer);
                    if (this._disabledByLongTouch) {
                        this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
                    }
                    this._longTouchTimer = null;
                }
            })), share());
            /** @type {?} */
            const touchstart$ = fromEvent(this.host, 'touchstart').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => event.detail === 0)), // only emit left mouse
            filter((/**
             * @return {?}
             */
            () => !this.disabled && (!this.isMobileDevice || !this._longTouchTimer))), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this.longTouch) {
                    this._longTouchSeconds = 0;
                    this.clearSelection();
                    this._longTouchTimer = setInterval((/**
                     * @return {?}
                     */
                    () => {
                        this._longTouchSeconds += 0.1;
                        this._longTouchSeconds = +this._longTouchSeconds.toFixed(2);
                        if (this._longTouchSeconds === this.longTouchSeconds) {
                            this._activateSelectingAfterLongTouch(event);
                        }
                    }), 100);
                }
                this._onMouseDown(event);
            })), share());
            /** @type {?} */
            const touchend$ = fromEvent(window, 'touchend').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @return {?}
             */
            () => {
                if (this.longTouch) {
                    if (this._longTouchTimer) {
                        clearInterval(this._longTouchTimer);
                        this._longTouchTimer = null;
                    }
                    this._deactivateSelectingAfterLongTouch();
                }
                this._onMouseUp();
            })), share());
            /** @type {?} */
            const dragging$ = touchstart$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => !this.shortcuts.disableSelection(event))), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @return {?}
             */
            () => !this.disableDrag)), switchMap((/**
             * @return {?}
             */
            () => touchmove$.pipe(takeUntil(touchend$)))), share());
            /** @type {?} */
            const currentMousePosition$ = touchstart$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            (event) => getRelativeMousePosition(event, this.host))));
            /** @type {?} */
            const show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            const hide$ = touchend$.pipe(mapTo(0));
            /** @type {?} */
            const opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            const selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host, (/**
             * @return {?}
             */
            () => this._disabledByLongTouch)), share());
            this.selectBoxClasses$ = merge(dragging$, touchend$, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            ([event, selectBox]) => {
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !this.shortcuts.removeFromSelection(event),
                    'dts-removing': this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => JSON.stringify(a) === JSON.stringify(b))));
            /** @type {?} */
            const selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            () => !this.selectOnDrag)), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => this._cursorWithinHost(event))), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            _ => touchend$.pipe(first()))), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => (!this.shortcuts.disableSelection(event) && !this.shortcuts.toggleSingleItem(event)) ||
                this.shortcuts.removeFromSelection(event))));
            /** @type {?} */
            const selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(touchmove$, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            (selectBox, event) => ({
                selectBox,
                event
            }))), filter((/**
             * @return {?}
             */
            () => this.selectOnDrag)), filter((/**
             * @param {?} __0
             * @return {?}
             */
            ({ selectBox }) => hasMinimumSize(selectBox))), map((/**
             * @param {?} __0
             * @return {?}
             */
            ({ event }) => event)));
            /** @type {?} */
            const selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this._isExtendedSelection(event)) {
                    this._tmpItems.clear();
                }
                else {
                    this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            event => this._selectItems(event)));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            selectBox => ({
                top: `${selectBox.top}px`,
                left: `${selectBox.left}px`,
                width: `${selectBox.width}px`,
                height: `${selectBox.height}px`,
                opacity: selectBox.opacity
            }))));
            this._initSelectionOutputs(touchstart$, touchend$);
        }
    }
    /**
     * @return {?}
     */
    selectAll() {
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this._selectItem(item);
        }));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    toggleItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._toggleItem(item)));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    selectItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._selectItem(item)));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    deselectItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._deselectItem(item)));
    }
    /**
     * @return {?}
     */
    clearSelection() {
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this._deselectItem(item);
        }));
    }
    /**
     * @return {?}
     */
    update() {
        this._calculateBoundingClientRect();
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => item.calculateBoundingClientRect()));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
    /**
     * @private
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    _filterSelectableItems(predicate) {
        // Wrap select items in an observable for better efficiency as
        // no intermediate arrays are created and we only need to process
        // every item once.
        return from(this.$selectableItems.toArray()).pipe(filter((/**
         * @param {?} item
         * @return {?}
         */
        item => predicate(item.value))));
    }
    /**
     * @private
     * @return {?}
     */
    _initSelectedItemsChange() {
        this._selectedItems$
            .pipe(auditTime(AUDIT_TIME), takeUntil(this.destroy$))
            .subscribe({
            next: (/**
             * @param {?} selectedItems
             * @return {?}
             */
            selectedItems => {
                this.selectedItemsChange.emit(selectedItems);
                this.select.emit(selectedItems);
            }),
            complete: (/**
             * @return {?}
             */
            () => {
                this.selectedItemsChange.emit([]);
            })
        });
    }
    /**
     * @private
     * @return {?}
     */
    _observeSelectableItems() {
        // Listen for updates and either select or deselect an item
        this.updateItems$
            .pipe(withLatestFrom(this._selectedItems$), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ([update, selectedItems]) => {
            /** @type {?} */
            const item = update.item;
            switch (update.type) {
                case UpdateActions.Add:
                    if (this._addItem(item, selectedItems)) {
                        item._select();
                    }
                    break;
                case UpdateActions.Remove:
                    if (this._removeItem(item, selectedItems)) {
                        item._deselect();
                    }
                    break;
            }
        }));
        // Update the container as well as all selectable items if the list has changed
        this.$selectableItems.changes
            .pipe(withLatestFrom(this._selectedItems$), observeOn(asyncScheduler), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ([items, selectedItems]) => {
            /** @type {?} */
            const newList = items.toArray();
            /** @type {?} */
            const removedItems = selectedItems.filter((/**
             * @param {?} item
             * @return {?}
             */
            item => !newList.includes(item.value)));
            if (removedItems.length) {
                removedItems.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => this._removeItem(item, selectedItems)));
            }
            this.update();
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _observeBoundingRectChanges() {
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const resize$ = fromEvent(window, 'resize');
            /** @type {?} */
            const windowScroll$ = fromEvent(window, 'scroll');
            /** @type {?} */
            const containerScroll$ = fromEvent(this.host, 'scroll');
            merge(resize$, windowScroll$, containerScroll$)
                .pipe(startWith('INITIAL_UPDATE'), auditTime(AUDIT_TIME), takeUntil(this.destroy$))
                .subscribe((/**
             * @return {?}
             */
            () => {
                this.update();
            }));
        }));
    }
    /**
     * @private
     * @param {?} mousedown$
     * @param {?} mouseup$
     * @return {?}
     */
    _initSelectionOutputs(mousedown$, mouseup$) {
        mousedown$
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => this._cursorWithinHost(event))), tap((/**
         * @return {?}
         */
        () => this.selectionStarted.emit())), concatMapTo(mouseup$.pipe(first())), withLatestFrom(this._selectedItems$), map((/**
         * @param {?} __0
         * @return {?}
         */
        ([, items]) => items)), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} items
         * @return {?}
         */
        items => {
            this.selectionEnded.emit(items);
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _calculateBoundingClientRect() {
        this.host.boundingClientRect = calculateBoundingClientRect(this.host);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _cursorWithinHost(event) {
        return cursorWithinElement(event, this.host);
    }
    /**
     * @private
     * @return {?}
     */
    _onMouseUp() {
        this._flushItems();
        this.renderer.removeClass(document.body, NO_SELECT_CLASS);
        this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _onMouseDown(event) {
        if (this.shortcuts.disableSelection(event) || this.disabled) {
            return;
        }
        clearSelection(window);
        if (!this.disableDrag && (!this.longTouch || !this._disabledByLongTouch)) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
        }
        if (!this.disableDrag && this.isMobileDevice) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        /** @type {?} */
        const mousePoint = getMousePosition(event);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        (item, index) => {
            /** @type {?} */
            const itemRect = item.getBoundingClientRect();
            /** @type {?} */
            const withinBoundingBox = inBoundingBox(mousePoint, itemRect);
            if (this.shortcuts.extendedSelectionShortcut(event)) {
                return;
            }
            /** @type {?} */
            const shouldAdd = (withinBoundingBox &&
                !this.shortcuts.toggleSingleItem(event) &&
                !this.selectMode &&
                !this.selectWithShortcut) ||
                (withinBoundingBox && this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (!withinBoundingBox && this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (withinBoundingBox && !item.selected && this.selectMode) ||
                (!withinBoundingBox && item.selected && this.selectMode);
            /** @type {?} */
            const shouldRemove = (!withinBoundingBox &&
                !this.shortcuts.toggleSingleItem(event) &&
                !this.selectMode &&
                !this.selectWithShortcut) ||
                (!withinBoundingBox && this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (withinBoundingBox && this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (!withinBoundingBox && !item.selected && this.selectMode) ||
                (withinBoundingBox && item.selected && this.selectMode);
            if (shouldAdd) {
                this._selectItem(item);
            }
            else if (shouldRemove) {
                this._deselectItem(item);
            }
        }));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _selectItems(event) {
        /** @type {?} */
        const selectionBox = calculateBoundingClientRect(this.$selectBox.nativeElement);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            if (this._isExtendedSelection(event)) {
                this._extendedSelectionMode(selectionBox, item, event);
            }
            else {
                this._normalSelectionMode(selectionBox, item, event);
            }
        }));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _isExtendedSelection(event) {
        return this.shortcuts.extendedSelectionShortcut(event) && this.selectOnDrag;
    }
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    _normalSelectionMode(selectBox, item, event) {
        /** @type {?} */
        const inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        const shouldAdd = inSelection && !item.selected && !this.shortcuts.removeFromSelection(event);
        /** @type {?} */
        const shouldRemove = (!inSelection && item.selected && !this.shortcuts.addToSelection(event)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event));
        if (shouldAdd) {
            this._selectItem(item);
        }
        else if (shouldRemove) {
            this._deselectItem(item);
        }
    }
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    _extendedSelectionMode(selectBox, item, event) {
        /** @type {?} */
        const inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        const shoudlAdd = (inSelection && !item.selected && !this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item));
        /** @type {?} */
        const shouldRemove = (!inSelection && item.selected && this.shortcuts.addToSelection(event) && this._tmpItems.has(item)) ||
            (!inSelection && !item.selected && this.shortcuts.removeFromSelection(event) && this._tmpItems.has(item));
        if (shoudlAdd) {
            item.selected ? item._deselect() : item._select();
            /** @type {?} */
            const action = this.shortcuts.removeFromSelection(event)
                ? Action.Delete
                : this.shortcuts.addToSelection(event)
                    ? Action.Add
                    : Action.None;
            this._tmpItems.set(item, action);
        }
        else if (shouldRemove) {
            this.shortcuts.removeFromSelection(event) ? item._select() : item._deselect();
            this._tmpItems.delete(item);
        }
    }
    /**
     * @private
     * @return {?}
     */
    _flushItems() {
        this._tmpItems.forEach((/**
         * @param {?} action
         * @param {?} item
         * @return {?}
         */
        (action, item) => {
            if (action === Action.Add) {
                this._selectItem(item);
            }
            if (action === Action.Delete) {
                this._deselectItem(item);
            }
        }));
        this._tmpItems.clear();
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _addItem(item, selectedItems) {
        /** @type {?} */
        let success = false;
        if (!this._disabledByLongTouch && !this._hasItem(item, selectedItems)) {
            success = true;
            selectedItems.push(item.value);
            this._selectedItems$.next(selectedItems);
            this.itemSelected.emit(item.value);
        }
        return success;
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _removeItem(item, selectedItems) {
        /** @type {?} */
        let success = false;
        /** @type {?} */
        const value = item instanceof SelectItemDirective ? item.value : item;
        /** @type {?} */
        const index = selectedItems.indexOf(value);
        if (index > -1) {
            success = true;
            selectedItems.splice(index, 1);
            this._selectedItems$.next(selectedItems);
            this.itemDeselected.emit(item.value);
        }
        return success;
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _toggleItem(item) {
        if (item.selected) {
            this._deselectItem(item);
        }
        else {
            this._selectItem(item);
        }
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _selectItem(item) {
        this.updateItems$.next({ type: UpdateActions.Add, item });
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _deselectItem(item) {
        this.updateItems$.next({ type: UpdateActions.Remove, item });
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _hasItem(item, selectedItems) {
        return selectedItems.includes(item.value);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _drawRipple(event) {
        /** @type {?} */
        const position = getMousePosition(event);
        /** @type {?} */
        const node = document.querySelector('.ripple');
        /** @type {?} */
        const newNode = (/** @type {?} */ (node.cloneNode(true)));
        newNode.classList.add('animate');
        newNode.style.left = position.x - 5 + 'px';
        newNode.style.top = position.y - 5 + 'px';
        node.parentNode.replaceChild(newNode, node);
    }
    ;
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _activateSelectingAfterLongTouch(event) {
        this._disabledByLongTouch = false;
        clearInterval(this._longTouchTimer);
        this._longTouchTimer = null;
        this.$selectBox.nativeElement.style.opacity = 1;
        if (!this.disableDrag) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        this._drawRipple(event);
    }
    /**
     * @private
     * @return {?}
     */
    _deactivateSelectingAfterLongTouch() {
        this._disabledByLongTouch = true;
        this.$selectBox.nativeElement.style.opacity = 0;
        document.body.style.overflow = 'auto';
    }
    /**
     * @private
     * @param {?} msg
     * @return {?}
     */
    log(msg) {
        document.getElementById('messages').style.whiteSpace = 'normal';
        document.getElementById('messages').innerHTML += ' ' + msg;
    }
}
SelectContainerComponent.decorators = [
    { type: Component, args: [{
                selector: 'dts-select-container',
                exportAs: 'dts-select-container',
                host: {
                    class: 'dts-select-container'
                },
                template: `
    <ng-content></ng-content>
    <div class="ripple" #ripple></div>
    <div
      class="dts-select-box"
      #selectBox
      [ngClass]="selectBoxClasses$ | async"
      [ngStyle]="selectBoxStyles$ | async"
    ></div>
  `,
                styles: [":host{display:block;position:relative}::ng-deep .dts-no-select dts-select-container{touch-action:none}::ng-deep body.dts-no-select-mobile{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}::ng-deep body.dts-no-select.dts-no-select-mobile{overflow:hidden!important}.ripple{width:20px;height:20px;opacity:0;-webkit-transform:scale(0);transform:scale(0);background:rgba(20,178,252,.5);border-radius:50%;position:fixed}.animate{-webkit-animation:1s cubic-bezier(0,0,.2,1) ripple-mo;animation:1s cubic-bezier(0,0,.2,1) ripple-mo}@-webkit-keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}@keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}"]
            }] }
];
/** @nocollapse */
SelectContainerComponent.ctorParameters = () => [
    { type: ShortcutService },
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgZone }
];
SelectContainerComponent.propDecorators = {
    $selectBox: [{ type: ViewChild, args: ['selectBox', { static: true },] }],
    $ripple: [{ type: ViewChild, args: ['ripple', { static: true },] }],
    $selectableItems: [{ type: ContentChildren, args: [SelectItemDirective, { descendants: true },] }],
    selectedItems: [{ type: Input }],
    selectOnDrag: [{ type: Input }],
    disabled: [{ type: Input }],
    disableDrag: [{ type: Input }],
    selectMode: [{ type: Input }],
    selectWithShortcut: [{ type: Input }],
    longTouch: [{ type: Input }],
    longTouchSeconds: [{ type: Input }],
    isMobileDevice: [{ type: Input }],
    custom: [{ type: Input }, { type: HostBinding, args: ['class.dts-custom',] }],
    selectedItemsChange: [{ type: Output }],
    select: [{ type: Output }],
    itemSelected: [{ type: Output }],
    itemDeselected: [{ type: Output }],
    selectionStarted: [{ type: Output }],
    selectionEnded: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SelectContainerComponent.prototype.host;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxStyles$;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxClasses$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectBox;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$ripple;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectableItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectOnDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.disabled;
    /** @type {?} */
    SelectContainerComponent.prototype.disableDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.selectMode;
    /** @type {?} */
    SelectContainerComponent.prototype.selectWithShortcut;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouch;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouchSeconds;
    /** @type {?} */
    SelectContainerComponent.prototype.isMobileDevice;
    /** @type {?} */
    SelectContainerComponent.prototype.custom;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItemsChange;
    /** @type {?} */
    SelectContainerComponent.prototype.select;
    /** @type {?} */
    SelectContainerComponent.prototype.itemSelected;
    /** @type {?} */
    SelectContainerComponent.prototype.itemDeselected;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionStarted;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionEnded;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._tmpItems;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._selectedItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.updateItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.destroy$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchTimer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchSeconds;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._disabledByLongTouch;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.shortcuts;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.hostElementRef;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.ngZone;
    /* Skipping unhandled member: ;*/
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const DEFAULT_CONFIG = {
    selectedClass: 'selected',
    shortcuts: {
        disableSelection: 'alt',
        toggleSingleItem: 'meta',
        addToSelection: 'shift',
        removeFromSelection: 'shift+meta'
    }
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const COMPONENTS = [SelectContainerComponent, SelectItemDirective];
/**
 * @param {?} config
 * @return {?}
 */
function CONFIG_FACTORY(config) {
    return mergeDeep(DEFAULT_CONFIG, config);
}
class DragToSelectModule {
    /**
     * @param {?=} config
     * @return {?}
     */
    static forRoot(config = {}) {
        return {
            ngModule: DragToSelectModule,
            providers: [
                ShortcutService,
                { provide: USER_CONFIG, useValue: config },
                {
                    provide: CONFIG,
                    useFactory: CONFIG_FACTORY,
                    deps: [USER_CONFIG]
                }
            ]
        };
    }
}
DragToSelectModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                declarations: [...COMPONENTS],
                exports: [...COMPONENTS]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { CONFIG_FACTORY, DragToSelectModule, SelectContainerComponent, SelectItemDirective, mergeDeep as ɵa, DEFAULT_CONFIG as ɵb, CONFIG as ɵc, USER_CONFIG as ɵd, ShortcutService as ɵf };
//# sourceMappingURL=ngx-drag-to-select.js.map
