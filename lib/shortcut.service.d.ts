import { DragToSelectConfig } from './models';
export declare class ShortcutService {
    private config;
    private _shortcuts;
    constructor(config: DragToSelectConfig);
    disableSelection(event: Event): boolean;
    toggleSingleItem(event: Event): boolean;
    addToSelection(event: Event): boolean;
    removeFromSelection(event: Event): boolean;
    extendedSelectionShortcut(event: Event): boolean;
    private createShortcutsFromConfig;
    private substituteKey;
    private getErrorMessage;
    private isShortcutPressed;
    private isSupportedCombo;
    private isSupportedShortcut;
}
