import { DoCheck, ElementRef, Renderer2 } from '@angular/core';
import { DragToSelectConfig } from './models';
export declare class SelectItemDirective implements DoCheck {
    private config;
    private platformId;
    private host;
    private renderer;
    private _boundingClientRect;
    selected: boolean;
    dtsSelectItem: any;
    readonly value: any;
    constructor(config: DragToSelectConfig, platformId: any, host: ElementRef, renderer: Renderer2);
    ngDoCheck(): void;
    getBoundingClientRect(): any;
    calculateBoundingClientRect(): void;
    _select(): void;
    _deselect(): void;
    private applySelectedClass;
}
