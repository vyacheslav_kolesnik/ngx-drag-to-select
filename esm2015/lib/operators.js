/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';
import { getRelativeMousePosition, hasMinimumSize } from './utils';
/** @type {?} */
export const createSelectBox = (/**
 * @param {?} container
 * @param {?=} disabled
 * @return {?}
 */
(container, disabled) => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(map((/**
 * @param {?} __0
 * @return {?}
 */
([event, opacity, { x, y }]) => {
    // Type annotation is required here, because `getRelativeMousePosition` returns a `MousePosition`,
    // the TS compiler cannot figure out the shape of this type.
    /** @type {?} */
    const mousePosition = getRelativeMousePosition(event, container);
    /** @type {?} */
    const width = opacity > 0 ? mousePosition.x - x : 0;
    /** @type {?} */
    const height = opacity > 0 ? mousePosition.y - y : 0;
    return {
        top: height < 0 ? mousePosition.y : y,
        left: width < 0 ? mousePosition.x : x,
        width: Math.abs(width),
        height: Math.abs(height),
        opacity: disabled && disabled() ? 0 : opacity
    };
})))));
/** @type {?} */
export const whenSelectBoxVisible = (/**
 * @param {?} selectBox$
 * @return {?}
 */
(selectBox$) => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(withLatestFrom(selectBox$), filter((/**
 * @param {?} __0
 * @return {?}
 */
([, selectBox]) => hasMinimumSize(selectBox, 0, 0))), map((/**
 * @param {?} __0
 * @return {?}
 */
([event, _]) => event)))));
/** @type {?} */
export const distinctKeyEvents = (/**
 * @return {?}
 */
() => (/**
 * @param {?} source
 * @return {?}
 */
(source) => source.pipe(distinctUntilChanged((/**
 * @param {?} prev
 * @param {?} curr
 * @return {?}
 */
(prev, curr) => {
    return prev.keyCode === curr.keyCode;
})))));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0b3JzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdG9ycy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbkYsT0FBTyxFQUFFLHdCQUF3QixFQUFFLGNBQWMsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7QUFFbkUsTUFBTSxPQUFPLGVBQWU7Ozs7O0FBQUcsQ0FBQyxTQUE4QixFQUFFLFFBQXdCLEVBQUUsRUFBRTs7OztBQUFDLENBQzNGLE1BQWtDLEVBQ0gsRUFBRSxDQUNqQyxNQUFNLENBQUMsSUFBSSxDQUNULEdBQUc7Ozs7QUFBQyxDQUFDLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Ozs7VUFHM0IsYUFBYSxHQUFrQix3QkFBd0IsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDOztVQUV6RSxLQUFLLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7O1VBQzdDLE1BQU0sR0FBRyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUVwRCxPQUFPO1FBQ0wsR0FBRyxFQUFFLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsSUFBSSxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ3RCLE1BQU0sRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUN4QixPQUFPLEVBQUUsUUFBUSxJQUFJLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87S0FDOUMsQ0FBQztBQUNKLENBQUMsRUFBQyxDQUNILENBQUEsQ0FBQTs7QUFFSCxNQUFNLE9BQU8sb0JBQW9COzs7O0FBQUcsQ0FBQyxVQUF5QyxFQUFFLEVBQUU7Ozs7QUFBQyxDQUFDLE1BQXlCLEVBQUUsRUFBRSxDQUMvRyxNQUFNLENBQUMsSUFBSSxDQUNULGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFDMUIsTUFBTTs7OztBQUFDLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxFQUMxRCxHQUFHOzs7O0FBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFDLENBQzNCLENBQUEsQ0FBQTs7QUFFSCxNQUFNLE9BQU8saUJBQWlCOzs7QUFBRyxHQUFHLEVBQUU7Ozs7QUFBQyxDQUFDLE1BQWlDLEVBQUUsRUFBRSxDQUMzRSxNQUFNLENBQUMsSUFBSSxDQUNULG9CQUFvQjs7Ozs7QUFBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRTtJQUNsQyxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQztBQUN2QyxDQUFDLEVBQUMsQ0FDSCxDQUFBLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBmaWx0ZXIsIG1hcCwgd2l0aExhdGVzdEZyb20gfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1vdXNlUG9zaXRpb24sIFNlbGVjdEJveCwgU2VsZWN0Qm94SW5wdXQsIFNlbGVjdENvbnRhaW5lckhvc3QgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbiwgaGFzTWluaW11bVNpemUgfSBmcm9tICcuL3V0aWxzJztcclxuXHJcbmV4cG9ydCBjb25zdCBjcmVhdGVTZWxlY3RCb3ggPSAoY29udGFpbmVyOiBTZWxlY3RDb250YWluZXJIb3N0LCBkaXNhYmxlZD86ICgpID0+IGJvb2xlYW4pID0+IChcclxuICBzb3VyY2U6IE9ic2VydmFibGU8U2VsZWN0Qm94SW5wdXQ+XHJcbik6IE9ic2VydmFibGU8U2VsZWN0Qm94PG51bWJlcj4+ID0+XHJcbiAgc291cmNlLnBpcGUoXHJcbiAgICBtYXAoKFtldmVudCwgb3BhY2l0eSwgeyB4LCB5IH1dKSA9PiB7XHJcbiAgICAgIC8vIFR5cGUgYW5ub3RhdGlvbiBpcyByZXF1aXJlZCBoZXJlLCBiZWNhdXNlIGBnZXRSZWxhdGl2ZU1vdXNlUG9zaXRpb25gIHJldHVybnMgYSBgTW91c2VQb3NpdGlvbmAsXHJcbiAgICAgIC8vIHRoZSBUUyBjb21waWxlciBjYW5ub3QgZmlndXJlIG91dCB0aGUgc2hhcGUgb2YgdGhpcyB0eXBlLlxyXG4gICAgICBjb25zdCBtb3VzZVBvc2l0aW9uOiBNb3VzZVBvc2l0aW9uID0gZ2V0UmVsYXRpdmVNb3VzZVBvc2l0aW9uKGV2ZW50LCBjb250YWluZXIpO1xyXG5cclxuICAgICAgY29uc3Qgd2lkdGggPSBvcGFjaXR5ID4gMCA/IG1vdXNlUG9zaXRpb24ueCAtIHggOiAwO1xyXG4gICAgICBjb25zdCBoZWlnaHQgPSBvcGFjaXR5ID4gMCA/IG1vdXNlUG9zaXRpb24ueSAtIHkgOiAwO1xyXG5cclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICB0b3A6IGhlaWdodCA8IDAgPyBtb3VzZVBvc2l0aW9uLnkgOiB5LFxyXG4gICAgICAgIGxlZnQ6IHdpZHRoIDwgMCA/IG1vdXNlUG9zaXRpb24ueCA6IHgsXHJcbiAgICAgICAgd2lkdGg6IE1hdGguYWJzKHdpZHRoKSxcclxuICAgICAgICBoZWlnaHQ6IE1hdGguYWJzKGhlaWdodCksXHJcbiAgICAgICAgb3BhY2l0eTogZGlzYWJsZWQgJiYgZGlzYWJsZWQoKSA/IDAgOiBvcGFjaXR5XHJcbiAgICAgIH07XHJcbiAgICB9KVxyXG4gICk7XHJcblxyXG5leHBvcnQgY29uc3Qgd2hlblNlbGVjdEJveFZpc2libGUgPSAoc2VsZWN0Qm94JDogT2JzZXJ2YWJsZTxTZWxlY3RCb3g8bnVtYmVyPj4pID0+IChzb3VyY2U6IE9ic2VydmFibGU8RXZlbnQ+KSA9PlxyXG4gIHNvdXJjZS5waXBlKFxyXG4gICAgd2l0aExhdGVzdEZyb20oc2VsZWN0Qm94JCksXHJcbiAgICBmaWx0ZXIoKFssIHNlbGVjdEJveF0pID0+IGhhc01pbmltdW1TaXplKHNlbGVjdEJveCwgMCwgMCkpLFxyXG4gICAgbWFwKChbZXZlbnQsIF9dKSA9PiBldmVudClcclxuICApO1xyXG5cclxuZXhwb3J0IGNvbnN0IGRpc3RpbmN0S2V5RXZlbnRzID0gKCkgPT4gKHNvdXJjZTogT2JzZXJ2YWJsZTxLZXlib2FyZEV2ZW50PikgPT5cclxuICBzb3VyY2UucGlwZShcclxuICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKChwcmV2LCBjdXJyKSA9PiB7XHJcbiAgICAgIHJldHVybiBwcmV2LmtleUNvZGUgPT09IGN1cnIua2V5Q29kZTtcclxuICAgIH0pXHJcbiAgKTtcclxuIl19