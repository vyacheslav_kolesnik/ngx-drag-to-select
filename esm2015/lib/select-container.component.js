/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Output, EventEmitter, Input, Renderer2, ViewChild, NgZone, ContentChildren, QueryList, HostBinding } from '@angular/core';
import { Subject, combineLatest, merge, from, fromEvent, BehaviorSubject, asyncScheduler } from 'rxjs';
import { switchMap, takeUntil, map, tap, filter, auditTime, mapTo, share, withLatestFrom, distinctUntilChanged, observeOn, startWith, concatMapTo, first } from 'rxjs/operators';
import { SelectItemDirective } from './select-item.directive';
import { ShortcutService } from './shortcut.service';
import { createSelectBox, whenSelectBoxVisible, distinctKeyEvents } from './operators';
import { Action, UpdateActions } from './models';
import { AUDIT_TIME, NO_SELECT_CLASS, NO_SELECT_CLASS_MOBILE } from './constants';
import { inBoundingBox, cursorWithinElement, clearSelection, boxIntersects, calculateBoundingClientRect, getRelativeMousePosition, getMousePosition, hasMinimumSize } from './utils';
export class SelectContainerComponent {
    /**
     * @param {?} shortcuts
     * @param {?} hostElementRef
     * @param {?} renderer
     * @param {?} ngZone
     */
    constructor(shortcuts, hostElementRef, renderer, ngZone) {
        this.shortcuts = shortcuts;
        this.hostElementRef = hostElementRef;
        this.renderer = renderer;
        this.ngZone = ngZone;
        this.selectOnDrag = true;
        this.disabled = false;
        this.disableDrag = false;
        this.selectMode = false;
        this.selectWithShortcut = false;
        this.longTouch = false;
        this.longTouchSeconds = 1;
        this.isMobileDevice = false;
        this.custom = false;
        this.selectedItemsChange = new EventEmitter();
        this.select = new EventEmitter();
        this.itemSelected = new EventEmitter();
        this.itemDeselected = new EventEmitter();
        this.selectionStarted = new EventEmitter();
        this.selectionEnded = new EventEmitter();
        this._tmpItems = new Map();
        this._selectedItems$ = new BehaviorSubject([]);
        this.updateItems$ = new Subject();
        this.destroy$ = new Subject();
        this._disabledByLongTouch = false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.host = this.hostElementRef.nativeElement;
        this._initSelectedItemsChange();
        this._calculateBoundingClientRect();
        this._observeBoundingRectChanges();
        this._observeSelectableItems();
        // distinctKeyEvents is used to prevent multiple key events to be fired repeatedly
        // on Windows when a key is being pressed
        /** @type {?} */
        const keydown$ = fromEvent(window, 'keydown').pipe(distinctKeyEvents(), share());
        /** @type {?} */
        const keyup$ = fromEvent(window, 'keyup').pipe(distinctKeyEvents(), share());
        if (!this.isMobileDevice) {
            /** @type {?} */
            const mouseup$ = fromEvent(window, 'mouseup').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @return {?}
             */
            () => this._onMouseUp())), share());
            /** @type {?} */
            const mousemove$ = fromEvent(window, 'mousemove').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), share());
            /** @type {?} */
            const mousedown$ = fromEvent(this.host, 'mousedown').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => event.button === 0)), // only emit left mouse
            filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => this._onMouseDown(event))), share());
            /** @type {?} */
            const dragging$ = mousedown$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => !this.shortcuts.disableSelection(event))), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @return {?}
             */
            () => !this.disableDrag)), switchMap((/**
             * @return {?}
             */
            () => mousemove$.pipe(takeUntil(mouseup$)))), share());
            /** @type {?} */
            const currentMousePosition$ = mousedown$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            (event) => getRelativeMousePosition(event, this.host))));
            /** @type {?} */
            const show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            const hide$ = mouseup$.pipe(mapTo(0));
            /** @type {?} */
            const opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            const selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host), share());
            this.selectBoxClasses$ = merge(dragging$, mouseup$, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            ([event, selectBox]) => {
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !this.shortcuts.removeFromSelection(event),
                    'dts-removing': this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => JSON.stringify(a) === JSON.stringify(b))));
            /** @type {?} */
            const selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            () => !this.selectOnDrag)), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => this._cursorWithinHost(event))), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            _ => mouseup$.pipe(first()))), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => (!this.shortcuts.disableSelection(event) && !this.shortcuts.toggleSingleItem(event)) ||
                this.shortcuts.removeFromSelection(event))));
            /** @type {?} */
            const selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(mousemove$, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            (selectBox, event) => ({
                selectBox,
                event
            }))), filter((/**
             * @return {?}
             */
            () => this.selectOnDrag)), filter((/**
             * @param {?} __0
             * @return {?}
             */
            ({ selectBox }) => hasMinimumSize(selectBox))), map((/**
             * @param {?} __0
             * @return {?}
             */
            ({ event }) => event)));
            /** @type {?} */
            const selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this._isExtendedSelection(event)) {
                    this._tmpItems.clear();
                }
                else {
                    this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            event => this._selectItems(event)));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            selectBox => ({
                top: `${selectBox.top}px`,
                left: `${selectBox.left}px`,
                width: `${selectBox.width}px`,
                height: `${selectBox.height}px`,
                opacity: selectBox.opacity
            }))));
            this._initSelectionOutputs(mousedown$, mouseup$);
        }
        else {
            this._disabledByLongTouch = this.longTouch;
            /** @type {?} */
            const touchmove$ = fromEvent(window, 'touchmove').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                if (this.longTouch && this._longTouchTimer) {
                    clearInterval(this._longTouchTimer);
                    if (this._disabledByLongTouch) {
                        this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
                    }
                    this._longTouchTimer = null;
                }
            })), share());
            /** @type {?} */
            const touchstart$ = fromEvent(this.host, 'touchstart').pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => event.detail === 0)), // only emit left mouse
            filter((/**
             * @return {?}
             */
            () => !this.disabled && (!this.isMobileDevice || !this._longTouchTimer))), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this.longTouch) {
                    this._longTouchSeconds = 0;
                    this.clearSelection();
                    this._longTouchTimer = setInterval((/**
                     * @return {?}
                     */
                    () => {
                        this._longTouchSeconds += 0.1;
                        this._longTouchSeconds = +this._longTouchSeconds.toFixed(2);
                        if (this._longTouchSeconds === this.longTouchSeconds) {
                            this._activateSelectingAfterLongTouch(event);
                        }
                    }), 100);
                }
                this._onMouseDown(event);
            })), share());
            /** @type {?} */
            const touchend$ = fromEvent(window, 'touchend').pipe(filter((/**
             * @return {?}
             */
            () => !this.disabled)), tap((/**
             * @return {?}
             */
            () => {
                if (this.longTouch) {
                    if (this._longTouchTimer) {
                        clearInterval(this._longTouchTimer);
                        this._longTouchTimer = null;
                    }
                    this._deactivateSelectingAfterLongTouch();
                }
                this._onMouseUp();
            })), share());
            /** @type {?} */
            const dragging$ = touchstart$.pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            event => !this.shortcuts.disableSelection(event))), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @return {?}
             */
            () => !this.disableDrag)), switchMap((/**
             * @return {?}
             */
            () => touchmove$.pipe(takeUntil(touchend$)))), share());
            /** @type {?} */
            const currentMousePosition$ = touchstart$.pipe(map((/**
             * @param {?} event
             * @return {?}
             */
            (event) => getRelativeMousePosition(event, this.host))));
            /** @type {?} */
            const show$ = dragging$.pipe(mapTo(1));
            /** @type {?} */
            const hide$ = touchend$.pipe(mapTo(0));
            /** @type {?} */
            const opacity$ = merge(show$, hide$).pipe(distinctUntilChanged());
            /** @type {?} */
            const selectBox$ = combineLatest(dragging$, opacity$, currentMousePosition$).pipe(createSelectBox(this.host, (/**
             * @return {?}
             */
            () => this._disabledByLongTouch)), share());
            this.selectBoxClasses$ = merge(dragging$, touchend$, keydown$, keyup$).pipe(auditTime(AUDIT_TIME), withLatestFrom(selectBox$), map((/**
             * @param {?} __0
             * @return {?}
             */
            ([event, selectBox]) => {
                return {
                    'dts-adding': hasMinimumSize(selectBox, 0, 0) && !this.shortcuts.removeFromSelection(event),
                    'dts-removing': this.shortcuts.removeFromSelection(event)
                };
            })), distinctUntilChanged((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => JSON.stringify(a) === JSON.stringify(b))));
            /** @type {?} */
            const selectOnMouseUp$ = dragging$.pipe(filter((/**
             * @return {?}
             */
            () => !this.selectOnDrag)), filter((/**
             * @return {?}
             */
            () => !this.selectMode)), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => this._cursorWithinHost(event))), switchMap((/**
             * @param {?} _
             * @return {?}
             */
            _ => touchend$.pipe(first()))), filter((/**
             * @param {?} event
             * @return {?}
             */
            event => (!this.shortcuts.disableSelection(event) && !this.shortcuts.toggleSingleItem(event)) ||
                this.shortcuts.removeFromSelection(event))));
            /** @type {?} */
            const selectOnDrag$ = selectBox$.pipe(auditTime(AUDIT_TIME), withLatestFrom(touchmove$, (/**
             * @param {?} selectBox
             * @param {?} event
             * @return {?}
             */
            (selectBox, event) => ({
                selectBox,
                event
            }))), filter((/**
             * @return {?}
             */
            () => this.selectOnDrag)), filter((/**
             * @param {?} __0
             * @return {?}
             */
            ({ selectBox }) => hasMinimumSize(selectBox))), map((/**
             * @param {?} __0
             * @return {?}
             */
            ({ event }) => event)));
            /** @type {?} */
            const selectOnKeyboardEvent$ = merge(keydown$, keyup$).pipe(auditTime(AUDIT_TIME), whenSelectBoxVisible(selectBox$), tap((/**
             * @param {?} event
             * @return {?}
             */
            event => {
                if (this._isExtendedSelection(event)) {
                    this._tmpItems.clear();
                }
                else {
                    this._flushItems();
                }
            })));
            merge(selectOnMouseUp$, selectOnDrag$, selectOnKeyboardEvent$)
                .pipe(takeUntil(this.destroy$))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            event => this._selectItems(event)));
            this.selectBoxStyles$ = selectBox$.pipe(map((/**
             * @param {?} selectBox
             * @return {?}
             */
            selectBox => ({
                top: `${selectBox.top}px`,
                left: `${selectBox.left}px`,
                width: `${selectBox.width}px`,
                height: `${selectBox.height}px`,
                opacity: selectBox.opacity
            }))));
            this._initSelectionOutputs(touchstart$, touchend$);
        }
    }
    /**
     * @return {?}
     */
    selectAll() {
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this._selectItem(item);
        }));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    toggleItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._toggleItem(item)));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    selectItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._selectItem(item)));
    }
    /**
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    deselectItems(predicate) {
        this._filterSelectableItems(predicate).subscribe((/**
         * @param {?} item
         * @return {?}
         */
        (item) => this._deselectItem(item)));
    }
    /**
     * @return {?}
     */
    clearSelection() {
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this._deselectItem(item);
        }));
    }
    /**
     * @return {?}
     */
    update() {
        this._calculateBoundingClientRect();
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => item.calculateBoundingClientRect()));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }
    /**
     * @private
     * @template T
     * @param {?} predicate
     * @return {?}
     */
    _filterSelectableItems(predicate) {
        // Wrap select items in an observable for better efficiency as
        // no intermediate arrays are created and we only need to process
        // every item once.
        return from(this.$selectableItems.toArray()).pipe(filter((/**
         * @param {?} item
         * @return {?}
         */
        item => predicate(item.value))));
    }
    /**
     * @private
     * @return {?}
     */
    _initSelectedItemsChange() {
        this._selectedItems$
            .pipe(auditTime(AUDIT_TIME), takeUntil(this.destroy$))
            .subscribe({
            next: (/**
             * @param {?} selectedItems
             * @return {?}
             */
            selectedItems => {
                this.selectedItemsChange.emit(selectedItems);
                this.select.emit(selectedItems);
            }),
            complete: (/**
             * @return {?}
             */
            () => {
                this.selectedItemsChange.emit([]);
            })
        });
    }
    /**
     * @private
     * @return {?}
     */
    _observeSelectableItems() {
        // Listen for updates and either select or deselect an item
        this.updateItems$
            .pipe(withLatestFrom(this._selectedItems$), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ([update, selectedItems]) => {
            /** @type {?} */
            const item = update.item;
            switch (update.type) {
                case UpdateActions.Add:
                    if (this._addItem(item, selectedItems)) {
                        item._select();
                    }
                    break;
                case UpdateActions.Remove:
                    if (this._removeItem(item, selectedItems)) {
                        item._deselect();
                    }
                    break;
            }
        }));
        // Update the container as well as all selectable items if the list has changed
        this.$selectableItems.changes
            .pipe(withLatestFrom(this._selectedItems$), observeOn(asyncScheduler), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ([items, selectedItems]) => {
            /** @type {?} */
            const newList = items.toArray();
            /** @type {?} */
            const removedItems = selectedItems.filter((/**
             * @param {?} item
             * @return {?}
             */
            item => !newList.includes(item.value)));
            if (removedItems.length) {
                removedItems.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => this._removeItem(item, selectedItems)));
            }
            this.update();
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _observeBoundingRectChanges() {
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const resize$ = fromEvent(window, 'resize');
            /** @type {?} */
            const windowScroll$ = fromEvent(window, 'scroll');
            /** @type {?} */
            const containerScroll$ = fromEvent(this.host, 'scroll');
            merge(resize$, windowScroll$, containerScroll$)
                .pipe(startWith('INITIAL_UPDATE'), auditTime(AUDIT_TIME), takeUntil(this.destroy$))
                .subscribe((/**
             * @return {?}
             */
            () => {
                this.update();
            }));
        }));
    }
    /**
     * @private
     * @param {?} mousedown$
     * @param {?} mouseup$
     * @return {?}
     */
    _initSelectionOutputs(mousedown$, mouseup$) {
        mousedown$
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => this._cursorWithinHost(event))), tap((/**
         * @return {?}
         */
        () => this.selectionStarted.emit())), concatMapTo(mouseup$.pipe(first())), withLatestFrom(this._selectedItems$), map((/**
         * @param {?} __0
         * @return {?}
         */
        ([, items]) => items)), takeUntil(this.destroy$))
            .subscribe((/**
         * @param {?} items
         * @return {?}
         */
        items => {
            this.selectionEnded.emit(items);
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _calculateBoundingClientRect() {
        this.host.boundingClientRect = calculateBoundingClientRect(this.host);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _cursorWithinHost(event) {
        return cursorWithinElement(event, this.host);
    }
    /**
     * @private
     * @return {?}
     */
    _onMouseUp() {
        this._flushItems();
        this.renderer.removeClass(document.body, NO_SELECT_CLASS);
        this.renderer.removeClass(document.body, NO_SELECT_CLASS_MOBILE);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _onMouseDown(event) {
        if (this.shortcuts.disableSelection(event) || this.disabled) {
            return;
        }
        clearSelection(window);
        if (!this.disableDrag && (!this.longTouch || !this._disabledByLongTouch)) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
        }
        if (!this.disableDrag && this.isMobileDevice) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        /** @type {?} */
        const mousePoint = getMousePosition(event);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        (item, index) => {
            /** @type {?} */
            const itemRect = item.getBoundingClientRect();
            /** @type {?} */
            const withinBoundingBox = inBoundingBox(mousePoint, itemRect);
            if (this.shortcuts.extendedSelectionShortcut(event)) {
                return;
            }
            /** @type {?} */
            const shouldAdd = (withinBoundingBox &&
                !this.shortcuts.toggleSingleItem(event) &&
                !this.selectMode &&
                !this.selectWithShortcut) ||
                (withinBoundingBox && this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (!withinBoundingBox && this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (withinBoundingBox && !item.selected && this.selectMode) ||
                (!withinBoundingBox && item.selected && this.selectMode);
            /** @type {?} */
            const shouldRemove = (!withinBoundingBox &&
                !this.shortcuts.toggleSingleItem(event) &&
                !this.selectMode &&
                !this.selectWithShortcut) ||
                (!withinBoundingBox && this.shortcuts.toggleSingleItem(event) && !item.selected) ||
                (withinBoundingBox && this.shortcuts.toggleSingleItem(event) && item.selected) ||
                (!withinBoundingBox && !item.selected && this.selectMode) ||
                (withinBoundingBox && item.selected && this.selectMode);
            if (shouldAdd) {
                this._selectItem(item);
            }
            else if (shouldRemove) {
                this._deselectItem(item);
            }
        }));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _selectItems(event) {
        /** @type {?} */
        const selectionBox = calculateBoundingClientRect(this.$selectBox.nativeElement);
        this.$selectableItems.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            if (this._isExtendedSelection(event)) {
                this._extendedSelectionMode(selectionBox, item, event);
            }
            else {
                this._normalSelectionMode(selectionBox, item, event);
            }
        }));
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _isExtendedSelection(event) {
        return this.shortcuts.extendedSelectionShortcut(event) && this.selectOnDrag;
    }
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    _normalSelectionMode(selectBox, item, event) {
        /** @type {?} */
        const inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        const shouldAdd = inSelection && !item.selected && !this.shortcuts.removeFromSelection(event);
        /** @type {?} */
        const shouldRemove = (!inSelection && item.selected && !this.shortcuts.addToSelection(event)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event));
        if (shouldAdd) {
            this._selectItem(item);
        }
        else if (shouldRemove) {
            this._deselectItem(item);
        }
    }
    /**
     * @private
     * @param {?} selectBox
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    _extendedSelectionMode(selectBox, item, event) {
        /** @type {?} */
        const inSelection = boxIntersects(selectBox, item.getBoundingClientRect());
        /** @type {?} */
        const shoudlAdd = (inSelection && !item.selected && !this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item)) ||
            (inSelection && item.selected && this.shortcuts.removeFromSelection(event) && !this._tmpItems.has(item));
        /** @type {?} */
        const shouldRemove = (!inSelection && item.selected && this.shortcuts.addToSelection(event) && this._tmpItems.has(item)) ||
            (!inSelection && !item.selected && this.shortcuts.removeFromSelection(event) && this._tmpItems.has(item));
        if (shoudlAdd) {
            item.selected ? item._deselect() : item._select();
            /** @type {?} */
            const action = this.shortcuts.removeFromSelection(event)
                ? Action.Delete
                : this.shortcuts.addToSelection(event)
                    ? Action.Add
                    : Action.None;
            this._tmpItems.set(item, action);
        }
        else if (shouldRemove) {
            this.shortcuts.removeFromSelection(event) ? item._select() : item._deselect();
            this._tmpItems.delete(item);
        }
    }
    /**
     * @private
     * @return {?}
     */
    _flushItems() {
        this._tmpItems.forEach((/**
         * @param {?} action
         * @param {?} item
         * @return {?}
         */
        (action, item) => {
            if (action === Action.Add) {
                this._selectItem(item);
            }
            if (action === Action.Delete) {
                this._deselectItem(item);
            }
        }));
        this._tmpItems.clear();
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _addItem(item, selectedItems) {
        /** @type {?} */
        let success = false;
        if (!this._disabledByLongTouch && !this._hasItem(item, selectedItems)) {
            success = true;
            selectedItems.push(item.value);
            this._selectedItems$.next(selectedItems);
            this.itemSelected.emit(item.value);
        }
        return success;
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _removeItem(item, selectedItems) {
        /** @type {?} */
        let success = false;
        /** @type {?} */
        const value = item instanceof SelectItemDirective ? item.value : item;
        /** @type {?} */
        const index = selectedItems.indexOf(value);
        if (index > -1) {
            success = true;
            selectedItems.splice(index, 1);
            this._selectedItems$.next(selectedItems);
            this.itemDeselected.emit(item.value);
        }
        return success;
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _toggleItem(item) {
        if (item.selected) {
            this._deselectItem(item);
        }
        else {
            this._selectItem(item);
        }
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _selectItem(item) {
        this.updateItems$.next({ type: UpdateActions.Add, item });
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    _deselectItem(item) {
        this.updateItems$.next({ type: UpdateActions.Remove, item });
    }
    /**
     * @private
     * @param {?} item
     * @param {?} selectedItems
     * @return {?}
     */
    _hasItem(item, selectedItems) {
        return selectedItems.includes(item.value);
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _drawRipple(event) {
        /** @type {?} */
        const position = getMousePosition(event);
        /** @type {?} */
        const node = document.querySelector('.ripple');
        /** @type {?} */
        const newNode = (/** @type {?} */ (node.cloneNode(true)));
        newNode.classList.add('animate');
        newNode.style.left = position.x - 5 + 'px';
        newNode.style.top = position.y - 5 + 'px';
        node.parentNode.replaceChild(newNode, node);
    }
    ;
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _activateSelectingAfterLongTouch(event) {
        this._disabledByLongTouch = false;
        clearInterval(this._longTouchTimer);
        this._longTouchTimer = null;
        this.$selectBox.nativeElement.style.opacity = 1;
        if (!this.disableDrag) {
            this.renderer.addClass(document.body, NO_SELECT_CLASS);
            this.renderer.addClass(document.body, NO_SELECT_CLASS_MOBILE);
        }
        this._drawRipple(event);
    }
    /**
     * @private
     * @return {?}
     */
    _deactivateSelectingAfterLongTouch() {
        this._disabledByLongTouch = true;
        this.$selectBox.nativeElement.style.opacity = 0;
        document.body.style.overflow = 'auto';
    }
    /**
     * @private
     * @param {?} msg
     * @return {?}
     */
    log(msg) {
        document.getElementById('messages').style.whiteSpace = 'normal';
        document.getElementById('messages').innerHTML += ' ' + msg;
    }
}
SelectContainerComponent.decorators = [
    { type: Component, args: [{
                selector: 'dts-select-container',
                exportAs: 'dts-select-container',
                host: {
                    class: 'dts-select-container'
                },
                template: `
    <ng-content></ng-content>
    <div class="ripple" #ripple></div>
    <div
      class="dts-select-box"
      #selectBox
      [ngClass]="selectBoxClasses$ | async"
      [ngStyle]="selectBoxStyles$ | async"
    ></div>
  `,
                styles: [":host{display:block;position:relative}::ng-deep .dts-no-select dts-select-container{touch-action:none}::ng-deep body.dts-no-select-mobile{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}::ng-deep body.dts-no-select.dts-no-select-mobile{overflow:hidden!important}.ripple{width:20px;height:20px;opacity:0;-webkit-transform:scale(0);transform:scale(0);background:rgba(20,178,252,.5);border-radius:50%;position:fixed}.animate{-webkit-animation:1s cubic-bezier(0,0,.2,1) ripple-mo;animation:1s cubic-bezier(0,0,.2,1) ripple-mo}@-webkit-keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}@keyframes ripple-mo{0%{-webkit-transform:scale(0);transform:scale(0);opacity:1}100%{-webkit-transform:scale(10);transform:scale(10);opacity:0}}"]
            }] }
];
/** @nocollapse */
SelectContainerComponent.ctorParameters = () => [
    { type: ShortcutService },
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgZone }
];
SelectContainerComponent.propDecorators = {
    $selectBox: [{ type: ViewChild, args: ['selectBox', { static: true },] }],
    $ripple: [{ type: ViewChild, args: ['ripple', { static: true },] }],
    $selectableItems: [{ type: ContentChildren, args: [SelectItemDirective, { descendants: true },] }],
    selectedItems: [{ type: Input }],
    selectOnDrag: [{ type: Input }],
    disabled: [{ type: Input }],
    disableDrag: [{ type: Input }],
    selectMode: [{ type: Input }],
    selectWithShortcut: [{ type: Input }],
    longTouch: [{ type: Input }],
    longTouchSeconds: [{ type: Input }],
    isMobileDevice: [{ type: Input }],
    custom: [{ type: Input }, { type: HostBinding, args: ['class.dts-custom',] }],
    selectedItemsChange: [{ type: Output }],
    select: [{ type: Output }],
    itemSelected: [{ type: Output }],
    itemDeselected: [{ type: Output }],
    selectionStarted: [{ type: Output }],
    selectionEnded: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    SelectContainerComponent.prototype.host;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxStyles$;
    /** @type {?} */
    SelectContainerComponent.prototype.selectBoxClasses$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectBox;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$ripple;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.$selectableItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItems;
    /** @type {?} */
    SelectContainerComponent.prototype.selectOnDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.disabled;
    /** @type {?} */
    SelectContainerComponent.prototype.disableDrag;
    /** @type {?} */
    SelectContainerComponent.prototype.selectMode;
    /** @type {?} */
    SelectContainerComponent.prototype.selectWithShortcut;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouch;
    /** @type {?} */
    SelectContainerComponent.prototype.longTouchSeconds;
    /** @type {?} */
    SelectContainerComponent.prototype.isMobileDevice;
    /** @type {?} */
    SelectContainerComponent.prototype.custom;
    /** @type {?} */
    SelectContainerComponent.prototype.selectedItemsChange;
    /** @type {?} */
    SelectContainerComponent.prototype.select;
    /** @type {?} */
    SelectContainerComponent.prototype.itemSelected;
    /** @type {?} */
    SelectContainerComponent.prototype.itemDeselected;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionStarted;
    /** @type {?} */
    SelectContainerComponent.prototype.selectionEnded;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._tmpItems;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._selectedItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.updateItems$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.destroy$;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchTimer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._longTouchSeconds;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype._disabledByLongTouch;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.shortcuts;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.hostElementRef;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    SelectContainerComponent.prototype.ngZone;
    /* Skipping unhandled member: ;*/
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWNvbnRhaW5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZHJhZy10by1zZWxlY3QvIiwic291cmNlcyI6WyJsaWIvc2VsZWN0LWNvbnRhaW5lci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsVUFBVSxFQUNWLE1BQU0sRUFDTixZQUFZLEVBQ1osS0FBSyxFQUVMLFNBQVMsRUFDVCxTQUFTLEVBQ1QsTUFBTSxFQUNOLGVBQWUsRUFDZixTQUFTLEVBQ1QsV0FBVyxFQUlaLE1BQU0sZUFBZSxDQUFDO0FBSXZCLE9BQU8sRUFBYyxPQUFPLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFbkgsT0FBTyxFQUNMLFNBQVMsRUFDVCxTQUFTLEVBQ1QsR0FBRyxFQUNILEdBQUcsRUFDSCxNQUFNLEVBQ04sU0FBUyxFQUNULEtBQUssRUFDTCxLQUFLLEVBQ0wsY0FBYyxFQUNkLG9CQUFvQixFQUNwQixTQUFTLEVBQ1QsU0FBUyxFQUNULFdBQVcsRUFDWCxLQUFLLEVBQ04sTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFckQsT0FBTyxFQUFFLGVBQWUsRUFBRSxvQkFBb0IsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUV2RixPQUFPLEVBQ0wsTUFBTSxFQUtOLGFBQWEsRUFFZCxNQUFNLFVBQVUsQ0FBQztBQUVsQixPQUFPLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUVsRixPQUFPLEVBQ0wsYUFBYSxFQUNiLG1CQUFtQixFQUNuQixjQUFjLEVBQ2QsYUFBYSxFQUNiLDJCQUEyQixFQUMzQix3QkFBd0IsRUFDeEIsZ0JBQWdCLEVBQ2hCLGNBQWMsRUFDZixNQUFNLFNBQVMsQ0FBQztBQW9CakIsTUFBTSxPQUFPLHdCQUF3Qjs7Ozs7OztJQTZDbkMsWUFDVSxTQUEwQixFQUMxQixjQUEwQixFQUMxQixRQUFtQixFQUNuQixNQUFjO1FBSGQsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsbUJBQWMsR0FBZCxjQUFjLENBQVk7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBbENmLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQix1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDM0IsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixxQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDckIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFJaEMsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUVMLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsV0FBTSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDakMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN6QyxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDO1FBQzVDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQztRQUVsRCxjQUFTLEdBQUcsSUFBSSxHQUFHLEVBQStCLENBQUM7UUFFbkQsb0JBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBYSxFQUFFLENBQUMsQ0FBQztRQUN0RCxpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFnQixDQUFDO1FBQzNDLGFBQVEsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBSS9CLHlCQUFvQixHQUFHLEtBQUssQ0FBQztJQU9qQyxDQUFDOzs7O0lBRUwsZUFBZTtRQUNiLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUM7UUFFOUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7UUFFaEMsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7Ozs7Y0FLekIsUUFBUSxHQUFHLFNBQVMsQ0FBZ0IsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FDL0QsaUJBQWlCLEVBQUUsRUFDbkIsS0FBSyxFQUFFLENBQ1I7O2NBRUssTUFBTSxHQUFHLFNBQVMsQ0FBZ0IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDM0QsaUJBQWlCLEVBQUUsRUFDbkIsS0FBSyxFQUFFLENBQ1I7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTs7a0JBQ2xCLFFBQVEsR0FBRyxTQUFTLENBQWEsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FDNUQsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFDLEVBQzVCLEdBQUc7OztZQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQyxFQUM1QixLQUFLLEVBQUUsQ0FDUjs7a0JBRUssVUFBVSxHQUFHLFNBQVMsQ0FBYSxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUNoRSxNQUFNOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsRUFDNUIsS0FBSyxFQUFFLENBQ1I7O2tCQUVLLFVBQVUsR0FBRyxTQUFTLENBQWEsSUFBSSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQ25FLE1BQU07Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFDLEVBQUUsdUJBQXVCO1lBQzVELE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBQyxFQUM1QixHQUFHOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFDLEVBQ3RDLEtBQUssRUFBRSxDQUNSOztrQkFFSyxTQUFTLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FDL0IsTUFBTTs7OztZQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxFQUFDLEVBQ3hELE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBQyxFQUM5QixNQUFNOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUMsRUFDL0IsU0FBUzs7O1lBQUMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBQyxFQUNyRCxLQUFLLEVBQUUsQ0FDUjs7a0JBRUsscUJBQXFCLEdBQThCLFVBQVUsQ0FBQyxJQUFJLENBQ3RFLEdBQUc7Ozs7WUFBQyxDQUFDLEtBQWlCLEVBQUUsRUFBRSxDQUFDLHdCQUF3QixDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FDdkU7O2tCQUVLLEtBQUssR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7a0JBQ2hDLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs7a0JBQy9CLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztrQkFFM0QsVUFBVSxHQUFHLGFBQWEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLHFCQUFxQixDQUFDLENBQUMsSUFBSSxDQUMvRSxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUMxQixLQUFLLEVBQUUsQ0FDUjtZQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUN4RSxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFDMUIsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLEVBQUUsRUFBRTtnQkFDekIsT0FBTztvQkFDTCxZQUFZLEVBQUUsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQztvQkFDM0YsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDO2lCQUMxRCxDQUFDO1lBQ0osQ0FBQyxFQUFDLEVBQ0Ysb0JBQW9COzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQ3hFLENBQUM7O2tCQUVJLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQ3JDLE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQyxFQUNoQyxNQUFNOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUMsRUFDOUIsTUFBTTs7OztZQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxFQUFDLEVBQzlDLFNBQVM7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBQyxFQUN0QyxNQUFNOzs7O1lBQ0osS0FBSyxDQUFDLEVBQUUsQ0FDTixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEVBQzVDLENBQ0Y7O2tCQUVLLGFBQWEsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUNuQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLGNBQWMsQ0FBQyxVQUFVOzs7OztZQUFFLENBQUMsU0FBUyxFQUFFLEtBQWlCLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzVELFNBQVM7Z0JBQ1QsS0FBSzthQUNOLENBQUMsRUFBQyxFQUNILE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUMsRUFDL0IsTUFBTTs7OztZQUFDLENBQUMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxFQUFDLEVBQ3BELEdBQUc7Ozs7WUFBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBQyxDQUMxQjs7a0JBRUssc0JBQXNCLEdBQUcsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3pELFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFDckIsb0JBQW9CLENBQUMsVUFBVSxDQUFDLEVBQ2hDLEdBQUc7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDVixJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDeEI7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2lCQUNwQjtZQUNILENBQUMsRUFBQyxDQUNIO1lBRUQsS0FBSyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxzQkFBc0IsQ0FBQztpQkFDM0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzlCLFNBQVM7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQztZQUVoRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FDckMsR0FBRzs7OztZQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEIsR0FBRyxFQUFFLEdBQUcsU0FBUyxDQUFDLEdBQUcsSUFBSTtnQkFDekIsSUFBSSxFQUFFLEdBQUcsU0FBUyxDQUFDLElBQUksSUFBSTtnQkFDM0IsS0FBSyxFQUFFLEdBQUcsU0FBUyxDQUFDLEtBQUssSUFBSTtnQkFDN0IsTUFBTSxFQUFFLEdBQUcsU0FBUyxDQUFDLE1BQU0sSUFBSTtnQkFDL0IsT0FBTyxFQUFFLFNBQVMsQ0FBQyxPQUFPO2FBQzNCLENBQUMsRUFBQyxDQUNKLENBQUM7WUFFRixJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2xEO2FBQU07WUFDTCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7a0JBRXJDLFVBQVUsR0FBRyxTQUFTLENBQWEsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FDaEUsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFDLEVBQzVCLEdBQUc7Ozs7WUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNaLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO29CQUUxQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUVwQyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTt3QkFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO3FCQUNsRTtvQkFFRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDN0I7WUFDSCxDQUFDLEVBQUMsRUFDRixLQUFLLEVBQUUsQ0FDUjs7a0JBRUssV0FBVyxHQUFHLFNBQVMsQ0FBYSxJQUFJLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FDckUsTUFBTTs7OztZQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUMsRUFBRSx1QkFBdUI7WUFDNUQsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFDLEVBQy9FLEdBQUc7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDVixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLENBQUM7b0JBRTNCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFFdEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxXQUFXOzs7b0JBQUMsR0FBRyxFQUFFO3dCQUN0QyxJQUFJLENBQUMsaUJBQWlCLElBQUksR0FBRyxDQUFDO3dCQUU5QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUU1RCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQ3BELElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDOUM7b0JBQ0gsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUNUO2dCQUVELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDM0IsQ0FBQyxFQUFDLEVBQ0YsS0FBSyxFQUFFLENBQ1I7O2tCQUVLLFNBQVMsR0FBRyxTQUFTLENBQWEsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FDOUQsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFDLEVBQzVCLEdBQUc7OztZQUFDLEdBQUcsRUFBRTtnQkFDUCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTt3QkFDeEIsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7cUJBQzdCO29CQUVELElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDO2lCQUMzQztnQkFFRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDcEIsQ0FBQyxFQUFDLEVBQ0YsS0FBSyxFQUFFLENBQ1I7O2tCQUVLLFNBQVMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUNoQyxNQUFNOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEVBQUMsRUFDeEQsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFDLEVBQzlCLE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBQyxFQUMvQixTQUFTOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLEVBQ3RELEtBQUssRUFBRSxDQUNSOztrQkFFSyxxQkFBcUIsR0FBOEIsV0FBVyxDQUFDLElBQUksQ0FDdkUsR0FBRzs7OztZQUFDLENBQUMsS0FBaUIsRUFBRSxFQUFFLENBQUMsd0JBQXdCLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUN2RTs7a0JBRUssS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOztrQkFDaEMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDOztrQkFDaEMsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7O2tCQUUzRCxVQUFVLEdBQUcsYUFBYSxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQy9FLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSTs7O1lBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFDLEVBQzNELEtBQUssRUFBRSxDQUNSO1lBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQ3pFLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFDckIsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUMxQixHQUFHOzs7O1lBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsRUFBRSxFQUFFO2dCQUN6QixPQUFPO29CQUNMLFlBQVksRUFBRSxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDO29CQUMzRixjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7aUJBQzFELENBQUM7WUFDSixDQUFDLEVBQUMsRUFDRixvQkFBb0I7Ozs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FDeEUsQ0FBQzs7a0JBRUksZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FDckMsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDLEVBQ2hDLE1BQU07OztZQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBQyxFQUM5QixNQUFNOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEVBQUMsRUFDOUMsU0FBUzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFDLEVBQ3ZDLE1BQU07Ozs7WUFDSixLQUFLLENBQUMsRUFBRSxDQUNOLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsRUFDNUMsQ0FDRjs7a0JBRUssYUFBYSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQ25DLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFDckIsY0FBYyxDQUFDLFVBQVU7Ozs7O1lBQUUsQ0FBQyxTQUFTLEVBQUUsS0FBOEIsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDekUsU0FBUztnQkFDVCxLQUFLO2FBQ04sQ0FBQyxFQUFDLEVBQ0gsTUFBTTs7O1lBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQyxFQUMvQixNQUFNOzs7O1lBQUMsQ0FBQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEVBQUMsRUFDcEQsR0FBRzs7OztZQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFDLENBQzFCOztrQkFFSyxzQkFBc0IsR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDekQsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFDaEMsR0FBRzs7OztZQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNWLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUN4QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3BCO1lBQ0gsQ0FBQyxFQUFDLENBQ0g7WUFFRCxLQUFLLENBQUMsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLHNCQUFzQixDQUFDO2lCQUMzRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDOUIsU0FBUzs7OztZQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUFDO1lBRWhELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUNyQyxHQUFHOzs7O1lBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQixHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUMsR0FBRyxJQUFJO2dCQUN6QixJQUFJLEVBQUUsR0FBRyxTQUFTLENBQUMsSUFBSSxJQUFJO2dCQUMzQixLQUFLLEVBQUUsR0FBRyxTQUFTLENBQUMsS0FBSyxJQUFJO2dCQUM3QixNQUFNLEVBQUUsR0FBRyxTQUFTLENBQUMsTUFBTSxJQUFJO2dCQUMvQixPQUFPLEVBQUUsU0FBUyxDQUFDLE9BQU87YUFDM0IsQ0FBQyxFQUFDLENBQ0osQ0FBQztZQUVGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7U0FDcEQ7SUFDSCxDQUFDOzs7O0lBRUQsU0FBUztRQUNQLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBSSxTQUF5QjtRQUN0QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsSUFBeUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUFDO0lBQzFHLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBSSxTQUF5QjtRQUN0QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsSUFBeUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUFDO0lBQzFHLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBSSxTQUF5QjtRQUN4QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsSUFBeUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUFDO0lBQzVHLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU87Ozs7UUFBQyxJQUFJLENBQUMsRUFBRTtZQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTzs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLEVBQUMsQ0FBQztJQUM1RSxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7Ozs7O0lBRU8sc0JBQXNCLENBQUksU0FBeUI7UUFDekQsOERBQThEO1FBQzlELGlFQUFpRTtRQUNqRSxtQkFBbUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzNGLENBQUM7Ozs7O0lBRU8sd0JBQXdCO1FBQzlCLElBQUksQ0FBQyxlQUFlO2FBQ2pCLElBQUksQ0FDSCxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQ3JCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2FBQ0EsU0FBUyxDQUFDO1lBQ1QsSUFBSTs7OztZQUFFLGFBQWEsQ0FBQyxFQUFFO2dCQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNsQyxDQUFDLENBQUE7WUFDRCxRQUFROzs7WUFBRSxHQUFHLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxDQUFDLENBQUE7U0FDRixDQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVPLHVCQUF1QjtRQUM3QiwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFlBQVk7YUFDZCxJQUFJLENBQ0gsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFDcEMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FDekI7YUFDQSxTQUFTOzs7O1FBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQXdCLEVBQUUsRUFBRTs7a0JBQ3RELElBQUksR0FBRyxNQUFNLENBQUMsSUFBSTtZQUV4QixRQUFRLE1BQU0sQ0FBQyxJQUFJLEVBQUU7Z0JBQ25CLEtBQUssYUFBYSxDQUFDLEdBQUc7b0JBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEVBQUU7d0JBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFDaEI7b0JBQ0QsTUFBTTtnQkFDUixLQUFLLGFBQWEsQ0FBQyxNQUFNO29CQUN2QixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxFQUFFO3dCQUN6QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7cUJBQ2xCO29CQUNELE1BQU07YUFDVDtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUwsK0VBQStFO1FBQy9FLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPO2FBQzFCLElBQUksQ0FDSCxjQUFjLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUNwQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQ3pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2FBQ0EsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUEwQyxFQUFFLEVBQUU7O2tCQUN2RSxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sRUFBRTs7a0JBQ3pCLFlBQVksR0FBRyxhQUFhLENBQUMsTUFBTTs7OztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBQztZQUVoRixJQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3ZCLFlBQVksQ0FBQyxPQUFPOzs7O2dCQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLEVBQUMsQ0FBQzthQUNyRTtZQUVELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoQixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRU8sMkJBQTJCO1FBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCOzs7UUFBQyxHQUFHLEVBQUU7O2tCQUMzQixPQUFPLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7O2tCQUNyQyxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7O2tCQUMzQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUM7WUFFdkQsS0FBSyxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsZ0JBQWdCLENBQUM7aUJBQzVDLElBQUksQ0FDSCxTQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFDM0IsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUNyQixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUN6QjtpQkFDQSxTQUFTOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2hCLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7O0lBRU8scUJBQXFCLENBQzNCLFVBQStDLEVBQy9DLFFBQTZDO1FBRTdDLFVBQVU7YUFDUCxJQUFJLENBQ0gsTUFBTTs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxFQUFDLEVBQzlDLEdBQUc7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsRUFBQyxFQUN2QyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQ25DLGNBQWMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQ3BDLEdBQUc7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFDLEVBQ3pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQ3pCO2FBQ0EsU0FBUzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTyw0QkFBNEI7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsR0FBRywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEUsQ0FBQzs7Ozs7O0lBRU8saUJBQWlCLENBQUMsS0FBOEI7UUFDdEQsT0FBTyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7O0lBRU8sVUFBVTtRQUNoQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLHNCQUFzQixDQUFDLENBQUM7SUFDbkUsQ0FBQzs7Ozs7O0lBRU8sWUFBWSxDQUFDLEtBQThCO1FBQ2pELElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzNELE9BQU87U0FDUjtRQUVELGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV2QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO1lBQ3hFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7U0FDeEQ7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztTQUMvRDs7Y0FFSyxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBRTFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFOztrQkFDdEMsUUFBUSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsRUFBRTs7a0JBQ3ZDLGlCQUFpQixHQUFHLGFBQWEsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDO1lBRTdELElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbkQsT0FBTzthQUNSOztrQkFFSyxTQUFTLEdBQ2IsQ0FBQyxpQkFBaUI7Z0JBQ2hCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7Z0JBQ3ZDLENBQUMsSUFBSSxDQUFDLFVBQVU7Z0JBQ2hCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDO2dCQUMzQixDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUMvRSxDQUFDLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUMvRSxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN4RCxDQUFDLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDOztrQkFFcEQsWUFBWSxHQUNoQixDQUFDLENBQUMsaUJBQWlCO2dCQUNqQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO2dCQUN2QyxDQUFDLElBQUksQ0FBQyxVQUFVO2dCQUNoQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztnQkFDM0IsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNoRixDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDOUUsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN6RCxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUV6RCxJQUFJLFNBQVMsRUFBRTtnQkFDYixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hCO2lCQUFNLElBQUksWUFBWSxFQUFFO2dCQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxZQUFZLENBQUMsS0FBWTs7Y0FDekIsWUFBWSxHQUFHLDJCQUEyQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBRS9FLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ3hEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ3REO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxvQkFBb0IsQ0FBQyxLQUFZO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzlFLENBQUM7Ozs7Ozs7O0lBRU8sb0JBQW9CLENBQUMsU0FBUyxFQUFFLElBQXlCLEVBQUUsS0FBWTs7Y0FDdkUsV0FBVyxHQUFHLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7O2NBRXBFLFNBQVMsR0FBRyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7O2NBRXZGLFlBQVksR0FDaEIsQ0FBQyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEUsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTdFLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QjthQUFNLElBQUksWUFBWSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7Ozs7OztJQUVPLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxJQUF5QixFQUFFLEtBQVk7O2NBQ3pFLFdBQVcsR0FBRyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDOztjQUVwRSxTQUFTLEdBQ2IsQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFHLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDOztjQUVwRyxZQUFZLEdBQ2hCLENBQUMsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRyxDQUFDLENBQUMsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTNHLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7O2tCQUU1QyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7Z0JBQ3RELENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTTtnQkFDZixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO29CQUNwQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUc7b0JBQ1osQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJO1lBRWpCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksWUFBWSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzlFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdCO0lBQ0gsQ0FBQzs7Ozs7SUFFTyxXQUFXO1FBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTzs7Ozs7UUFBQyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRTtZQUN0QyxJQUFJLE1BQU0sS0FBSyxNQUFNLENBQUMsR0FBRyxFQUFFO2dCQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hCO1lBRUQsSUFBSSxNQUFNLEtBQUssTUFBTSxDQUFDLE1BQU0sRUFBRTtnQkFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7Ozs7O0lBRU8sUUFBUSxDQUFDLElBQXlCLEVBQUUsYUFBeUI7O1lBQy9ELE9BQU8sR0FBRyxLQUFLO1FBRW5CLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsRUFBRTtZQUNyRSxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ2YsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BDO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQzs7Ozs7OztJQUVPLFdBQVcsQ0FBQyxJQUF5QixFQUFFLGFBQXlCOztZQUNsRSxPQUFPLEdBQUcsS0FBSzs7Y0FDYixLQUFLLEdBQUcsSUFBSSxZQUFZLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJOztjQUMvRCxLQUFLLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFFMUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDZCxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ2YsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RDO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQXlCO1FBQzNDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzFCO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQXlCO1FBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLGFBQWEsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM1RCxDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsSUFBeUI7UUFDN0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsYUFBYSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7Ozs7SUFFTyxRQUFRLENBQUMsSUFBeUIsRUFBRSxhQUF5QjtRQUNuRSxPQUFPLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxLQUFpQjs7Y0FDN0IsUUFBUSxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQzs7Y0FFbEMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDOztjQUV4QyxPQUFPLEdBQUcsbUJBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBZTtRQUNuRCxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNqQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDM0MsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBRTFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQUEsQ0FBQzs7Ozs7O0lBRU0sZ0NBQWdDLENBQUMsS0FBaUI7UUFDeEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztRQUNsQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1NBQy9EO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVPLGtDQUFrQztRQUN4QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7SUFDeEMsQ0FBQzs7Ozs7O0lBRU8sR0FBRyxDQUFDLEdBQVc7UUFDckIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztRQUNoRSxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDO0lBQzdELENBQUM7OztZQS9yQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLElBQUksRUFBRTtvQkFDSixLQUFLLEVBQUUsc0JBQXNCO2lCQUM5QjtnQkFDRCxRQUFRLEVBQUU7Ozs7Ozs7OztHQVNUOzthQUVGOzs7O1lBNUNRLGVBQWU7WUF0Q3RCLFVBQVU7WUFLVixTQUFTO1lBRVQsTUFBTTs7O3lCQWlGTCxTQUFTLFNBQUMsV0FBVyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtzQkFHdkMsU0FBUyxTQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7K0JBR3BDLGVBQWUsU0FBQyxtQkFBbUIsRUFBRSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7NEJBRzFELEtBQUs7MkJBQ0wsS0FBSzt1QkFDTCxLQUFLOzBCQUNMLEtBQUs7eUJBQ0wsS0FBSztpQ0FDTCxLQUFLO3dCQUNMLEtBQUs7K0JBQ0wsS0FBSzs2QkFDTCxLQUFLO3FCQUVMLEtBQUssWUFDTCxXQUFXLFNBQUMsa0JBQWtCO2tDQUc5QixNQUFNO3FCQUNOLE1BQU07MkJBQ04sTUFBTTs2QkFDTixNQUFNOytCQUNOLE1BQU07NkJBQ04sTUFBTTs7OztJQWhDUCx3Q0FBMEI7O0lBQzFCLG9EQUFnRDs7SUFDaEQscURBQTBEOzs7OztJQUUxRCw4Q0FDK0I7Ozs7O0lBRS9CLDJDQUM0Qjs7Ozs7SUFFNUIsb0RBQ3lEOztJQUV6RCxpREFBNEI7O0lBQzVCLGdEQUE2Qjs7SUFDN0IsNENBQTBCOztJQUMxQiwrQ0FBNkI7O0lBQzdCLDhDQUE0Qjs7SUFDNUIsc0RBQW9DOztJQUNwQyw2Q0FBMkI7O0lBQzNCLG9EQUE4Qjs7SUFDOUIsa0RBQWdDOztJQUVoQywwQ0FFZTs7SUFFZix1REFBd0Q7O0lBQ3hELDBDQUEyQzs7SUFDM0MsZ0RBQWlEOztJQUNqRCxrREFBbUQ7O0lBQ25ELG9EQUFzRDs7SUFDdEQsa0RBQTBEOzs7OztJQUUxRCw2Q0FBMkQ7Ozs7O0lBRTNELG1EQUE4RDs7Ozs7SUFDOUQsZ0RBQW1EOzs7OztJQUNuRCw0Q0FBdUM7Ozs7O0lBRXZDLG1EQUE2Qjs7Ozs7SUFDN0IscURBQWtDOzs7OztJQUNsQyx3REFBcUM7Ozs7O0lBR25DLDZDQUFrQzs7Ozs7SUFDbEMsa0RBQWtDOzs7OztJQUNsQyw0Q0FBMkI7Ozs7O0lBQzNCLDBDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgT3V0cHV0LFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBJbnB1dCxcclxuICBPbkRlc3Ryb3ksXHJcbiAgUmVuZGVyZXIyLFxyXG4gIFZpZXdDaGlsZCxcclxuICBOZ1pvbmUsXHJcbiAgQ29udGVudENoaWxkcmVuLFxyXG4gIFF1ZXJ5TGlzdCxcclxuICBIb3N0QmluZGluZyxcclxuICBBZnRlclZpZXdJbml0LFxyXG4gIFBMQVRGT1JNX0lELFxyXG4gIEluamVjdFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgaXNQbGF0Zm9ybUJyb3dzZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCwgY29tYmluZUxhdGVzdCwgbWVyZ2UsIGZyb20sIGZyb21FdmVudCwgQmVoYXZpb3JTdWJqZWN0LCBhc3luY1NjaGVkdWxlciB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHtcclxuICBzd2l0Y2hNYXAsXHJcbiAgdGFrZVVudGlsLFxyXG4gIG1hcCxcclxuICB0YXAsXHJcbiAgZmlsdGVyLFxyXG4gIGF1ZGl0VGltZSxcclxuICBtYXBUbyxcclxuICBzaGFyZSxcclxuICB3aXRoTGF0ZXN0RnJvbSxcclxuICBkaXN0aW5jdFVudGlsQ2hhbmdlZCxcclxuICBvYnNlcnZlT24sXHJcbiAgc3RhcnRXaXRoLFxyXG4gIGNvbmNhdE1hcFRvLFxyXG4gIGZpcnN0XHJcbn0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgU2VsZWN0SXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vc2VsZWN0LWl0ZW0uZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU2hvcnRjdXRTZXJ2aWNlIH0gZnJvbSAnLi9zaG9ydGN1dC5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IGNyZWF0ZVNlbGVjdEJveCwgd2hlblNlbGVjdEJveFZpc2libGUsIGRpc3RpbmN0S2V5RXZlbnRzIH0gZnJvbSAnLi9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHtcclxuICBBY3Rpb24sXHJcbiAgU2VsZWN0Qm94LFxyXG4gIE1vdXNlUG9zaXRpb24sXHJcbiAgU2VsZWN0Q29udGFpbmVySG9zdCxcclxuICBVcGRhdGVBY3Rpb24sXHJcbiAgVXBkYXRlQWN0aW9ucyxcclxuICBQcmVkaWNhdGVGblxyXG59IGZyb20gJy4vbW9kZWxzJztcclxuXHJcbmltcG9ydCB7IEFVRElUX1RJTUUsIE5PX1NFTEVDVF9DTEFTUywgTk9fU0VMRUNUX0NMQVNTX01PQklMRSB9IGZyb20gJy4vY29uc3RhbnRzJztcclxuXHJcbmltcG9ydCB7XHJcbiAgaW5Cb3VuZGluZ0JveCxcclxuICBjdXJzb3JXaXRoaW5FbGVtZW50LFxyXG4gIGNsZWFyU2VsZWN0aW9uLFxyXG4gIGJveEludGVyc2VjdHMsXHJcbiAgY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0LFxyXG4gIGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbixcclxuICBnZXRNb3VzZVBvc2l0aW9uLFxyXG4gIGhhc01pbmltdW1TaXplXHJcbn0gZnJvbSAnLi91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2R0cy1zZWxlY3QtY29udGFpbmVyJyxcclxuICBleHBvcnRBczogJ2R0cy1zZWxlY3QtY29udGFpbmVyJyxcclxuICBob3N0OiB7XHJcbiAgICBjbGFzczogJ2R0cy1zZWxlY3QtY29udGFpbmVyJ1xyXG4gIH0sXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgIDxkaXYgY2xhc3M9XCJyaXBwbGVcIiAjcmlwcGxlPjwvZGl2PlxyXG4gICAgPGRpdlxyXG4gICAgICBjbGFzcz1cImR0cy1zZWxlY3QtYm94XCJcclxuICAgICAgI3NlbGVjdEJveFxyXG4gICAgICBbbmdDbGFzc109XCJzZWxlY3RCb3hDbGFzc2VzJCB8IGFzeW5jXCJcclxuICAgICAgW25nU3R5bGVdPVwic2VsZWN0Qm94U3R5bGVzJCB8IGFzeW5jXCJcclxuICAgID48L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlVXJsczogWycuL3NlbGVjdC1jb250YWluZXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VsZWN0Q29udGFpbmVyQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcclxuICBob3N0OiBTZWxlY3RDb250YWluZXJIb3N0O1xyXG4gIHNlbGVjdEJveFN0eWxlcyQ6IE9ic2VydmFibGU8U2VsZWN0Qm94PHN0cmluZz4+O1xyXG4gIHNlbGVjdEJveENsYXNzZXMkOiBPYnNlcnZhYmxlPHsgW2tleTogc3RyaW5nXTogYm9vbGVhbiB9PjtcclxuXHJcbiAgQFZpZXdDaGlsZCgnc2VsZWN0Qm94JywgeyBzdGF0aWM6IHRydWUgfSlcclxuICBwcml2YXRlICRzZWxlY3RCb3g6IEVsZW1lbnRSZWY7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3JpcHBsZScsIHsgc3RhdGljOiB0cnVlIH0pXHJcbiAgcHJpdmF0ZSAkcmlwcGxlOiBFbGVtZW50UmVmO1xyXG5cclxuICBAQ29udGVudENoaWxkcmVuKFNlbGVjdEl0ZW1EaXJlY3RpdmUsIHsgZGVzY2VuZGFudHM6IHRydWUgfSlcclxuICBwcml2YXRlICRzZWxlY3RhYmxlSXRlbXM6IFF1ZXJ5TGlzdDxTZWxlY3RJdGVtRGlyZWN0aXZlPjtcclxuXHJcbiAgQElucHV0KCkgc2VsZWN0ZWRJdGVtczogYW55O1xyXG4gIEBJbnB1dCgpIHNlbGVjdE9uRHJhZyA9IHRydWU7XHJcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBkaXNhYmxlRHJhZyA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHNlbGVjdE1vZGUgPSBmYWxzZTtcclxuICBASW5wdXQoKSBzZWxlY3RXaXRoU2hvcnRjdXQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBsb25nVG91Y2ggPSBmYWxzZTtcclxuICBASW5wdXQoKSBsb25nVG91Y2hTZWNvbmRzID0gMTtcclxuICBASW5wdXQoKSBpc01vYmlsZURldmljZSA9IGZhbHNlO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIEBIb3N0QmluZGluZygnY2xhc3MuZHRzLWN1c3RvbScpXHJcbiAgY3VzdG9tID0gZmFsc2U7XHJcblxyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZEl0ZW1zQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHNlbGVjdCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBpdGVtU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgaXRlbURlc2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgc2VsZWN0aW9uU3RhcnRlZCA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuICBAT3V0cHV0KCkgc2VsZWN0aW9uRW5kZWQgPSBuZXcgRXZlbnRFbWl0dGVyPEFycmF5PGFueT4+KCk7XHJcblxyXG4gIHByaXZhdGUgX3RtcEl0ZW1zID0gbmV3IE1hcDxTZWxlY3RJdGVtRGlyZWN0aXZlLCBBY3Rpb24+KCk7XHJcblxyXG4gIHByaXZhdGUgX3NlbGVjdGVkSXRlbXMkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxBcnJheTxhbnk+PihbXSk7XHJcbiAgcHJpdmF0ZSB1cGRhdGVJdGVtcyQgPSBuZXcgU3ViamVjdDxVcGRhdGVBY3Rpb24+KCk7XHJcbiAgcHJpdmF0ZSBkZXN0cm95JCA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcblxyXG4gIHByaXZhdGUgX2xvbmdUb3VjaFRpbWVyOiBhbnk7XHJcbiAgcHJpdmF0ZSBfbG9uZ1RvdWNoU2Vjb25kczogbnVtYmVyO1xyXG4gIHByaXZhdGUgX2Rpc2FibGVkQnlMb25nVG91Y2ggPSBmYWxzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHNob3J0Y3V0czogU2hvcnRjdXRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBob3N0RWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgIHByaXZhdGUgbmdab25lOiBOZ1pvbmVcclxuICApIHsgfVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICB0aGlzLmhvc3QgPSB0aGlzLmhvc3RFbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcblxyXG4gICAgdGhpcy5faW5pdFNlbGVjdGVkSXRlbXNDaGFuZ2UoKTtcclxuXHJcbiAgICB0aGlzLl9jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIHRoaXMuX29ic2VydmVCb3VuZGluZ1JlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLl9vYnNlcnZlU2VsZWN0YWJsZUl0ZW1zKCk7XHJcblxyXG4gICAgLy8gZGlzdGluY3RLZXlFdmVudHMgaXMgdXNlZCB0byBwcmV2ZW50IG11bHRpcGxlIGtleSBldmVudHMgdG8gYmUgZmlyZWQgcmVwZWF0ZWRseVxyXG4gICAgLy8gb24gV2luZG93cyB3aGVuIGEga2V5IGlzIGJlaW5nIHByZXNzZWRcclxuXHJcbiAgICBjb25zdCBrZXlkb3duJCA9IGZyb21FdmVudDxLZXlib2FyZEV2ZW50Pih3aW5kb3csICdrZXlkb3duJykucGlwZShcclxuICAgICAgZGlzdGluY3RLZXlFdmVudHMoKSxcclxuICAgICAgc2hhcmUoKVxyXG4gICAgKTtcclxuXHJcbiAgICBjb25zdCBrZXl1cCQgPSBmcm9tRXZlbnQ8S2V5Ym9hcmRFdmVudD4od2luZG93LCAna2V5dXAnKS5waXBlKFxyXG4gICAgICBkaXN0aW5jdEtleUV2ZW50cygpLFxyXG4gICAgICBzaGFyZSgpXHJcbiAgICApO1xyXG5cclxuICAgIGlmICghdGhpcy5pc01vYmlsZURldmljZSkge1xyXG4gICAgICBjb25zdCBtb3VzZXVwJCA9IGZyb21FdmVudDxNb3VzZUV2ZW50Pih3aW5kb3csICdtb3VzZXVwJykucGlwZShcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZWQpLFxyXG4gICAgICAgIHRhcCgoKSA9PiB0aGlzLl9vbk1vdXNlVXAoKSksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgbW91c2Vtb3ZlJCA9IGZyb21FdmVudDxNb3VzZUV2ZW50Pih3aW5kb3csICdtb3VzZW1vdmUnKS5waXBlKFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlZCksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgbW91c2Vkb3duJCA9IGZyb21FdmVudDxNb3VzZUV2ZW50Pih0aGlzLmhvc3QsICdtb3VzZWRvd24nKS5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiBldmVudC5idXR0b24gPT09IDApLCAvLyBvbmx5IGVtaXQgbGVmdCBtb3VzZVxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5kaXNhYmxlZCksXHJcbiAgICAgICAgdGFwKGV2ZW50ID0+IHRoaXMuX29uTW91c2VEb3duKGV2ZW50KSksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgZHJhZ2dpbmckID0gbW91c2Vkb3duJC5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiAhdGhpcy5zaG9ydGN1dHMuZGlzYWJsZVNlbGVjdGlvbihldmVudCkpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5zZWxlY3RNb2RlKSxcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZURyYWcpLFxyXG4gICAgICAgIHN3aXRjaE1hcCgoKSA9PiBtb3VzZW1vdmUkLnBpcGUodGFrZVVudGlsKG1vdXNldXAkKSkpLFxyXG4gICAgICAgIHNoYXJlKClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IGN1cnJlbnRNb3VzZVBvc2l0aW9uJDogT2JzZXJ2YWJsZTxNb3VzZVBvc2l0aW9uPiA9IG1vdXNlZG93biQucGlwZShcclxuICAgICAgICBtYXAoKGV2ZW50OiBNb3VzZUV2ZW50KSA9PiBnZXRSZWxhdGl2ZU1vdXNlUG9zaXRpb24oZXZlbnQsIHRoaXMuaG9zdCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBzaG93JCA9IGRyYWdnaW5nJC5waXBlKG1hcFRvKDEpKTtcclxuICAgICAgY29uc3QgaGlkZSQgPSBtb3VzZXVwJC5waXBlKG1hcFRvKDApKTtcclxuICAgICAgY29uc3Qgb3BhY2l0eSQgPSBtZXJnZShzaG93JCwgaGlkZSQpLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RCb3gkID0gY29tYmluZUxhdGVzdChkcmFnZ2luZyQsIG9wYWNpdHkkLCBjdXJyZW50TW91c2VQb3NpdGlvbiQpLnBpcGUoXHJcbiAgICAgICAgY3JlYXRlU2VsZWN0Qm94KHRoaXMuaG9zdCksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgdGhpcy5zZWxlY3RCb3hDbGFzc2VzJCA9IG1lcmdlKGRyYWdnaW5nJCwgbW91c2V1cCQsIGtleWRvd24kLCBrZXl1cCQpLnBpcGUoXHJcbiAgICAgICAgYXVkaXRUaW1lKEFVRElUX1RJTUUpLFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHNlbGVjdEJveCQpLFxyXG4gICAgICAgIG1hcCgoW2V2ZW50LCBzZWxlY3RCb3hdKSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAnZHRzLWFkZGluZyc6IGhhc01pbmltdW1TaXplKHNlbGVjdEJveCwgMCwgMCkgJiYgIXRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpLFxyXG4gICAgICAgICAgICAnZHRzLXJlbW92aW5nJzogdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudClcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgZGlzdGluY3RVbnRpbENoYW5nZWQoKGEsIGIpID0+IEpTT04uc3RyaW5naWZ5KGEpID09PSBKU09OLnN0cmluZ2lmeShiKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uTW91c2VVcCQgPSBkcmFnZ2luZyQucGlwZShcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuc2VsZWN0T25EcmFnKSxcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuc2VsZWN0TW9kZSksXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IHRoaXMuX2N1cnNvcldpdGhpbkhvc3QoZXZlbnQpKSxcclxuICAgICAgICBzd2l0Y2hNYXAoXyA9PiBtb3VzZXVwJC5waXBlKGZpcnN0KCkpKSxcclxuICAgICAgICBmaWx0ZXIoXHJcbiAgICAgICAgICBldmVudCA9PlxyXG4gICAgICAgICAgICAoIXRoaXMuc2hvcnRjdXRzLmRpc2FibGVTZWxlY3Rpb24oZXZlbnQpICYmICF0aGlzLnNob3J0Y3V0cy50b2dnbGVTaW5nbGVJdGVtKGV2ZW50KSkgfHxcclxuICAgICAgICAgICAgdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudClcclxuICAgICAgICApXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RPbkRyYWckID0gc2VsZWN0Qm94JC5waXBlKFxyXG4gICAgICAgIGF1ZGl0VGltZShBVURJVF9USU1FKSxcclxuICAgICAgICB3aXRoTGF0ZXN0RnJvbShtb3VzZW1vdmUkLCAoc2VsZWN0Qm94LCBldmVudDogTW91c2VFdmVudCkgPT4gKHtcclxuICAgICAgICAgIHNlbGVjdEJveCxcclxuICAgICAgICAgIGV2ZW50XHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiB0aGlzLnNlbGVjdE9uRHJhZyksXHJcbiAgICAgICAgZmlsdGVyKCh7IHNlbGVjdEJveCB9KSA9PiBoYXNNaW5pbXVtU2l6ZShzZWxlY3RCb3gpKSxcclxuICAgICAgICBtYXAoKHsgZXZlbnQgfSkgPT4gZXZlbnQpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RPbktleWJvYXJkRXZlbnQkID0gbWVyZ2Uoa2V5ZG93biQsIGtleXVwJCkucGlwZShcclxuICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgd2hlblNlbGVjdEJveFZpc2libGUoc2VsZWN0Qm94JCksXHJcbiAgICAgICAgdGFwKGV2ZW50ID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLl9pc0V4dGVuZGVkU2VsZWN0aW9uKGV2ZW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLl90bXBJdGVtcy5jbGVhcigpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fZmx1c2hJdGVtcygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBtZXJnZShzZWxlY3RPbk1vdXNlVXAkLCBzZWxlY3RPbkRyYWckLCBzZWxlY3RPbktleWJvYXJkRXZlbnQkKVxyXG4gICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKSlcclxuICAgICAgICAuc3Vic2NyaWJlKGV2ZW50ID0+IHRoaXMuX3NlbGVjdEl0ZW1zKGV2ZW50KSk7XHJcblxyXG4gICAgICB0aGlzLnNlbGVjdEJveFN0eWxlcyQgPSBzZWxlY3RCb3gkLnBpcGUoXHJcbiAgICAgICAgbWFwKHNlbGVjdEJveCA9PiAoe1xyXG4gICAgICAgICAgdG9wOiBgJHtzZWxlY3RCb3gudG9wfXB4YCxcclxuICAgICAgICAgIGxlZnQ6IGAke3NlbGVjdEJveC5sZWZ0fXB4YCxcclxuICAgICAgICAgIHdpZHRoOiBgJHtzZWxlY3RCb3gud2lkdGh9cHhgLFxyXG4gICAgICAgICAgaGVpZ2h0OiBgJHtzZWxlY3RCb3guaGVpZ2h0fXB4YCxcclxuICAgICAgICAgIG9wYWNpdHk6IHNlbGVjdEJveC5vcGFjaXR5XHJcbiAgICAgICAgfSkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLl9pbml0U2VsZWN0aW9uT3V0cHV0cyhtb3VzZWRvd24kLCBtb3VzZXVwJCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLl9kaXNhYmxlZEJ5TG9uZ1RvdWNoID0gdGhpcy5sb25nVG91Y2g7XHJcblxyXG4gICAgICBjb25zdCB0b3VjaG1vdmUkID0gZnJvbUV2ZW50PFRvdWNoRXZlbnQ+KHdpbmRvdywgJ3RvdWNobW92ZScpLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLmRpc2FibGVkKSxcclxuICAgICAgICB0YXAoKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICBpZiAodGhpcy5sb25nVG91Y2ggJiYgdGhpcy5fbG9uZ1RvdWNoVGltZXIpIHtcclxuXHJcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5fbG9uZ1RvdWNoVGltZXIpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2gpIHtcclxuICAgICAgICAgICAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUNsYXNzKGRvY3VtZW50LmJvZHksIE5PX1NFTEVDVF9DTEFTU19NT0JJTEUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9sb25nVG91Y2hUaW1lciA9IG51bGw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgdG91Y2hzdGFydCQgPSBmcm9tRXZlbnQ8VG91Y2hFdmVudD4odGhpcy5ob3N0LCAndG91Y2hzdGFydCcpLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50LmRldGFpbCA9PT0gMCksIC8vIG9ubHkgZW1pdCBsZWZ0IG1vdXNlXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLmRpc2FibGVkICYmICghdGhpcy5pc01vYmlsZURldmljZSB8fCAhdGhpcy5fbG9uZ1RvdWNoVGltZXIpKSxcclxuICAgICAgICB0YXAoZXZlbnQgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMubG9uZ1RvdWNoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvbmdUb3VjaFNlY29uZHMgPSAwO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5jbGVhclNlbGVjdGlvbigpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fbG9uZ1RvdWNoVGltZXIgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5fbG9uZ1RvdWNoU2Vjb25kcyArPSAwLjE7XHJcblxyXG4gICAgICAgICAgICAgIHRoaXMuX2xvbmdUb3VjaFNlY29uZHMgPSArdGhpcy5fbG9uZ1RvdWNoU2Vjb25kcy50b0ZpeGVkKDIpO1xyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIGlmICh0aGlzLl9sb25nVG91Y2hTZWNvbmRzID09PSB0aGlzLmxvbmdUb3VjaFNlY29uZHMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2FjdGl2YXRlU2VsZWN0aW5nQWZ0ZXJMb25nVG91Y2goZXZlbnQpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgMTAwKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLl9vbk1vdXNlRG93bihldmVudCk7XHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgY29uc3QgdG91Y2hlbmQkID0gZnJvbUV2ZW50PFRvdWNoRXZlbnQ+KHdpbmRvdywgJ3RvdWNoZW5kJykucGlwZShcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZWQpLFxyXG4gICAgICAgIHRhcCgoKSA9PiB7XHJcbiAgICAgICAgICBpZiAodGhpcy5sb25nVG91Y2gpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2xvbmdUb3VjaFRpbWVyKSB7XHJcbiAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLl9sb25nVG91Y2hUaW1lcik7XHJcbiAgICAgICAgICAgICAgdGhpcy5fbG9uZ1RvdWNoVGltZXIgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9kZWFjdGl2YXRlU2VsZWN0aW5nQWZ0ZXJMb25nVG91Y2goKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLl9vbk1vdXNlVXAoKTtcclxuICAgICAgICB9KSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBkcmFnZ2luZyQgPSB0b3VjaHN0YXJ0JC5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiAhdGhpcy5zaG9ydGN1dHMuZGlzYWJsZVNlbGVjdGlvbihldmVudCkpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiAhdGhpcy5zZWxlY3RNb2RlKSxcclxuICAgICAgICBmaWx0ZXIoKCkgPT4gIXRoaXMuZGlzYWJsZURyYWcpLFxyXG4gICAgICAgIHN3aXRjaE1hcCgoKSA9PiB0b3VjaG1vdmUkLnBpcGUodGFrZVVudGlsKHRvdWNoZW5kJCkpKSxcclxuICAgICAgICBzaGFyZSgpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBjdXJyZW50TW91c2VQb3NpdGlvbiQ6IE9ic2VydmFibGU8TW91c2VQb3NpdGlvbj4gPSB0b3VjaHN0YXJ0JC5waXBlKFxyXG4gICAgICAgIG1hcCgoZXZlbnQ6IFRvdWNoRXZlbnQpID0+IGdldFJlbGF0aXZlTW91c2VQb3NpdGlvbihldmVudCwgdGhpcy5ob3N0KSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNob3ckID0gZHJhZ2dpbmckLnBpcGUobWFwVG8oMSkpO1xyXG4gICAgICBjb25zdCBoaWRlJCA9IHRvdWNoZW5kJC5waXBlKG1hcFRvKDApKTtcclxuICAgICAgY29uc3Qgb3BhY2l0eSQgPSBtZXJnZShzaG93JCwgaGlkZSQpLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RCb3gkID0gY29tYmluZUxhdGVzdChkcmFnZ2luZyQsIG9wYWNpdHkkLCBjdXJyZW50TW91c2VQb3NpdGlvbiQpLnBpcGUoXHJcbiAgICAgICAgY3JlYXRlU2VsZWN0Qm94KHRoaXMuaG9zdCwgKCkgPT4gdGhpcy5fZGlzYWJsZWRCeUxvbmdUb3VjaCksXHJcbiAgICAgICAgc2hhcmUoKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgdGhpcy5zZWxlY3RCb3hDbGFzc2VzJCA9IG1lcmdlKGRyYWdnaW5nJCwgdG91Y2hlbmQkLCBrZXlkb3duJCwga2V5dXAkKS5waXBlKFxyXG4gICAgICAgIGF1ZGl0VGltZShBVURJVF9USU1FKSxcclxuICAgICAgICB3aXRoTGF0ZXN0RnJvbShzZWxlY3RCb3gkKSxcclxuICAgICAgICBtYXAoKFtldmVudCwgc2VsZWN0Qm94XSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgJ2R0cy1hZGRpbmcnOiBoYXNNaW5pbXVtU2l6ZShzZWxlY3RCb3gsIDAsIDApICYmICF0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KSxcclxuICAgICAgICAgICAgJ2R0cy1yZW1vdmluZyc6IHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpXHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKChhLCBiKSA9PiBKU09OLnN0cmluZ2lmeShhKSA9PT0gSlNPTi5zdHJpbmdpZnkoYikpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RPbk1vdXNlVXAkID0gZHJhZ2dpbmckLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLnNlbGVjdE9uRHJhZyksXHJcbiAgICAgICAgZmlsdGVyKCgpID0+ICF0aGlzLnNlbGVjdE1vZGUpLFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiB0aGlzLl9jdXJzb3JXaXRoaW5Ib3N0KGV2ZW50KSksXHJcbiAgICAgICAgc3dpdGNoTWFwKF8gPT4gdG91Y2hlbmQkLnBpcGUoZmlyc3QoKSkpLFxyXG4gICAgICAgIGZpbHRlcihcclxuICAgICAgICAgIGV2ZW50ID0+XHJcbiAgICAgICAgICAgICghdGhpcy5zaG9ydGN1dHMuZGlzYWJsZVNlbGVjdGlvbihldmVudCkgJiYgIXRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpKSB8fFxyXG4gICAgICAgICAgICB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KVxyXG4gICAgICAgIClcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGNvbnN0IHNlbGVjdE9uRHJhZyQgPSBzZWxlY3RCb3gkLnBpcGUoXHJcbiAgICAgICAgYXVkaXRUaW1lKEFVRElUX1RJTUUpLFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHRvdWNobW92ZSQsIChzZWxlY3RCb3gsIGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkgPT4gKHtcclxuICAgICAgICAgIHNlbGVjdEJveCxcclxuICAgICAgICAgIGV2ZW50XHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIGZpbHRlcigoKSA9PiB0aGlzLnNlbGVjdE9uRHJhZyksXHJcbiAgICAgICAgZmlsdGVyKCh7IHNlbGVjdEJveCB9KSA9PiBoYXNNaW5pbXVtU2l6ZShzZWxlY3RCb3gpKSxcclxuICAgICAgICBtYXAoKHsgZXZlbnQgfSkgPT4gZXZlbnQpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBjb25zdCBzZWxlY3RPbktleWJvYXJkRXZlbnQkID0gbWVyZ2Uoa2V5ZG93biQsIGtleXVwJCkucGlwZShcclxuICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgd2hlblNlbGVjdEJveFZpc2libGUoc2VsZWN0Qm94JCksXHJcbiAgICAgICAgdGFwKGV2ZW50ID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLl9pc0V4dGVuZGVkU2VsZWN0aW9uKGV2ZW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLl90bXBJdGVtcy5jbGVhcigpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fZmx1c2hJdGVtcygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICk7XHJcblxyXG4gICAgICBtZXJnZShzZWxlY3RPbk1vdXNlVXAkLCBzZWxlY3RPbkRyYWckLCBzZWxlY3RPbktleWJvYXJkRXZlbnQkKVxyXG4gICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKSlcclxuICAgICAgICAuc3Vic2NyaWJlKGV2ZW50ID0+IHRoaXMuX3NlbGVjdEl0ZW1zKGV2ZW50KSk7XHJcblxyXG4gICAgICB0aGlzLnNlbGVjdEJveFN0eWxlcyQgPSBzZWxlY3RCb3gkLnBpcGUoXHJcbiAgICAgICAgbWFwKHNlbGVjdEJveCA9PiAoe1xyXG4gICAgICAgICAgdG9wOiBgJHtzZWxlY3RCb3gudG9wfXB4YCxcclxuICAgICAgICAgIGxlZnQ6IGAke3NlbGVjdEJveC5sZWZ0fXB4YCxcclxuICAgICAgICAgIHdpZHRoOiBgJHtzZWxlY3RCb3gud2lkdGh9cHhgLFxyXG4gICAgICAgICAgaGVpZ2h0OiBgJHtzZWxlY3RCb3guaGVpZ2h0fXB4YCxcclxuICAgICAgICAgIG9wYWNpdHk6IHNlbGVjdEJveC5vcGFjaXR5XHJcbiAgICAgICAgfSkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLl9pbml0U2VsZWN0aW9uT3V0cHV0cyh0b3VjaHN0YXJ0JCwgdG91Y2hlbmQkKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlbGVjdEFsbCgpIHtcclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICB0aGlzLl9zZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVJdGVtczxUPihwcmVkaWNhdGU6IFByZWRpY2F0ZUZuPFQ+KSB7XHJcbiAgICB0aGlzLl9maWx0ZXJTZWxlY3RhYmxlSXRlbXMocHJlZGljYXRlKS5zdWJzY3JpYmUoKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpID0+IHRoaXMuX3RvZ2dsZUl0ZW0oaXRlbSkpO1xyXG4gIH1cclxuXHJcbiAgc2VsZWN0SXRlbXM8VD4ocHJlZGljYXRlOiBQcmVkaWNhdGVGbjxUPikge1xyXG4gICAgdGhpcy5fZmlsdGVyU2VsZWN0YWJsZUl0ZW1zKHByZWRpY2F0ZSkuc3Vic2NyaWJlKChpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlKSA9PiB0aGlzLl9zZWxlY3RJdGVtKGl0ZW0pKTtcclxuICB9XHJcblxyXG4gIGRlc2VsZWN0SXRlbXM8VD4ocHJlZGljYXRlOiBQcmVkaWNhdGVGbjxUPikge1xyXG4gICAgdGhpcy5fZmlsdGVyU2VsZWN0YWJsZUl0ZW1zKHByZWRpY2F0ZSkuc3Vic2NyaWJlKChpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlKSA9PiB0aGlzLl9kZXNlbGVjdEl0ZW0oaXRlbSkpO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJTZWxlY3Rpb24oKSB7XHJcbiAgICB0aGlzLiRzZWxlY3RhYmxlSXRlbXMuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgdGhpcy5fZGVzZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGUoKSB7XHJcbiAgICB0aGlzLl9jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5mb3JFYWNoKGl0ZW0gPT4gaXRlbS5jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuZGVzdHJveSQubmV4dCgpO1xyXG4gICAgdGhpcy5kZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZmlsdGVyU2VsZWN0YWJsZUl0ZW1zPFQ+KHByZWRpY2F0ZTogUHJlZGljYXRlRm48VD4pIHtcclxuICAgIC8vIFdyYXAgc2VsZWN0IGl0ZW1zIGluIGFuIG9ic2VydmFibGUgZm9yIGJldHRlciBlZmZpY2llbmN5IGFzXHJcbiAgICAvLyBubyBpbnRlcm1lZGlhdGUgYXJyYXlzIGFyZSBjcmVhdGVkIGFuZCB3ZSBvbmx5IG5lZWQgdG8gcHJvY2Vzc1xyXG4gICAgLy8gZXZlcnkgaXRlbSBvbmNlLlxyXG4gICAgcmV0dXJuIGZyb20odGhpcy4kc2VsZWN0YWJsZUl0ZW1zLnRvQXJyYXkoKSkucGlwZShmaWx0ZXIoaXRlbSA9PiBwcmVkaWNhdGUoaXRlbS52YWx1ZSkpKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2luaXRTZWxlY3RlZEl0ZW1zQ2hhbmdlKCkge1xyXG4gICAgdGhpcy5fc2VsZWN0ZWRJdGVtcyRcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgYXVkaXRUaW1lKEFVRElUX1RJTUUpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoe1xyXG4gICAgICAgIG5leHQ6IHNlbGVjdGVkSXRlbXMgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zQ2hhbmdlLmVtaXQoc2VsZWN0ZWRJdGVtcyk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdC5lbWl0KHNlbGVjdGVkSXRlbXMpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY29tcGxldGU6ICgpID0+IHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtc0NoYW5nZS5lbWl0KFtdKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfb2JzZXJ2ZVNlbGVjdGFibGVJdGVtcygpIHtcclxuICAgIC8vIExpc3RlbiBmb3IgdXBkYXRlcyBhbmQgZWl0aGVyIHNlbGVjdCBvciBkZXNlbGVjdCBhbiBpdGVtXHJcbiAgICB0aGlzLnVwZGF0ZUl0ZW1zJFxyXG4gICAgICAucGlwZShcclxuICAgICAgICB3aXRoTGF0ZXN0RnJvbSh0aGlzLl9zZWxlY3RlZEl0ZW1zJCksXHJcbiAgICAgICAgdGFrZVVudGlsKHRoaXMuZGVzdHJveSQpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgoW3VwZGF0ZSwgc2VsZWN0ZWRJdGVtc106IFtVcGRhdGVBY3Rpb24sIGFueVtdXSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGl0ZW0gPSB1cGRhdGUuaXRlbTtcclxuXHJcbiAgICAgICAgc3dpdGNoICh1cGRhdGUudHlwZSkge1xyXG4gICAgICAgICAgY2FzZSBVcGRhdGVBY3Rpb25zLkFkZDpcclxuICAgICAgICAgICAgaWYgKHRoaXMuX2FkZEl0ZW0oaXRlbSwgc2VsZWN0ZWRJdGVtcykpIHtcclxuICAgICAgICAgICAgICBpdGVtLl9zZWxlY3QoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGNhc2UgVXBkYXRlQWN0aW9ucy5SZW1vdmU6XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9yZW1vdmVJdGVtKGl0ZW0sIHNlbGVjdGVkSXRlbXMpKSB7XHJcbiAgICAgICAgICAgICAgaXRlbS5fZGVzZWxlY3QoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIFVwZGF0ZSB0aGUgY29udGFpbmVyIGFzIHdlbGwgYXMgYWxsIHNlbGVjdGFibGUgaXRlbXMgaWYgdGhlIGxpc3QgaGFzIGNoYW5nZWRcclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5jaGFuZ2VzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHRoaXMuX3NlbGVjdGVkSXRlbXMkKSxcclxuICAgICAgICBvYnNlcnZlT24oYXN5bmNTY2hlZHVsZXIpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoKFtpdGVtcywgc2VsZWN0ZWRJdGVtc106IFtRdWVyeUxpc3Q8U2VsZWN0SXRlbURpcmVjdGl2ZT4sIGFueVtdXSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IG5ld0xpc3QgPSBpdGVtcy50b0FycmF5KCk7XHJcbiAgICAgICAgY29uc3QgcmVtb3ZlZEl0ZW1zID0gc2VsZWN0ZWRJdGVtcy5maWx0ZXIoaXRlbSA9PiAhbmV3TGlzdC5pbmNsdWRlcyhpdGVtLnZhbHVlKSk7XHJcblxyXG4gICAgICAgIGlmIChyZW1vdmVkSXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICByZW1vdmVkSXRlbXMuZm9yRWFjaChpdGVtID0+IHRoaXMuX3JlbW92ZUl0ZW0oaXRlbSwgc2VsZWN0ZWRJdGVtcykpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy51cGRhdGUoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9vYnNlcnZlQm91bmRpbmdSZWN0Q2hhbmdlcygpIHtcclxuICAgIHRoaXMubmdab25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcclxuICAgICAgY29uc3QgcmVzaXplJCA9IGZyb21FdmVudCh3aW5kb3csICdyZXNpemUnKTtcclxuICAgICAgY29uc3Qgd2luZG93U2Nyb2xsJCA9IGZyb21FdmVudCh3aW5kb3csICdzY3JvbGwnKTtcclxuICAgICAgY29uc3QgY29udGFpbmVyU2Nyb2xsJCA9IGZyb21FdmVudCh0aGlzLmhvc3QsICdzY3JvbGwnKTtcclxuXHJcbiAgICAgIG1lcmdlKHJlc2l6ZSQsIHdpbmRvd1Njcm9sbCQsIGNvbnRhaW5lclNjcm9sbCQpXHJcbiAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICBzdGFydFdpdGgoJ0lOSVRJQUxfVVBEQVRFJyksXHJcbiAgICAgICAgICBhdWRpdFRpbWUoQVVESVRfVElNRSksXHJcbiAgICAgICAgICB0YWtlVW50aWwodGhpcy5kZXN0cm95JClcclxuICAgICAgICApXHJcbiAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9pbml0U2VsZWN0aW9uT3V0cHV0cyhcclxuICAgIG1vdXNlZG93biQ6IE9ic2VydmFibGU8TW91c2VFdmVudCB8IFRvdWNoRXZlbnQ+LFxyXG4gICAgbW91c2V1cCQ6IE9ic2VydmFibGU8TW91c2VFdmVudCB8IFRvdWNoRXZlbnQ+XHJcbiAgKSB7XHJcbiAgICBtb3VzZWRvd24kXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiB0aGlzLl9jdXJzb3JXaXRoaW5Ib3N0KGV2ZW50KSksXHJcbiAgICAgICAgdGFwKCgpID0+IHRoaXMuc2VsZWN0aW9uU3RhcnRlZC5lbWl0KCkpLFxyXG4gICAgICAgIGNvbmNhdE1hcFRvKG1vdXNldXAkLnBpcGUoZmlyc3QoKSkpLFxyXG4gICAgICAgIHdpdGhMYXRlc3RGcm9tKHRoaXMuX3NlbGVjdGVkSXRlbXMkKSxcclxuICAgICAgICBtYXAoKFssIGl0ZW1zXSkgPT4gaXRlbXMpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLmRlc3Ryb3kkKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoaXRlbXMgPT4ge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0aW9uRW5kZWQuZW1pdChpdGVtcyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0KCkge1xyXG4gICAgdGhpcy5ob3N0LmJvdW5kaW5nQ2xpZW50UmVjdCA9IGNhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCh0aGlzLmhvc3QpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfY3Vyc29yV2l0aGluSG9zdChldmVudDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpIHtcclxuICAgIHJldHVybiBjdXJzb3JXaXRoaW5FbGVtZW50KGV2ZW50LCB0aGlzLmhvc3QpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfb25Nb3VzZVVwKCkge1xyXG4gICAgdGhpcy5fZmx1c2hJdGVtcygpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhkb2N1bWVudC5ib2R5LCBOT19TRUxFQ1RfQ0xBU1MpO1xyXG4gICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhkb2N1bWVudC5ib2R5LCBOT19TRUxFQ1RfQ0xBU1NfTU9CSUxFKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX29uTW91c2VEb3duKGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xyXG4gICAgaWYgKHRoaXMuc2hvcnRjdXRzLmRpc2FibGVTZWxlY3Rpb24oZXZlbnQpIHx8IHRoaXMuZGlzYWJsZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNsZWFyU2VsZWN0aW9uKHdpbmRvdyk7XHJcblxyXG4gICAgaWYgKCF0aGlzLmRpc2FibGVEcmFnICYmICghdGhpcy5sb25nVG91Y2ggfHwgIXRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2gpKSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3MoZG9jdW1lbnQuYm9keSwgTk9fU0VMRUNUX0NMQVNTKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoIXRoaXMuZGlzYWJsZURyYWcgJiYgdGhpcy5pc01vYmlsZURldmljZSkge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKGRvY3VtZW50LmJvZHksIE5PX1NFTEVDVF9DTEFTU19NT0JJTEUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IG1vdXNlUG9pbnQgPSBnZXRNb3VzZVBvc2l0aW9uKGV2ZW50KTtcclxuXHJcbiAgICB0aGlzLiRzZWxlY3RhYmxlSXRlbXMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgY29uc3QgaXRlbVJlY3QgPSBpdGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgICBjb25zdCB3aXRoaW5Cb3VuZGluZ0JveCA9IGluQm91bmRpbmdCb3gobW91c2VQb2ludCwgaXRlbVJlY3QpO1xyXG5cclxuICAgICAgaWYgKHRoaXMuc2hvcnRjdXRzLmV4dGVuZGVkU2VsZWN0aW9uU2hvcnRjdXQoZXZlbnQpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBzaG91bGRBZGQgPVxyXG4gICAgICAgICh3aXRoaW5Cb3VuZGluZ0JveCAmJlxyXG4gICAgICAgICAgIXRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpICYmXHJcbiAgICAgICAgICAhdGhpcy5zZWxlY3RNb2RlICYmXHJcbiAgICAgICAgICAhdGhpcy5zZWxlY3RXaXRoU2hvcnRjdXQpIHx8XHJcbiAgICAgICAgKHdpdGhpbkJvdW5kaW5nQm94ICYmIHRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpICYmICFpdGVtLnNlbGVjdGVkKSB8fFxyXG4gICAgICAgICghd2l0aGluQm91bmRpbmdCb3ggJiYgdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkgJiYgaXRlbS5zZWxlY3RlZCkgfHxcclxuICAgICAgICAod2l0aGluQm91bmRpbmdCb3ggJiYgIWl0ZW0uc2VsZWN0ZWQgJiYgdGhpcy5zZWxlY3RNb2RlKSB8fFxyXG4gICAgICAgICghd2l0aGluQm91bmRpbmdCb3ggJiYgaXRlbS5zZWxlY3RlZCAmJiB0aGlzLnNlbGVjdE1vZGUpO1xyXG5cclxuICAgICAgY29uc3Qgc2hvdWxkUmVtb3ZlID1cclxuICAgICAgICAoIXdpdGhpbkJvdW5kaW5nQm94ICYmXHJcbiAgICAgICAgICAhdGhpcy5zaG9ydGN1dHMudG9nZ2xlU2luZ2xlSXRlbShldmVudCkgJiZcclxuICAgICAgICAgICF0aGlzLnNlbGVjdE1vZGUgJiZcclxuICAgICAgICAgICF0aGlzLnNlbGVjdFdpdGhTaG9ydGN1dCkgfHxcclxuICAgICAgICAoIXdpdGhpbkJvdW5kaW5nQm94ICYmIHRoaXMuc2hvcnRjdXRzLnRvZ2dsZVNpbmdsZUl0ZW0oZXZlbnQpICYmICFpdGVtLnNlbGVjdGVkKSB8fFxyXG4gICAgICAgICh3aXRoaW5Cb3VuZGluZ0JveCAmJiB0aGlzLnNob3J0Y3V0cy50b2dnbGVTaW5nbGVJdGVtKGV2ZW50KSAmJiBpdGVtLnNlbGVjdGVkKSB8fFxyXG4gICAgICAgICghd2l0aGluQm91bmRpbmdCb3ggJiYgIWl0ZW0uc2VsZWN0ZWQgJiYgdGhpcy5zZWxlY3RNb2RlKSB8fFxyXG4gICAgICAgICh3aXRoaW5Cb3VuZGluZ0JveCAmJiBpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2VsZWN0TW9kZSk7XHJcblxyXG4gICAgICBpZiAoc2hvdWxkQWRkKSB7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0SXRlbShpdGVtKTtcclxuICAgICAgfSBlbHNlIGlmIChzaG91bGRSZW1vdmUpIHtcclxuICAgICAgICB0aGlzLl9kZXNlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfc2VsZWN0SXRlbXMoZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBjb25zdCBzZWxlY3Rpb25Cb3ggPSBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QodGhpcy4kc2VsZWN0Qm94Lm5hdGl2ZUVsZW1lbnQpO1xyXG5cclxuICAgIHRoaXMuJHNlbGVjdGFibGVJdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBpZiAodGhpcy5faXNFeHRlbmRlZFNlbGVjdGlvbihldmVudCkpIHtcclxuICAgICAgICB0aGlzLl9leHRlbmRlZFNlbGVjdGlvbk1vZGUoc2VsZWN0aW9uQm94LCBpdGVtLCBldmVudCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5fbm9ybWFsU2VsZWN0aW9uTW9kZShzZWxlY3Rpb25Cb3gsIGl0ZW0sIGV2ZW50KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9pc0V4dGVuZGVkU2VsZWN0aW9uKGV2ZW50OiBFdmVudCkge1xyXG4gICAgcmV0dXJuIHRoaXMuc2hvcnRjdXRzLmV4dGVuZGVkU2VsZWN0aW9uU2hvcnRjdXQoZXZlbnQpICYmIHRoaXMuc2VsZWN0T25EcmFnO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfbm9ybWFsU2VsZWN0aW9uTW9kZShzZWxlY3RCb3gsIGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUsIGV2ZW50OiBFdmVudCkge1xyXG4gICAgY29uc3QgaW5TZWxlY3Rpb24gPSBib3hJbnRlcnNlY3RzKHNlbGVjdEJveCwgaXRlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSk7XHJcblxyXG4gICAgY29uc3Qgc2hvdWxkQWRkID0gaW5TZWxlY3Rpb24gJiYgIWl0ZW0uc2VsZWN0ZWQgJiYgIXRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpO1xyXG5cclxuICAgIGNvbnN0IHNob3VsZFJlbW92ZSA9XHJcbiAgICAgICghaW5TZWxlY3Rpb24gJiYgaXRlbS5zZWxlY3RlZCAmJiAhdGhpcy5zaG9ydGN1dHMuYWRkVG9TZWxlY3Rpb24oZXZlbnQpKSB8fFxyXG4gICAgICAoaW5TZWxlY3Rpb24gJiYgaXRlbS5zZWxlY3RlZCAmJiB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KSk7XHJcblxyXG4gICAgaWYgKHNob3VsZEFkZCkge1xyXG4gICAgICB0aGlzLl9zZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgfSBlbHNlIGlmIChzaG91bGRSZW1vdmUpIHtcclxuICAgICAgdGhpcy5fZGVzZWxlY3RJdGVtKGl0ZW0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZXh0ZW5kZWRTZWxlY3Rpb25Nb2RlKHNlbGVjdEJveCwgaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSwgZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBjb25zdCBpblNlbGVjdGlvbiA9IGJveEludGVyc2VjdHMoc2VsZWN0Qm94LCBpdGVtLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpKTtcclxuXHJcbiAgICBjb25zdCBzaG91ZGxBZGQgPVxyXG4gICAgICAoaW5TZWxlY3Rpb24gJiYgIWl0ZW0uc2VsZWN0ZWQgJiYgIXRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpICYmICF0aGlzLl90bXBJdGVtcy5oYXMoaXRlbSkpIHx8XHJcbiAgICAgIChpblNlbGVjdGlvbiAmJiBpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpICYmICF0aGlzLl90bXBJdGVtcy5oYXMoaXRlbSkpO1xyXG5cclxuICAgIGNvbnN0IHNob3VsZFJlbW92ZSA9XHJcbiAgICAgICghaW5TZWxlY3Rpb24gJiYgaXRlbS5zZWxlY3RlZCAmJiB0aGlzLnNob3J0Y3V0cy5hZGRUb1NlbGVjdGlvbihldmVudCkgJiYgdGhpcy5fdG1wSXRlbXMuaGFzKGl0ZW0pKSB8fFxyXG4gICAgICAoIWluU2VsZWN0aW9uICYmICFpdGVtLnNlbGVjdGVkICYmIHRoaXMuc2hvcnRjdXRzLnJlbW92ZUZyb21TZWxlY3Rpb24oZXZlbnQpICYmIHRoaXMuX3RtcEl0ZW1zLmhhcyhpdGVtKSk7XHJcblxyXG4gICAgaWYgKHNob3VkbEFkZCkge1xyXG4gICAgICBpdGVtLnNlbGVjdGVkID8gaXRlbS5fZGVzZWxlY3QoKSA6IGl0ZW0uX3NlbGVjdCgpO1xyXG5cclxuICAgICAgY29uc3QgYWN0aW9uID0gdGhpcy5zaG9ydGN1dHMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudClcclxuICAgICAgICA/IEFjdGlvbi5EZWxldGVcclxuICAgICAgICA6IHRoaXMuc2hvcnRjdXRzLmFkZFRvU2VsZWN0aW9uKGV2ZW50KVxyXG4gICAgICAgICAgPyBBY3Rpb24uQWRkXHJcbiAgICAgICAgICA6IEFjdGlvbi5Ob25lO1xyXG5cclxuICAgICAgdGhpcy5fdG1wSXRlbXMuc2V0KGl0ZW0sIGFjdGlvbik7XHJcbiAgICB9IGVsc2UgaWYgKHNob3VsZFJlbW92ZSkge1xyXG4gICAgICB0aGlzLnNob3J0Y3V0cy5yZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50KSA/IGl0ZW0uX3NlbGVjdCgpIDogaXRlbS5fZGVzZWxlY3QoKTtcclxuICAgICAgdGhpcy5fdG1wSXRlbXMuZGVsZXRlKGl0ZW0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZmx1c2hJdGVtcygpIHtcclxuICAgIHRoaXMuX3RtcEl0ZW1zLmZvckVhY2goKGFjdGlvbiwgaXRlbSkgPT4ge1xyXG4gICAgICBpZiAoYWN0aW9uID09PSBBY3Rpb24uQWRkKSB7XHJcbiAgICAgICAgdGhpcy5fc2VsZWN0SXRlbShpdGVtKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGFjdGlvbiA9PT0gQWN0aW9uLkRlbGV0ZSkge1xyXG4gICAgICAgIHRoaXMuX2Rlc2VsZWN0SXRlbShpdGVtKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5fdG1wSXRlbXMuY2xlYXIoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2FkZEl0ZW0oaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSwgc2VsZWN0ZWRJdGVtczogQXJyYXk8YW55Pikge1xyXG4gICAgbGV0IHN1Y2Nlc3MgPSBmYWxzZTtcclxuXHJcbiAgICBpZiAoIXRoaXMuX2Rpc2FibGVkQnlMb25nVG91Y2ggJiYgIXRoaXMuX2hhc0l0ZW0oaXRlbSwgc2VsZWN0ZWRJdGVtcykpIHtcclxuICAgICAgc3VjY2VzcyA9IHRydWU7XHJcbiAgICAgIHNlbGVjdGVkSXRlbXMucHVzaChpdGVtLnZhbHVlKTtcclxuICAgICAgdGhpcy5fc2VsZWN0ZWRJdGVtcyQubmV4dChzZWxlY3RlZEl0ZW1zKTtcclxuICAgICAgdGhpcy5pdGVtU2VsZWN0ZWQuZW1pdChpdGVtLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gc3VjY2VzcztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3JlbW92ZUl0ZW0oaXRlbTogU2VsZWN0SXRlbURpcmVjdGl2ZSwgc2VsZWN0ZWRJdGVtczogQXJyYXk8YW55Pikge1xyXG4gICAgbGV0IHN1Y2Nlc3MgPSBmYWxzZTtcclxuICAgIGNvbnN0IHZhbHVlID0gaXRlbSBpbnN0YW5jZW9mIFNlbGVjdEl0ZW1EaXJlY3RpdmUgPyBpdGVtLnZhbHVlIDogaXRlbTtcclxuICAgIGNvbnN0IGluZGV4ID0gc2VsZWN0ZWRJdGVtcy5pbmRleE9mKHZhbHVlKTtcclxuXHJcbiAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICBzdWNjZXNzID0gdHJ1ZTtcclxuICAgICAgc2VsZWN0ZWRJdGVtcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICB0aGlzLl9zZWxlY3RlZEl0ZW1zJC5uZXh0KHNlbGVjdGVkSXRlbXMpO1xyXG4gICAgICB0aGlzLml0ZW1EZXNlbGVjdGVkLmVtaXQoaXRlbS52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHN1Y2Nlc3M7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF90b2dnbGVJdGVtKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpIHtcclxuICAgIGlmIChpdGVtLnNlbGVjdGVkKSB7XHJcbiAgICAgIHRoaXMuX2Rlc2VsZWN0SXRlbShpdGVtKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuX3NlbGVjdEl0ZW0oaXRlbSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9zZWxlY3RJdGVtKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUpIHtcclxuICAgIHRoaXMudXBkYXRlSXRlbXMkLm5leHQoeyB0eXBlOiBVcGRhdGVBY3Rpb25zLkFkZCwgaXRlbSB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2Rlc2VsZWN0SXRlbShpdGVtOiBTZWxlY3RJdGVtRGlyZWN0aXZlKSB7XHJcbiAgICB0aGlzLnVwZGF0ZUl0ZW1zJC5uZXh0KHsgdHlwZTogVXBkYXRlQWN0aW9ucy5SZW1vdmUsIGl0ZW0gfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9oYXNJdGVtKGl0ZW06IFNlbGVjdEl0ZW1EaXJlY3RpdmUsIHNlbGVjdGVkSXRlbXM6IEFycmF5PGFueT4pIHtcclxuICAgIHJldHVybiBzZWxlY3RlZEl0ZW1zLmluY2x1ZGVzKGl0ZW0udmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZHJhd1JpcHBsZShldmVudDogVG91Y2hFdmVudCkge1xyXG4gICAgY29uc3QgcG9zaXRpb24gPSBnZXRNb3VzZVBvc2l0aW9uKGV2ZW50KTtcclxuXHJcbiAgICBjb25zdCBub2RlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnJpcHBsZScpO1xyXG5cclxuICAgIGNvbnN0IG5ld05vZGUgPSBub2RlLmNsb25lTm9kZSh0cnVlKSBhcyBIVE1MRWxlbWVudDtcclxuICAgIG5ld05vZGUuY2xhc3NMaXN0LmFkZCgnYW5pbWF0ZScpO1xyXG4gICAgbmV3Tm9kZS5zdHlsZS5sZWZ0ID0gcG9zaXRpb24ueCAtIDUgKyAncHgnO1xyXG4gICAgbmV3Tm9kZS5zdHlsZS50b3AgPSBwb3NpdGlvbi55IC0gNSArICdweCc7XHJcblxyXG4gICAgbm9kZS5wYXJlbnROb2RlLnJlcGxhY2VDaGlsZChuZXdOb2RlLCBub2RlKTtcclxuICB9O1xyXG5cclxuICBwcml2YXRlIF9hY3RpdmF0ZVNlbGVjdGluZ0FmdGVyTG9uZ1RvdWNoKGV2ZW50OiBUb3VjaEV2ZW50KSB7XHJcbiAgICB0aGlzLl9kaXNhYmxlZEJ5TG9uZ1RvdWNoID0gZmFsc2U7XHJcbiAgICBjbGVhckludGVydmFsKHRoaXMuX2xvbmdUb3VjaFRpbWVyKTtcclxuICAgIHRoaXMuX2xvbmdUb3VjaFRpbWVyID0gbnVsbDtcclxuICAgIHRoaXMuJHNlbGVjdEJveC5uYXRpdmVFbGVtZW50LnN0eWxlLm9wYWNpdHkgPSAxO1xyXG5cclxuICAgIGlmICghdGhpcy5kaXNhYmxlRHJhZykge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKGRvY3VtZW50LmJvZHksIE5PX1NFTEVDVF9DTEFTUyk7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuYWRkQ2xhc3MoZG9jdW1lbnQuYm9keSwgTk9fU0VMRUNUX0NMQVNTX01PQklMRSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5fZHJhd1JpcHBsZShldmVudCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9kZWFjdGl2YXRlU2VsZWN0aW5nQWZ0ZXJMb25nVG91Y2goKSB7XHJcbiAgICB0aGlzLl9kaXNhYmxlZEJ5TG9uZ1RvdWNoID0gdHJ1ZTtcclxuICAgIHRoaXMuJHNlbGVjdEJveC5uYXRpdmVFbGVtZW50LnN0eWxlLm9wYWNpdHkgPSAwO1xyXG4gICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICdhdXRvJztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgbG9nKG1zZzogc3RyaW5nKTogdm9pZCB7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWVzc2FnZXMnKS5zdHlsZS53aGl0ZVNwYWNlID0gJ25vcm1hbCc7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWVzc2FnZXMnKS5pbm5lckhUTUwgKz0gJyAnICsgbXNnO1xyXG4gIH1cclxufVxyXG4iXX0=