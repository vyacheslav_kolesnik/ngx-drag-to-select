/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { isPlatformBrowser } from '@angular/common';
import { Directive, ElementRef, Inject, Input, PLATFORM_ID, Renderer2 } from '@angular/core';
import { CONFIG } from './tokens';
import { calculateBoundingClientRect } from './utils';
export class SelectItemDirective {
    /**
     * @param {?} config
     * @param {?} platformId
     * @param {?} host
     * @param {?} renderer
     */
    constructor(config, platformId, host, renderer) {
        this.config = config;
        this.platformId = platformId;
        this.host = host;
        this.renderer = renderer;
        this.selected = false;
    }
    /**
     * @return {?}
     */
    get value() {
        return this.dtsSelectItem ? this.dtsSelectItem : this;
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        this.applySelectedClass();
    }
    /**
     * @return {?}
     */
    getBoundingClientRect() {
        if (isPlatformBrowser(this.platformId) && !this._boundingClientRect) {
            this.calculateBoundingClientRect();
        }
        return this._boundingClientRect;
    }
    /**
     * @return {?}
     */
    calculateBoundingClientRect() {
        this._boundingClientRect = calculateBoundingClientRect(this.host.nativeElement);
    }
    /**
     * @return {?}
     */
    _select() {
        this.selected = true;
    }
    /**
     * @return {?}
     */
    _deselect() {
        this.selected = false;
    }
    /**
     * @private
     * @return {?}
     */
    applySelectedClass() {
        if (this.selected) {
            this.renderer.addClass(this.host.nativeElement, this.config.selectedClass);
        }
        else {
            this.renderer.removeClass(this.host.nativeElement, this.config.selectedClass);
        }
    }
}
SelectItemDirective.decorators = [
    { type: Directive, args: [{
                selector: '[dtsSelectItem]',
                exportAs: 'dtsSelectItem',
                host: {
                    class: 'dts-select-item'
                }
            },] }
];
/** @nocollapse */
SelectItemDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] },
    { type: undefined, decorators: [{ type: Inject, args: [PLATFORM_ID,] }] },
    { type: ElementRef },
    { type: Renderer2 }
];
SelectItemDirective.propDecorators = {
    dtsSelectItem: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype._boundingClientRect;
    /** @type {?} */
    SelectItemDirective.prototype.selected;
    /** @type {?} */
    SelectItemDirective.prototype.dtsSelectItem;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.platformId;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.host;
    /**
     * @type {?}
     * @private
     */
    SelectItemDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWl0ZW0uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL3NlbGVjdC1pdGVtLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFcEQsT0FBTyxFQUFFLFNBQVMsRUFBVyxVQUFVLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR3RHLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDbEMsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBU3RELE1BQU0sT0FBTyxtQkFBbUI7Ozs7Ozs7SUFZOUIsWUFDMEIsTUFBMEIsRUFDckIsVUFBVSxFQUMvQixJQUFnQixFQUNoQixRQUFtQjtRQUhILFdBQU0sR0FBTixNQUFNLENBQW9CO1FBQ3JCLGVBQVUsR0FBVixVQUFVLENBQUE7UUFDL0IsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBYjdCLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFjZCxDQUFDOzs7O0lBVEosSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDeEQsQ0FBQzs7OztJQVNELFNBQVM7UUFDUCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ25CLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQ25FLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELDJCQUEyQjtRQUN6QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsMkJBQTJCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsRixDQUFDOzs7O0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFTyxrQkFBa0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDNUU7YUFBTTtZQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDL0U7SUFDSCxDQUFDOzs7WUF2REYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixJQUFJLEVBQUU7b0JBQ0osS0FBSyxFQUFFLGlCQUFpQjtpQkFDekI7YUFDRjs7Ozs0Q0FjSSxNQUFNLFNBQUMsTUFBTTs0Q0FDYixNQUFNLFNBQUMsV0FBVztZQTNCTSxVQUFVO1lBQThCLFNBQVM7Ozs0QkFrQjNFLEtBQUs7Ozs7Ozs7SUFKTixrREFBNEI7O0lBRTVCLHVDQUFpQjs7SUFFakIsNENBQ2M7Ozs7O0lBT1oscUNBQWtEOzs7OztJQUNsRCx5Q0FBdUM7Ozs7O0lBQ3ZDLG1DQUF3Qjs7Ozs7SUFDeEIsdUNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgaXNQbGF0Zm9ybUJyb3dzZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgRGlyZWN0aXZlLCBEb0NoZWNrLCBFbGVtZW50UmVmLCBJbmplY3QsIElucHV0LCBQTEFURk9STV9JRCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBEcmFnVG9TZWxlY3RDb25maWcgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IENPTkZJRyB9IGZyb20gJy4vdG9rZW5zJztcclxuaW1wb3J0IHsgY2FsY3VsYXRlQm91bmRpbmdDbGllbnRSZWN0IH0gZnJvbSAnLi91dGlscyc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tkdHNTZWxlY3RJdGVtXScsXHJcbiAgZXhwb3J0QXM6ICdkdHNTZWxlY3RJdGVtJyxcclxuICBob3N0OiB7XHJcbiAgICBjbGFzczogJ2R0cy1zZWxlY3QtaXRlbSdcclxuICB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWxlY3RJdGVtRGlyZWN0aXZlIGltcGxlbWVudHMgRG9DaGVjayB7XHJcbiAgcHJpdmF0ZSBfYm91bmRpbmdDbGllbnRSZWN0O1xyXG5cclxuICBzZWxlY3RlZCA9IGZhbHNlO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIGR0c1NlbGVjdEl0ZW07XHJcblxyXG4gIGdldCB2YWx1ZSgpIHtcclxuICAgIHJldHVybiB0aGlzLmR0c1NlbGVjdEl0ZW0gPyB0aGlzLmR0c1NlbGVjdEl0ZW0gOiB0aGlzO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPTkZJRykgcHJpdmF0ZSBjb25maWc6IERyYWdUb1NlbGVjdENvbmZpZyxcclxuICAgIEBJbmplY3QoUExBVEZPUk1fSUQpIHByaXZhdGUgcGxhdGZvcm1JZCxcclxuICAgIHByaXZhdGUgaG9zdDogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICkge31cclxuXHJcbiAgbmdEb0NoZWNrKCkge1xyXG4gICAgdGhpcy5hcHBseVNlbGVjdGVkQ2xhc3MoKTtcclxuICB9XHJcblxyXG4gIGdldEJvdW5kaW5nQ2xpZW50UmVjdCgpIHtcclxuICAgIGlmIChpc1BsYXRmb3JtQnJvd3Nlcih0aGlzLnBsYXRmb3JtSWQpICYmICF0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3QpIHtcclxuICAgICAgdGhpcy5jYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3Q7XHJcbiAgfVxyXG5cclxuICBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QoKSB7XHJcbiAgICB0aGlzLl9ib3VuZGluZ0NsaWVudFJlY3QgPSBjYWxjdWxhdGVCb3VuZGluZ0NsaWVudFJlY3QodGhpcy5ob3N0Lm5hdGl2ZUVsZW1lbnQpO1xyXG4gIH1cclxuXHJcbiAgX3NlbGVjdCgpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWQgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgX2Rlc2VsZWN0KCkge1xyXG4gICAgdGhpcy5zZWxlY3RlZCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhcHBseVNlbGVjdGVkQ2xhc3MoKSB7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZCkge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuaG9zdC5uYXRpdmVFbGVtZW50LCB0aGlzLmNvbmZpZy5zZWxlY3RlZENsYXNzKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5ob3N0Lm5hdGl2ZUVsZW1lbnQsIHRoaXMuY29uZmlnLnNlbGVjdGVkQ2xhc3MpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=