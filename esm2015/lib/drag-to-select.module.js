/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectContainerComponent } from './select-container.component';
import { SelectItemDirective } from './select-item.directive';
import { ShortcutService } from './shortcut.service';
import { CONFIG, USER_CONFIG } from './tokens';
import { DEFAULT_CONFIG } from './config';
import { mergeDeep } from './utils';
/** @type {?} */
const COMPONENTS = [SelectContainerComponent, SelectItemDirective];
/**
 * @param {?} config
 * @return {?}
 */
export function CONFIG_FACTORY(config) {
    return mergeDeep(DEFAULT_CONFIG, config);
}
export class DragToSelectModule {
    /**
     * @param {?=} config
     * @return {?}
     */
    static forRoot(config = {}) {
        return {
            ngModule: DragToSelectModule,
            providers: [
                ShortcutService,
                { provide: USER_CONFIG, useValue: config },
                {
                    provide: CONFIG,
                    useFactory: CONFIG_FACTORY,
                    deps: [USER_CONFIG]
                }
            ]
        };
    }
}
DragToSelectModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                declarations: [...COMPONENTS],
                exports: [...COMPONENTS]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhZy10by1zZWxlY3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL2RyYWctdG8tc2VsZWN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBdUIsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3hFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVyRCxPQUFPLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxTQUFTLENBQUM7O01BRTlCLFVBQVUsR0FBRyxDQUFDLHdCQUF3QixFQUFFLG1CQUFtQixDQUFDOzs7OztBQUVsRSxNQUFNLFVBQVUsY0FBYyxDQUFDLE1BQW1DO0lBQ2hFLE9BQU8sU0FBUyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUMzQyxDQUFDO0FBT0QsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7SUFDN0IsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFzQyxFQUFFO1FBQ3JELE9BQU87WUFDTCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFNBQVMsRUFBRTtnQkFDVCxlQUFlO2dCQUNmLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO2dCQUMxQztvQkFDRSxPQUFPLEVBQUUsTUFBTTtvQkFDZixVQUFVLEVBQUUsY0FBYztvQkFDMUIsSUFBSSxFQUFFLENBQUMsV0FBVyxDQUFDO2lCQUNwQjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7OztZQW5CRixRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUN2QixZQUFZLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUM7YUFDekIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgU2VsZWN0Q29udGFpbmVyQ29tcG9uZW50IH0gZnJvbSAnLi9zZWxlY3QtY29udGFpbmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlbGVjdEl0ZW1EaXJlY3RpdmUgfSBmcm9tICcuL3NlbGVjdC1pdGVtLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFNob3J0Y3V0U2VydmljZSB9IGZyb20gJy4vc2hvcnRjdXQuc2VydmljZSc7XHJcbmltcG9ydCB7IERyYWdUb1NlbGVjdENvbmZpZyB9IGZyb20gJy4vbW9kZWxzJztcclxuaW1wb3J0IHsgQ09ORklHLCBVU0VSX0NPTkZJRyB9IGZyb20gJy4vdG9rZW5zJztcclxuaW1wb3J0IHsgREVGQVVMVF9DT05GSUcgfSBmcm9tICcuL2NvbmZpZyc7XHJcbmltcG9ydCB7IG1lcmdlRGVlcCB9IGZyb20gJy4vdXRpbHMnO1xyXG5cclxuY29uc3QgQ09NUE9ORU5UUyA9IFtTZWxlY3RDb250YWluZXJDb21wb25lbnQsIFNlbGVjdEl0ZW1EaXJlY3RpdmVdO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIENPTkZJR19GQUNUT1JZKGNvbmZpZzogUGFydGlhbDxEcmFnVG9TZWxlY3RDb25maWc+KSB7XHJcbiAgcmV0dXJuIG1lcmdlRGVlcChERUZBVUxUX0NPTkZJRywgY29uZmlnKTtcclxufVxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcclxuICBkZWNsYXJhdGlvbnM6IFsuLi5DT01QT05FTlRTXSxcclxuICBleHBvcnRzOiBbLi4uQ09NUE9ORU5UU11cclxufSlcclxuZXhwb3J0IGNsYXNzIERyYWdUb1NlbGVjdE1vZHVsZSB7XHJcbiAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBQYXJ0aWFsPERyYWdUb1NlbGVjdENvbmZpZz4gPSB7fSk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IERyYWdUb1NlbGVjdE1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgU2hvcnRjdXRTZXJ2aWNlLFxyXG4gICAgICAgIHsgcHJvdmlkZTogVVNFUl9DT05GSUcsIHVzZVZhbHVlOiBjb25maWcgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBDT05GSUcsXHJcbiAgICAgICAgICB1c2VGYWN0b3J5OiBDT05GSUdfRkFDVE9SWSxcclxuICAgICAgICAgIGRlcHM6IFtVU0VSX0NPTkZJR11cclxuICAgICAgICB9XHJcbiAgICAgIF1cclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==