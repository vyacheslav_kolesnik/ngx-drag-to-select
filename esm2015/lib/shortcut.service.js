/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { CONFIG } from './tokens';
/** @type {?} */
const SUPPORTED_KEYS = {
    alt: true,
    shift: true,
    meta: true,
    ctrl: true
};
/** @type {?} */
const META_KEY = 'meta';
/** @type {?} */
const KEY_ALIASES = {
    [META_KEY]: ['ctrl', 'meta']
};
/** @type {?} */
const SUPPORTED_SHORTCUTS = {
    disableSelection: true,
    toggleSingleItem: true,
    addToSelection: true,
    removeFromSelection: true
};
/** @type {?} */
const ERROR_PREFIX = '[ShortcutService]';
export class ShortcutService {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
        this._shortcuts = {};
        this._shortcuts = this.createShortcutsFromConfig(config.shortcuts);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    disableSelection(event) {
        return this.isShortcutPressed('disableSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    toggleSingleItem(event) {
        return this.isShortcutPressed('toggleSingleItem', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    addToSelection(event) {
        return this.isShortcutPressed('addToSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    removeFromSelection(event) {
        return this.isShortcutPressed('removeFromSelection', event);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    extendedSelectionShortcut(event) {
        return this.addToSelection(event) || this.removeFromSelection(event);
    }
    /**
     * @private
     * @param {?} shortcuts
     * @return {?}
     */
    createShortcutsFromConfig(shortcuts) {
        /** @type {?} */
        const shortcutMap = {};
        for (const [key, shortcutsForCommand] of Object.entries(shortcuts)) {
            if (!this.isSupportedShortcut(key)) {
                throw new Error(this.getErrorMessage(`Shortcut ${key} not supported`));
            }
            shortcutsForCommand
                .replace(/ /g, '')
                .split(',')
                .forEach((/**
             * @param {?} shortcut
             * @return {?}
             */
            shortcut => {
                if (!shortcutMap[key]) {
                    shortcutMap[key] = [];
                }
                /** @type {?} */
                const combo = shortcut.split('+');
                /** @type {?} */
                const cleanCombos = this.substituteKey(shortcut, combo, META_KEY);
                cleanCombos.forEach((/**
                 * @param {?} cleanCombo
                 * @return {?}
                 */
                cleanCombo => {
                    /** @type {?} */
                    const unsupportedKey = this.isSupportedCombo(cleanCombo);
                    if (unsupportedKey) {
                        throw new Error(this.getErrorMessage(`Key '${unsupportedKey}' in shortcut ${shortcut} not supported`));
                    }
                    shortcutMap[key].push(cleanCombo.map((/**
                     * @param {?} comboKey
                     * @return {?}
                     */
                    comboKey => `${comboKey}Key`)));
                }));
            }));
        }
        return shortcutMap;
    }
    /**
     * @private
     * @param {?} shortcut
     * @param {?} combo
     * @param {?} substituteKey
     * @return {?}
     */
    substituteKey(shortcut, combo, substituteKey) {
        /** @type {?} */
        const hasSpecialKey = shortcut.includes(substituteKey);
        /** @type {?} */
        const substitutedShortcut = [];
        if (hasSpecialKey) {
            /** @type {?} */
            const cleanShortcut = combo.filter((/**
             * @param {?} element
             * @return {?}
             */
            element => element !== META_KEY));
            KEY_ALIASES.meta.forEach((/**
             * @param {?} alias
             * @return {?}
             */
            alias => {
                substitutedShortcut.push([...cleanShortcut, alias]);
            }));
        }
        else {
            substitutedShortcut.push(combo);
        }
        return substitutedShortcut;
    }
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    getErrorMessage(message) {
        return `${ERROR_PREFIX} ${message}`;
    }
    /**
     * @private
     * @param {?} shortcutName
     * @param {?} event
     * @return {?}
     */
    isShortcutPressed(shortcutName, event) {
        /** @type {?} */
        const shortcuts = this._shortcuts[shortcutName];
        return shortcuts.some((/**
         * @param {?} shortcut
         * @return {?}
         */
        shortcut => {
            return shortcut.every((/**
             * @param {?} key
             * @return {?}
             */
            key => event[key]));
        }));
    }
    /**
     * @private
     * @param {?} combo
     * @return {?}
     */
    isSupportedCombo(combo) {
        /** @type {?} */
        let unsupportedKey = null;
        combo.forEach((/**
         * @param {?} key
         * @return {?}
         */
        key => {
            if (!SUPPORTED_KEYS[key]) {
                unsupportedKey = key;
                return;
            }
        }));
        return unsupportedKey;
    }
    /**
     * @private
     * @param {?} shortcut
     * @return {?}
     */
    isSupportedShortcut(shortcut) {
        return SUPPORTED_SHORTCUTS[shortcut];
    }
}
ShortcutService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ShortcutService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [CONFIG,] }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype._shortcuts;
    /**
     * @type {?}
     * @private
     */
    ShortcutService.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcnRjdXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1kcmFnLXRvLXNlbGVjdC8iLCJzb3VyY2VzIjpbImxpYi9zaG9ydGN1dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVuRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFDOztNQUU1QixjQUFjLEdBQUc7SUFDckIsR0FBRyxFQUFFLElBQUk7SUFDVCxLQUFLLEVBQUUsSUFBSTtJQUNYLElBQUksRUFBRSxJQUFJO0lBQ1YsSUFBSSxFQUFFLElBQUk7Q0FDWDs7TUFFSyxRQUFRLEdBQUcsTUFBTTs7TUFFakIsV0FBVyxHQUFHO0lBQ2xCLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDO0NBQzdCOztNQUVLLG1CQUFtQixHQUFHO0lBQzFCLGdCQUFnQixFQUFFLElBQUk7SUFDdEIsZ0JBQWdCLEVBQUUsSUFBSTtJQUN0QixjQUFjLEVBQUUsSUFBSTtJQUNwQixtQkFBbUIsRUFBRSxJQUFJO0NBQzFCOztNQUVLLFlBQVksR0FBRyxtQkFBbUI7QUFHeEMsTUFBTSxPQUFPLGVBQWU7Ozs7SUFHMUIsWUFBb0MsTUFBMEI7UUFBMUIsV0FBTSxHQUFOLE1BQU0sQ0FBb0I7UUFGdEQsZUFBVSxHQUFrQyxFQUFFLENBQUM7UUFHckQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3JFLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsS0FBWTtRQUMzQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEtBQVk7UUFDM0IsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0QsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsS0FBWTtRQUN6QixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLEtBQVk7UUFDOUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7SUFFRCx5QkFBeUIsQ0FBQyxLQUFZO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkUsQ0FBQzs7Ozs7O0lBRU8seUJBQXlCLENBQUMsU0FBb0M7O2NBQzlELFdBQVcsR0FBRyxFQUFFO1FBRXRCLEtBQUssTUFBTSxDQUFDLEdBQUcsRUFBRSxtQkFBbUIsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7YUFDeEU7WUFFRCxtQkFBbUI7aUJBQ2hCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDO2lCQUNqQixLQUFLLENBQUMsR0FBRyxDQUFDO2lCQUNWLE9BQU87Ozs7WUFBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDckIsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDdkI7O3NCQUVLLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7c0JBQzNCLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDO2dCQUVqRSxXQUFXLENBQUMsT0FBTzs7OztnQkFBQyxVQUFVLENBQUMsRUFBRTs7MEJBQ3pCLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO29CQUV4RCxJQUFJLGNBQWMsRUFBRTt3QkFDbEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsY0FBYyxpQkFBaUIsUUFBUSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7cUJBQ3hHO29CQUVELFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUc7Ozs7b0JBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsS0FBSyxFQUFDLENBQUMsQ0FBQztnQkFDdEUsQ0FBQyxFQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsT0FBTyxXQUFXLENBQUM7SUFDckIsQ0FBQzs7Ozs7Ozs7SUFFTyxhQUFhLENBQUMsUUFBZ0IsRUFBRSxLQUFvQixFQUFFLGFBQXFCOztjQUMzRSxhQUFhLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7O2NBQ2hELG1CQUFtQixHQUFHLEVBQUU7UUFFOUIsSUFBSSxhQUFhLEVBQUU7O2tCQUNYLGFBQWEsR0FBRyxLQUFLLENBQUMsTUFBTTs7OztZQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxLQUFLLFFBQVEsRUFBQztZQUVuRSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTtnQkFDL0IsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0RCxDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakM7UUFFRCxPQUFPLG1CQUFtQixDQUFDO0lBQzdCLENBQUM7Ozs7OztJQUVPLGVBQWUsQ0FBQyxPQUFlO1FBQ3JDLE9BQU8sR0FBRyxZQUFZLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdEMsQ0FBQzs7Ozs7OztJQUVPLGlCQUFpQixDQUFDLFlBQW9CLEVBQUUsS0FBWTs7Y0FDcEQsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1FBRS9DLE9BQU8sU0FBUyxDQUFDLElBQUk7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixPQUFPLFFBQVEsQ0FBQyxLQUFLOzs7O1lBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztRQUMzQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLGdCQUFnQixDQUFDLEtBQW9COztZQUN2QyxjQUFjLEdBQUcsSUFBSTtRQUV6QixLQUFLLENBQUMsT0FBTzs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLGNBQWMsR0FBRyxHQUFHLENBQUM7Z0JBQ3JCLE9BQU87YUFDUjtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxjQUFjLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBRU8sbUJBQW1CLENBQUMsUUFBZ0I7UUFDMUMsT0FBTyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7WUExR0YsVUFBVTs7Ozs0Q0FJSSxNQUFNLFNBQUMsTUFBTTs7Ozs7OztJQUYxQixxQ0FBdUQ7Ozs7O0lBRTNDLGlDQUFrRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEcmFnVG9TZWxlY3RDb25maWcgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IENPTkZJRyB9IGZyb20gJy4vdG9rZW5zJztcclxuXHJcbmNvbnN0IFNVUFBPUlRFRF9LRVlTID0ge1xyXG4gIGFsdDogdHJ1ZSxcclxuICBzaGlmdDogdHJ1ZSxcclxuICBtZXRhOiB0cnVlLFxyXG4gIGN0cmw6IHRydWVcclxufTtcclxuXHJcbmNvbnN0IE1FVEFfS0VZID0gJ21ldGEnO1xyXG5cclxuY29uc3QgS0VZX0FMSUFTRVMgPSB7XHJcbiAgW01FVEFfS0VZXTogWydjdHJsJywgJ21ldGEnXVxyXG59O1xyXG5cclxuY29uc3QgU1VQUE9SVEVEX1NIT1JUQ1VUUyA9IHtcclxuICBkaXNhYmxlU2VsZWN0aW9uOiB0cnVlLFxyXG4gIHRvZ2dsZVNpbmdsZUl0ZW06IHRydWUsXHJcbiAgYWRkVG9TZWxlY3Rpb246IHRydWUsXHJcbiAgcmVtb3ZlRnJvbVNlbGVjdGlvbjogdHJ1ZVxyXG59O1xyXG5cclxuY29uc3QgRVJST1JfUFJFRklYID0gJ1tTaG9ydGN1dFNlcnZpY2VdJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFNob3J0Y3V0U2VydmljZSB7XHJcbiAgcHJpdmF0ZSBfc2hvcnRjdXRzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZ1tdW10gfSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihASW5qZWN0KENPTkZJRykgcHJpdmF0ZSBjb25maWc6IERyYWdUb1NlbGVjdENvbmZpZykge1xyXG4gICAgdGhpcy5fc2hvcnRjdXRzID0gdGhpcy5jcmVhdGVTaG9ydGN1dHNGcm9tQ29uZmlnKGNvbmZpZy5zaG9ydGN1dHMpO1xyXG4gIH1cclxuXHJcbiAgZGlzYWJsZVNlbGVjdGlvbihldmVudDogRXZlbnQpIHtcclxuICAgIHJldHVybiB0aGlzLmlzU2hvcnRjdXRQcmVzc2VkKCdkaXNhYmxlU2VsZWN0aW9uJywgZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlU2luZ2xlSXRlbShldmVudDogRXZlbnQpIHtcclxuICAgIHJldHVybiB0aGlzLmlzU2hvcnRjdXRQcmVzc2VkKCd0b2dnbGVTaW5nbGVJdGVtJywgZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgYWRkVG9TZWxlY3Rpb24oZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICByZXR1cm4gdGhpcy5pc1Nob3J0Y3V0UHJlc3NlZCgnYWRkVG9TZWxlY3Rpb24nLCBldmVudCk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVGcm9tU2VsZWN0aW9uKGV2ZW50OiBFdmVudCkge1xyXG4gICAgcmV0dXJuIHRoaXMuaXNTaG9ydGN1dFByZXNzZWQoJ3JlbW92ZUZyb21TZWxlY3Rpb24nLCBldmVudCk7XHJcbiAgfVxyXG5cclxuICBleHRlbmRlZFNlbGVjdGlvblNob3J0Y3V0KGV2ZW50OiBFdmVudCkge1xyXG4gICAgcmV0dXJuIHRoaXMuYWRkVG9TZWxlY3Rpb24oZXZlbnQpIHx8IHRoaXMucmVtb3ZlRnJvbVNlbGVjdGlvbihldmVudCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZVNob3J0Y3V0c0Zyb21Db25maWcoc2hvcnRjdXRzOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9KSB7XHJcbiAgICBjb25zdCBzaG9ydGN1dE1hcCA9IHt9O1xyXG5cclxuICAgIGZvciAoY29uc3QgW2tleSwgc2hvcnRjdXRzRm9yQ29tbWFuZF0gb2YgT2JqZWN0LmVudHJpZXMoc2hvcnRjdXRzKSkge1xyXG4gICAgICBpZiAoIXRoaXMuaXNTdXBwb3J0ZWRTaG9ydGN1dChrZXkpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKHRoaXMuZ2V0RXJyb3JNZXNzYWdlKGBTaG9ydGN1dCAke2tleX0gbm90IHN1cHBvcnRlZGApKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgc2hvcnRjdXRzRm9yQ29tbWFuZFxyXG4gICAgICAgIC5yZXBsYWNlKC8gL2csICcnKVxyXG4gICAgICAgIC5zcGxpdCgnLCcpXHJcbiAgICAgICAgLmZvckVhY2goc2hvcnRjdXQgPT4ge1xyXG4gICAgICAgICAgaWYgKCFzaG9ydGN1dE1hcFtrZXldKSB7XHJcbiAgICAgICAgICAgIHNob3J0Y3V0TWFwW2tleV0gPSBbXTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBjb25zdCBjb21ibyA9IHNob3J0Y3V0LnNwbGl0KCcrJyk7XHJcbiAgICAgICAgICBjb25zdCBjbGVhbkNvbWJvcyA9IHRoaXMuc3Vic3RpdHV0ZUtleShzaG9ydGN1dCwgY29tYm8sIE1FVEFfS0VZKTtcclxuXHJcbiAgICAgICAgICBjbGVhbkNvbWJvcy5mb3JFYWNoKGNsZWFuQ29tYm8gPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB1bnN1cHBvcnRlZEtleSA9IHRoaXMuaXNTdXBwb3J0ZWRDb21ibyhjbGVhbkNvbWJvKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh1bnN1cHBvcnRlZEtleSkge1xyXG4gICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcih0aGlzLmdldEVycm9yTWVzc2FnZShgS2V5ICcke3Vuc3VwcG9ydGVkS2V5fScgaW4gc2hvcnRjdXQgJHtzaG9ydGN1dH0gbm90IHN1cHBvcnRlZGApKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgc2hvcnRjdXRNYXBba2V5XS5wdXNoKGNsZWFuQ29tYm8ubWFwKGNvbWJvS2V5ID0+IGAke2NvbWJvS2V5fUtleWApKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBzaG9ydGN1dE1hcDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3Vic3RpdHV0ZUtleShzaG9ydGN1dDogc3RyaW5nLCBjb21ibzogQXJyYXk8c3RyaW5nPiwgc3Vic3RpdHV0ZUtleTogc3RyaW5nKSB7XHJcbiAgICBjb25zdCBoYXNTcGVjaWFsS2V5ID0gc2hvcnRjdXQuaW5jbHVkZXMoc3Vic3RpdHV0ZUtleSk7XHJcbiAgICBjb25zdCBzdWJzdGl0dXRlZFNob3J0Y3V0ID0gW107XHJcblxyXG4gICAgaWYgKGhhc1NwZWNpYWxLZXkpIHtcclxuICAgICAgY29uc3QgY2xlYW5TaG9ydGN1dCA9IGNvbWJvLmZpbHRlcihlbGVtZW50ID0+IGVsZW1lbnQgIT09IE1FVEFfS0VZKTtcclxuXHJcbiAgICAgIEtFWV9BTElBU0VTLm1ldGEuZm9yRWFjaChhbGlhcyA9PiB7XHJcbiAgICAgICAgc3Vic3RpdHV0ZWRTaG9ydGN1dC5wdXNoKFsuLi5jbGVhblNob3J0Y3V0LCBhbGlhc10pO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHN1YnN0aXR1dGVkU2hvcnRjdXQucHVzaChjb21ibyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHN1YnN0aXR1dGVkU2hvcnRjdXQ7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldEVycm9yTWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBgJHtFUlJPUl9QUkVGSVh9ICR7bWVzc2FnZX1gO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpc1Nob3J0Y3V0UHJlc3NlZChzaG9ydGN1dE5hbWU6IHN0cmluZywgZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBjb25zdCBzaG9ydGN1dHMgPSB0aGlzLl9zaG9ydGN1dHNbc2hvcnRjdXROYW1lXTtcclxuXHJcbiAgICByZXR1cm4gc2hvcnRjdXRzLnNvbWUoc2hvcnRjdXQgPT4ge1xyXG4gICAgICByZXR1cm4gc2hvcnRjdXQuZXZlcnkoa2V5ID0+IGV2ZW50W2tleV0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGlzU3VwcG9ydGVkQ29tYm8oY29tYm86IEFycmF5PHN0cmluZz4pIHtcclxuICAgIGxldCB1bnN1cHBvcnRlZEtleSA9IG51bGw7XHJcblxyXG4gICAgY29tYm8uZm9yRWFjaChrZXkgPT4ge1xyXG4gICAgICBpZiAoIVNVUFBPUlRFRF9LRVlTW2tleV0pIHtcclxuICAgICAgICB1bnN1cHBvcnRlZEtleSA9IGtleTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiB1bnN1cHBvcnRlZEtleTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaXNTdXBwb3J0ZWRTaG9ydGN1dChzaG9ydGN1dDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gU1VQUE9SVEVEX1NIT1JUQ1VUU1tzaG9ydGN1dF07XHJcbiAgfVxyXG59XHJcbiJdfQ==