/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export const DEFAULT_CONFIG = {
    selectedClass: 'selected',
    shortcuts: {
        disableSelection: 'alt',
        toggleSingleItem: 'meta',
        addToSelection: 'shift',
        removeFromSelection: 'shift+meta'
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWRyYWctdG8tc2VsZWN0LyIsInNvdXJjZXMiOlsibGliL2NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUVBLE1BQU0sT0FBTyxjQUFjLEdBQXVCO0lBQ2hELGFBQWEsRUFBRSxVQUFVO0lBQ3pCLFNBQVMsRUFBRTtRQUNULGdCQUFnQixFQUFFLEtBQUs7UUFDdkIsZ0JBQWdCLEVBQUUsTUFBTTtRQUN4QixjQUFjLEVBQUUsT0FBTztRQUN2QixtQkFBbUIsRUFBRSxZQUFZO0tBQ2xDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEcmFnVG9TZWxlY3RDb25maWcgfSBmcm9tICcuL21vZGVscyc7XHJcblxyXG5leHBvcnQgY29uc3QgREVGQVVMVF9DT05GSUc6IERyYWdUb1NlbGVjdENvbmZpZyA9IHtcclxuICBzZWxlY3RlZENsYXNzOiAnc2VsZWN0ZWQnLFxyXG4gIHNob3J0Y3V0czoge1xyXG4gICAgZGlzYWJsZVNlbGVjdGlvbjogJ2FsdCcsXHJcbiAgICB0b2dnbGVTaW5nbGVJdGVtOiAnbWV0YScsXHJcbiAgICBhZGRUb1NlbGVjdGlvbjogJ3NoaWZ0JyxcclxuICAgIHJlbW92ZUZyb21TZWxlY3Rpb246ICdzaGlmdCttZXRhJ1xyXG4gIH1cclxufTtcclxuIl19