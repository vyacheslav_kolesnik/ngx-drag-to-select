/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MIN_HEIGHT, MIN_WIDTH } from './constants';
/** @type {?} */
export const isObject = (/**
 * @param {?} item
 * @return {?}
 */
(item) => {
    return item && typeof item === 'object' && !Array.isArray(item) && item !== null;
});
/**
 * @param {?} target
 * @param {?} source
 * @return {?}
 */
export function mergeDeep(target, source) {
    if (isObject(target) && isObject(source)) {
        Object.keys(source).forEach((/**
         * @param {?} key
         * @return {?}
         */
        key => {
            if (isObject(source[key])) {
                if (!target[key]) {
                    Object.assign(target, { [key]: {} });
                }
                mergeDeep(target[key], source[key]);
            }
            else {
                Object.assign(target, { [key]: source[key] });
            }
        }));
    }
    return target;
}
/** @type {?} */
export const hasMinimumSize = (/**
 * @param {?} selectBox
 * @param {?=} minWidth
 * @param {?=} minHeight
 * @return {?}
 */
(selectBox, minWidth = MIN_WIDTH, minHeight = MIN_HEIGHT) => {
    return selectBox.width > minWidth || selectBox.height > minHeight;
});
/** @type {?} */
export const clearSelection = (/**
 * @param {?} window
 * @return {?}
 */
(window) => {
    /** @type {?} */
    const selection = window.getSelection();
    if (!selection) {
        return;
    }
    if (selection.removeAllRanges) {
        selection.removeAllRanges();
    }
    else if (selection.empty) {
        selection.empty();
    }
});
/** @type {?} */
export const inBoundingBox = (/**
 * @param {?} point
 * @param {?} box
 * @return {?}
 */
(point, box) => {
    return (box.left <= point.x && point.x <= box.left + box.width && box.top <= point.y && point.y <= box.top + box.height);
});
/** @type {?} */
export const boxIntersects = (/**
 * @param {?} boxA
 * @param {?} boxB
 * @return {?}
 */
(boxA, boxB) => {
    return (boxA.left <= boxB.left + boxB.width &&
        boxA.left + boxA.width >= boxB.left &&
        boxA.top <= boxB.top + boxB.height &&
        boxA.top + boxA.height >= boxB.top);
});
/** @type {?} */
export const calculateBoundingClientRect = (/**
 * @param {?} element
 * @return {?}
 */
(element) => {
    return element.getBoundingClientRect();
});
/** @type {?} */
export const getMousePosition = (/**
 * @param {?} event
 * @return {?}
 */
(event) => {
    return event instanceof MouseEvent
        ? {
            x: event.clientX,
            y: event.clientY
        }
        : {
            x: event.touches[0].clientX,
            y: event.touches[0].clientY
        };
});
/** @type {?} */
export const getScroll = (/**
 * @return {?}
 */
() => {
    if (!document || !document.documentElement) {
        return {
            x: 0,
            y: 0
        };
    }
    return {
        x: document.documentElement.scrollLeft || document.body.scrollLeft,
        y: document.documentElement.scrollTop || document.body.scrollTop
    };
});
/** @type {?} */
export const getRelativeMousePosition = (/**
 * @param {?} event
 * @param {?} container
 * @return {?}
 */
(event, container) => {
    const { x: clientX, y: clientY } = getMousePosition(event);
    /** @type {?} */
    const scroll = getScroll();
    /** @type {?} */
    const borderSize = (container.boundingClientRect.width - container.clientWidth) / 2;
    /** @type {?} */
    const offsetLeft = container.boundingClientRect.left + scroll.x;
    /** @type {?} */
    const offsetTop = container.boundingClientRect.top + scroll.y;
    return {
        x: clientX - borderSize - (offsetLeft - window.pageXOffset) + container.scrollLeft,
        y: clientY - borderSize - (offsetTop - window.pageYOffset) + container.scrollTop
    };
});
/** @type {?} */
export const cursorWithinElement = (/**
 * @param {?} event
 * @param {?} element
 * @return {?}
 */
(event, element) => {
    /** @type {?} */
    const mousePoint = getMousePosition(event);
    return inBoundingBox(mousePoint, calculateBoundingClientRect(element));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtZHJhZy10by1zZWxlY3QvIiwic291cmNlcyI6WyJsaWIvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDOztBQUdwRCxNQUFNLE9BQU8sUUFBUTs7OztBQUFHLENBQUMsSUFBUyxFQUFFLEVBQUU7SUFDcEMsT0FBTyxJQUFJLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEtBQUssSUFBSSxDQUFDO0FBQ25GLENBQUMsQ0FBQTs7Ozs7O0FBRUQsTUFBTSxVQUFVLFNBQVMsQ0FBQyxNQUFjLEVBQUUsTUFBYztJQUN0RCxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUU7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ2hCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2lCQUN0QztnQkFDRCxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ3JDO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQy9DO1FBQ0gsQ0FBQyxFQUFDLENBQUM7S0FDSjtJQUVELE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUM7O0FBRUQsTUFBTSxPQUFPLGNBQWM7Ozs7OztBQUFHLENBQUMsU0FBNEIsRUFBRSxRQUFRLEdBQUcsU0FBUyxFQUFFLFNBQVMsR0FBRyxVQUFVLEVBQUUsRUFBRTtJQUMzRyxPQUFPLFNBQVMsQ0FBQyxLQUFLLEdBQUcsUUFBUSxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO0FBQ3BFLENBQUMsQ0FBQTs7QUFFRCxNQUFNLE9BQU8sY0FBYzs7OztBQUFHLENBQUMsTUFBYyxFQUFFLEVBQUU7O1VBQ3pDLFNBQVMsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFO0lBRXZDLElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDZCxPQUFPO0tBQ1I7SUFFRCxJQUFJLFNBQVMsQ0FBQyxlQUFlLEVBQUU7UUFDN0IsU0FBUyxDQUFDLGVBQWUsRUFBRSxDQUFDO0tBQzdCO1NBQU0sSUFBSSxTQUFTLENBQUMsS0FBSyxFQUFFO1FBQzFCLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztLQUNuQjtBQUNILENBQUMsQ0FBQTs7QUFFRCxNQUFNLE9BQU8sYUFBYTs7Ozs7QUFBRyxDQUFDLEtBQW9CLEVBQUUsR0FBZ0IsRUFBRSxFQUFFO0lBQ3RFLE9BQU8sQ0FDTCxHQUFHLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUNoSCxDQUFDO0FBQ0osQ0FBQyxDQUFBOztBQUVELE1BQU0sT0FBTyxhQUFhOzs7OztBQUFHLENBQUMsSUFBaUIsRUFBRSxJQUFpQixFQUFFLEVBQUU7SUFDcEUsT0FBTyxDQUNMLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSztRQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLElBQUk7UUFDbkMsSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNO1FBQ2xDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUNuQyxDQUFDO0FBQ0osQ0FBQyxDQUFBOztBQUVELE1BQU0sT0FBTywyQkFBMkI7Ozs7QUFBRyxDQUFDLE9BQW9CLEVBQWUsRUFBRTtJQUMvRSxPQUFPLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0FBQ3pDLENBQUMsQ0FBQTs7QUFFRCxNQUFNLE9BQU8sZ0JBQWdCOzs7O0FBQUcsQ0FBQyxLQUE4QixFQUFFLEVBQUU7SUFDakUsT0FBTyxLQUFLLFlBQVksVUFBVTtRQUNoQyxDQUFDLENBQUM7WUFDRSxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU87WUFDaEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPO1NBQ2pCO1FBQ0gsQ0FBQyxDQUFDO1lBQ0UsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztZQUMzQixDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO1NBQzVCLENBQUM7QUFDUixDQUFDLENBQUE7O0FBRUQsTUFBTSxPQUFPLFNBQVM7OztBQUFHLEdBQUcsRUFBRTtJQUM1QixJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRTtRQUMxQyxPQUFPO1lBQ0wsQ0FBQyxFQUFFLENBQUM7WUFDSixDQUFDLEVBQUUsQ0FBQztTQUNMLENBQUM7S0FDSDtJQUVELE9BQU87UUFDTCxDQUFDLEVBQUUsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVO1FBQ2xFLENBQUMsRUFBRSxRQUFRLENBQUMsZUFBZSxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVM7S0FDakUsQ0FBQztBQUNKLENBQUMsQ0FBQTs7QUFFRCxNQUFNLE9BQU8sd0JBQXdCOzs7OztBQUFHLENBQ3RDLEtBQThCLEVBQzlCLFNBQThCLEVBQ2YsRUFBRTtVQUNYLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDOztVQUNwRCxNQUFNLEdBQUcsU0FBUyxFQUFFOztVQUVwQixVQUFVLEdBQUcsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDOztVQUM3RSxVQUFVLEdBQUcsU0FBUyxDQUFDLGtCQUFrQixDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQzs7VUFDekQsU0FBUyxHQUFHLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUM7SUFFN0QsT0FBTztRQUNMLENBQUMsRUFBRSxPQUFPLEdBQUcsVUFBVSxHQUFHLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxTQUFTLENBQUMsVUFBVTtRQUNsRixDQUFDLEVBQUUsT0FBTyxHQUFHLFVBQVUsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsU0FBUyxDQUFDLFNBQVM7S0FDakYsQ0FBQztBQUNKLENBQUMsQ0FBQTs7QUFFRCxNQUFNLE9BQU8sbUJBQW1COzs7OztBQUFHLENBQUMsS0FBOEIsRUFBRSxPQUFvQixFQUFFLEVBQUU7O1VBQ3BGLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7SUFDMUMsT0FBTyxhQUFhLENBQUMsVUFBVSxFQUFFLDJCQUEyQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDekUsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTUlOX0hFSUdIVCwgTUlOX1dJRFRIIH0gZnJvbSAnLi9jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBCb3VuZGluZ0JveCwgTW91c2VQb3NpdGlvbiwgU2VsZWN0Qm94LCBTZWxlY3RDb250YWluZXJIb3N0IH0gZnJvbSAnLi9tb2RlbHMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGlzT2JqZWN0ID0gKGl0ZW06IGFueSkgPT4ge1xyXG4gIHJldHVybiBpdGVtICYmIHR5cGVvZiBpdGVtID09PSAnb2JqZWN0JyAmJiAhQXJyYXkuaXNBcnJheShpdGVtKSAmJiBpdGVtICE9PSBudWxsO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG1lcmdlRGVlcCh0YXJnZXQ6IE9iamVjdCwgc291cmNlOiBPYmplY3QpIHtcclxuICBpZiAoaXNPYmplY3QodGFyZ2V0KSAmJiBpc09iamVjdChzb3VyY2UpKSB7XHJcbiAgICBPYmplY3Qua2V5cyhzb3VyY2UpLmZvckVhY2goa2V5ID0+IHtcclxuICAgICAgaWYgKGlzT2JqZWN0KHNvdXJjZVtrZXldKSkge1xyXG4gICAgICAgIGlmICghdGFyZ2V0W2tleV0pIHtcclxuICAgICAgICAgIE9iamVjdC5hc3NpZ24odGFyZ2V0LCB7IFtrZXldOiB7fSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbWVyZ2VEZWVwKHRhcmdldFtrZXldLCBzb3VyY2Vba2V5XSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgT2JqZWN0LmFzc2lnbih0YXJnZXQsIHsgW2tleV06IHNvdXJjZVtrZXldIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHJldHVybiB0YXJnZXQ7XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBoYXNNaW5pbXVtU2l6ZSA9IChzZWxlY3RCb3g6IFNlbGVjdEJveDxudW1iZXI+LCBtaW5XaWR0aCA9IE1JTl9XSURUSCwgbWluSGVpZ2h0ID0gTUlOX0hFSUdIVCkgPT4ge1xyXG4gIHJldHVybiBzZWxlY3RCb3gud2lkdGggPiBtaW5XaWR0aCB8fCBzZWxlY3RCb3guaGVpZ2h0ID4gbWluSGVpZ2h0O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNsZWFyU2VsZWN0aW9uID0gKHdpbmRvdzogV2luZG93KSA9PiB7XHJcbiAgY29uc3Qgc2VsZWN0aW9uID0gd2luZG93LmdldFNlbGVjdGlvbigpO1xyXG5cclxuICBpZiAoIXNlbGVjdGlvbikge1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgaWYgKHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMpIHtcclxuICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcclxuICB9IGVsc2UgaWYgKHNlbGVjdGlvbi5lbXB0eSkge1xyXG4gICAgc2VsZWN0aW9uLmVtcHR5KCk7XHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGluQm91bmRpbmdCb3ggPSAocG9pbnQ6IE1vdXNlUG9zaXRpb24sIGJveDogQm91bmRpbmdCb3gpID0+IHtcclxuICByZXR1cm4gKFxyXG4gICAgYm94LmxlZnQgPD0gcG9pbnQueCAmJiBwb2ludC54IDw9IGJveC5sZWZ0ICsgYm94LndpZHRoICYmIGJveC50b3AgPD0gcG9pbnQueSAmJiBwb2ludC55IDw9IGJveC50b3AgKyBib3guaGVpZ2h0XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBib3hJbnRlcnNlY3RzID0gKGJveEE6IEJvdW5kaW5nQm94LCBib3hCOiBCb3VuZGluZ0JveCkgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICBib3hBLmxlZnQgPD0gYm94Qi5sZWZ0ICsgYm94Qi53aWR0aCAmJlxyXG4gICAgYm94QS5sZWZ0ICsgYm94QS53aWR0aCA+PSBib3hCLmxlZnQgJiZcclxuICAgIGJveEEudG9wIDw9IGJveEIudG9wICsgYm94Qi5oZWlnaHQgJiZcclxuICAgIGJveEEudG9wICsgYm94QS5oZWlnaHQgPj0gYm94Qi50b3BcclxuICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdCA9IChlbGVtZW50OiBIVE1MRWxlbWVudCk6IEJvdW5kaW5nQm94ID0+IHtcclxuICByZXR1cm4gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRNb3VzZVBvc2l0aW9uID0gKGV2ZW50OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkgPT4ge1xyXG4gIHJldHVybiBldmVudCBpbnN0YW5jZW9mIE1vdXNlRXZlbnRcclxuICAgID8ge1xyXG4gICAgICAgIHg6IGV2ZW50LmNsaWVudFgsXHJcbiAgICAgICAgeTogZXZlbnQuY2xpZW50WVxyXG4gICAgICB9XHJcbiAgICA6IHtcclxuICAgICAgICB4OiBldmVudC50b3VjaGVzWzBdLmNsaWVudFgsXHJcbiAgICAgICAgeTogZXZlbnQudG91Y2hlc1swXS5jbGllbnRZXHJcbiAgICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0U2Nyb2xsID0gKCkgPT4ge1xyXG4gIGlmICghZG9jdW1lbnQgfHwgIWRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgeDogMCxcclxuICAgICAgeTogMFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICB4OiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbExlZnQsXHJcbiAgICB5OiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wXHJcbiAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRSZWxhdGl2ZU1vdXNlUG9zaXRpb24gPSAoXHJcbiAgZXZlbnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50LFxyXG4gIGNvbnRhaW5lcjogU2VsZWN0Q29udGFpbmVySG9zdFxyXG4pOiBNb3VzZVBvc2l0aW9uID0+IHtcclxuICBjb25zdCB7IHg6IGNsaWVudFgsIHk6IGNsaWVudFkgfSA9IGdldE1vdXNlUG9zaXRpb24oZXZlbnQpO1xyXG4gIGNvbnN0IHNjcm9sbCA9IGdldFNjcm9sbCgpO1xyXG5cclxuICBjb25zdCBib3JkZXJTaXplID0gKGNvbnRhaW5lci5ib3VuZGluZ0NsaWVudFJlY3Qud2lkdGggLSBjb250YWluZXIuY2xpZW50V2lkdGgpIC8gMjtcclxuICBjb25zdCBvZmZzZXRMZWZ0ID0gY29udGFpbmVyLmJvdW5kaW5nQ2xpZW50UmVjdC5sZWZ0ICsgc2Nyb2xsLng7XHJcbiAgY29uc3Qgb2Zmc2V0VG9wID0gY29udGFpbmVyLmJvdW5kaW5nQ2xpZW50UmVjdC50b3AgKyBzY3JvbGwueTtcclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHg6IGNsaWVudFggLSBib3JkZXJTaXplIC0gKG9mZnNldExlZnQgLSB3aW5kb3cucGFnZVhPZmZzZXQpICsgY29udGFpbmVyLnNjcm9sbExlZnQsXHJcbiAgICB5OiBjbGllbnRZIC0gYm9yZGVyU2l6ZSAtIChvZmZzZXRUb3AgLSB3aW5kb3cucGFnZVlPZmZzZXQpICsgY29udGFpbmVyLnNjcm9sbFRvcFxyXG4gIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY3Vyc29yV2l0aGluRWxlbWVudCA9IChldmVudDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQsIGVsZW1lbnQ6IEhUTUxFbGVtZW50KSA9PiB7XHJcbiAgY29uc3QgbW91c2VQb2ludCA9IGdldE1vdXNlUG9zaXRpb24oZXZlbnQpO1xyXG4gIHJldHVybiBpbkJvdW5kaW5nQm94KG1vdXNlUG9pbnQsIGNhbGN1bGF0ZUJvdW5kaW5nQ2xpZW50UmVjdChlbGVtZW50KSk7XHJcbn07XHJcbiJdfQ==