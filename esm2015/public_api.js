/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of ngx-drag-to-select
 */
export { CONFIG_FACTORY, DragToSelectModule } from './lib/drag-to-select.module';
export { SelectContainerComponent } from './lib/select-container.component';
export { SelectItemDirective } from './lib/select-item.directive';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1kcmFnLXRvLXNlbGVjdC8iLCJzb3VyY2VzIjpbInB1YmxpY19hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUlBLG1EQUFjLDZCQUE2QixDQUFDO0FBQzVDLHlDQUFjLGtDQUFrQyxDQUFDO0FBQ2pELG9DQUFjLDZCQUE2QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIG5neC1kcmFnLXRvLXNlbGVjdFxyXG4gKi9cclxuXHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL2RyYWctdG8tc2VsZWN0Lm1vZHVsZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlbGVjdC1jb250YWluZXIuY29tcG9uZW50JztcclxuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VsZWN0LWl0ZW0uZGlyZWN0aXZlJztcclxuIl19